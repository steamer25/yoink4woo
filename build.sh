#!/bin/bash
set -x

FILENAME=yoink4woo.zip
if [ -f $FILENAME ]; then
  rm $FILENAME
fi

rm -rf build/
mkdir build/

cp -r assets/ src/ composer.json composer.lock LICENSE readme.txt yoink4woo.php build/
pushd build/ || exit
composer install --no-dev --optimize-autoloader
rm composer.json composer.lock

zip -r ../$FILENAME ./*

popd || exit
#-x \*.DS_Store build.sh \*.gitignore \*/scratch.html \*/bak/\* localDev/\* -x yoink4woo.*.zip