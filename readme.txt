=== Yoink Inventory ===
Contributors: Brett Stime
Tags: inventory, warehouse, woocommerce
Requires at least: 4.7
Tested up to: 6.0.6
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Donate link: http://stime.tech/

For support, merge requests, etc. please see the official Git repository at
https://gitlab.com/steamer25/yoink4woo/

We'd love to help you get started with Yoink Inventory. You can also find
documentation and a contact form, etc. at the official site linked in the
'donate' field above. The best way to support Yoink Inventory would be to
buy a copy of the mobile app.
