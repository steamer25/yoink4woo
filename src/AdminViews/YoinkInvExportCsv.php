<?php

namespace YoinkInv\AdminViews;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use YoinkInv\Classes\YoinkInvCsvStashEntry;

include_once( YOINK_INV_ABSPATH . 'src/Includes/yoink-consts.php' );

function esc_quoted( $s ) {
	return str_replace( "\"", "\"\"", $s );
}

class YoinkInvExportCsv {
	public static function yoink_inv_display() {
		if ( ! current_user_can( 'yoink_export_data' ) ) {
			wp_die( __( 'Unfortunately, you don\'t have permission to export data from Yoink Inventory. Please ask your
				organization\'s administrator for access.',
				'yoinkinventory' ) );
		}

		// header( 'Content-type: "text/plain; charset=UTF-8"' );
		// header( 'X-Content-Type-Options: nosniff' );

		if ( ! isset( $_GET['suppress_download'] ) ) {
			header( 'Content-type: "text/csv; charset=UTF-8"' );
			header( 'Content-Disposition: attachment;filename=YoinkInventory_'
			        . gmdate( 'Ymd_His' ) . '.csv' );
		}

		global $wpdb;

		$ordered_stash_listings = array();
		$rows                   = $wpdb->get_results(
			"SELECT W.name AS warehouse, S.stash_id, P.post_title AS product, S.quantity
			FROM {$wpdb->prefix}yoinkinventory_stashes S
				LEFT JOIN {$wpdb->prefix}yoinkinventory_warehouses W
					ON S.warehouse_id = W.warehouse_id
				LEFT JOIN $wpdb->posts P
					ON S.product_id = P.ID"
		);

		foreach ( $rows as $row ) {
			// echo '"' . $row->stash_id . '", ' . $row->quantity . PHP_EOL;
			$entry                    = new YoinkInvCsvStashEntry( $row );
			$ordered_stash_listings[] = $entry;
		}

		usort( $ordered_stash_listings, function ( $a_parts, $b ) {
			$max_index = min( $a_parts->stash_part_count,
				$b->stash_part_count );
			$a_parts   = $a_parts->stash_parts;
			$b_parts   = $b->stash_parts;
			for ( $i = 0; $i < $max_index; $i ++ ) {
				$a_part = $a_parts[ $i ];
				$b_part = $b_parts[ $i ];
				if ( $a_part != $b_part ) {
					return $a_part - $b_part;
				}
			}

			return 0;
		} );

		_e(
			'Location Kind, Location, Product, Quantity',
			'yoinkinventory' );
		print( PHP_EOL );

		foreach ( $ordered_stash_listings as $o ) {
			printf( '"Warehouse stash (%1$s)", "%2$s", "%3$s", %4$d%5$s',
				esc_quoted( $o->row->warehouse ), esc_quoted( $o->display ),
				esc_quoted( $o->row->product ), $o->row->quantity, PHP_EOL );
		}

		$personal_custody_query = <<<SQL
SELECT U.display_name AS userDisplay, P.post_title AS product, C.quantity
FROM {$wpdb->prefix}yoinkinventory_personal_custody C
LEFT JOIN $wpdb->users U
	ON C.user_id = U.ID
LEFT JOIN {$wpdb->posts} P
	ON C.product_id = P.ID
ORDER BY display_name, product
SQL;

		//		ToDo: Move order/sort logic to middle tier

		$personal_custody_rows = $wpdb->get_results( $personal_custody_query );

		foreach ( $personal_custody_rows as $personal_custody_row ) {
//			SELECT U.display_name AS userDisplay, P.post_title AS product, C.quantity
			// translators: %1$s: The name of the person currently handling the inventory; %2$s: The name of the product being handled; %3$d: The quantity of products being handled.
			printf( __( '"Personal custody", "%1$s", "%2$s", %3$d',
				'yoinkinventory' ),
				esc_quoted( $personal_custody_row->userDisplay ),
				esc_quoted( $personal_custody_row->product ),
				$personal_custody_row->quantity );
			print( PHP_EOL );
		}

		$order_item_query = <<<SQL
SELECT S.order_id, U.display_name AS userDisplay, I.order_item_name AS product, S.quantity
FROM {$wpdb->prefix}yoinkinventory_order_item_state S
LEFT JOIN {$wpdb->prefix}woocommerce_order_items I
    ON S.order_item_id = I.order_item_id
LEFT JOIN $wpdb->users U
	ON S.user_id = U.ID
WHERE S.state_id = %d
ORDER BY order_id, userDisplay, product
SQL;

//		ToDo: Move order/sort logic to middle tier

		$order_item_rows
			= $wpdb->get_results( $wpdb->prepare( $order_item_query,
			YOINKINV_ITEM_STATUS_YOINKED_FOR_ORDER ) );

		foreach ( $order_item_rows as $order_item_row ) {
//			SELECT U.display_name AS userDisplay, P.post_title AS product, C.quantity
			// translators: %1$s: The name of the person currently handling the inventory; %2$s: The name of the product being handled; %3$d: The quantity of products being handled.
			printf( __( '"Yoinked for order #%1$d", "%2$s", "%3$s", %4$d',
				'yoinkinventory' ),
				$order_item_row->order_id,
				esc_quoted( $order_item_row->userDisplay ),
				esc_quoted( $order_item_row->product ),
				$order_item_row->quantity );
			print( PHP_EOL );
		}

		exit( 200 );
	}
}
