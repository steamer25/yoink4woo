(function () {
	// Copyright: © 2017 Brett Stime.
	const $ = jQuery,
		windowArgs = window.location.search.substring(1).split('&'),
		integerPattern = /^-?\d+$/;
	let prefix = [1],
		dimensions = [1],
		printSize = 654,
		cssSize,
		margin,
		fontSize,
		i,
		x;

	const calculateSizes = function () {
			cssSize = Math.ceil((printSize / 300.0) * 72);
			margin = Math.ceil(cssSize * 0.2);
			fontSize = Math.ceil(cssSize / 3.0);
		},
		cleanIntegerArray = function (windowArgVal, delimiter) {
			const decoded = decodeURIComponent(windowArgVal),
				arr = decoded.split(delimiter),
				parsed = [];
			let j, y;
			for (j = 0; j < arr.length; j++) {
				y = arr[j].trim();
				if (!integerPattern.test(y)) {
					$('#maybeErrMessages').append(
						// $('<li>').append(
						'<li>Invalid value "' +
							y +
							'" in "' +
							decoded +
							'"</li>'
						// )
					);
					// console.log("Invalid value '" + y + '\' in "' + decoded + '"');
				}
				parsed.push(parseInt(y, 10));
			}
			return parsed;
		},
		recurseDimIteration = function (
			innerPrefix,
			rangeStart,
			rangeEnd,
			recurseDimVals
		) {
			// console.log("recurseDimIteration", innerPrefix, rangeStart, rangeEnd, recurseDimVals);
			for (; rangeStart < rangeEnd; rangeStart++) {
				const recursePrefix = innerPrefix.slice(0);
				recursePrefix.push(rangeStart);
				recurseDims(recursePrefix, recurseDimVals);
			}
		},
		recurseDims = function (innerPrefix, innerDims) {
			// console.log("recurseDims", innerPrefix, innerDims);
			let shiftee, recurseDimVals, qr, jCanvas;
			if (innerDims.length > 0) {
				recurseDimVals = innerDims.slice(0);
				shiftee = recurseDimVals.shift();

				if (shiftee > 0) {
					recurseDimIteration(
						innerPrefix,
						1,
						shiftee + 1,
						recurseDimVals
					);
				}
				if (shiftee < 0) {
					recurseDimIteration(
						innerPrefix,
						shiftee - 1,
						-1,
						recurseDimVals
					);
				}
			} else {
				// console.log();
				// console.log("Code:", innerPrefix);
				// console.log();

				// Margin 0.16 = 4/25 of size. 25 == number of QR 'modules' across. 4 modules == minimum margin from spec.
				// Round up to allow for scissor kerf, wiggle room, etc.

				qr = $(
					'<div style="width: ' + (cssSize + 2 * margin) + 'px"/>'
				).qrcode({
					width: printSize,
					height: printSize,
					text: innerPrefix.join(','),
				});
				jCanvas = qr.find('canvas');
				jCanvas.css('margin', margin + 'px');
				jCanvas.css('width', cssSize + 'px');
				jCanvas.css('height', cssSize + 'px');
				// setDPI(jCanvas[0], 300);
				$('<div class="qr"/>')
					.append(qr)
					.append(
						'<p style="font-size: ' +
							fontSize +
							'px">' +
							innerPrefix.join(', ') +
							'</p>'
					)
					.appendTo($('body'));

				// console.log("Display:", innerPrefix.join(", ")); // Consider: Could append to string during recursion instead of joining heres. Might help performance.
			}
		};

	// codesPrefix=&codesdimensions=&codesSize=600
	for (i = 0; i < windowArgs.length; i++) {
		x = windowArgs[i].split('=');
		// console.log(x);
		if (x[0] === 'codesPrefix') {
			prefix = cleanIntegerArray(x[1], ',');
			// console.log('prefix', prefix);
			continue;
		}
		if (x[0] === 'codesDimensions') {
			dimensions = cleanIntegerArray(x[1], 'x');
			// console.log('dimensions:', dimensions);
			continue;
		}
		if (x[0] === 'codesSize') {
			printSize = parseInt(x[1], 10);
		}
	}

	calculateSizes();

	i = prefix.pop();
	x = dimensions.shift();
	if (i < 0) {
		recurseDimIteration(prefix, i - x + 1, i + 1, dimensions);
	} else {
		recurseDimIteration(prefix, i, i + x, dimensions);
	}
})();
