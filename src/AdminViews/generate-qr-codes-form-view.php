<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvGenerateQrCodesFormView {
	public static function yoink_inv_enqueue_assets() {
		wp_enqueue_script( 'qrcode' );
	}

	public static function yoink_inv_display() {
		if ( ! current_user_can( 'yoink_generate_qr_codes' ) ) {
			wp_die( esc_html__( 'Unfortunately, you don\'t have permission to generate QR codes. Please ask your organization\'s administrator for access.',
				'yoinkinventory' ) );
		}
		?>
        <div class="yoink">
            <h1 class="genQr"><?php esc_html_e( 'Generate QR codes for bins, shelves or other stashes',
					'yoinkinventory' ); ?></h1>
            <form action="<?php echo admin_url( 'admin.php' ); ?>"
                  method="get" target="_blank">
                <input type="hidden" name="page"
                       value="print-yoink-qr-codes"/>
                <table>
                    <tr>
                        <td class="labelCell">
                            <label for="codesPrefix"><?php esc_html_e( 'Prefix',
									'yoinkinventory' ); ?></label>
                        </td>
                        <td>
                            <input id="codesPrefix" name="codesPrefix"
                                   type="text"/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelCell">
                            <label for="codesDimensions"><?php esc_html_e( 'Stash Dimensions',
									'yoinkinventory' ); ?></label>
                        </td>
                        <td>
                            <input id="codesDimensions"
                                   name="codesDimensions"
                                   type="text"/>
                        </td>
                    </tr>

                    <!--<tr><td class="labelCell">
                    <label for="codesSize"><?php esc_html_e( 'Print Size',
						'yoinkinventory' ); ?></label>
                </td><td>
                    <!-/- 737 == 2x2 on letter; 654 == 3x2 on letter; -/->
                    <input name="codesSize" type="text" value="654"></input>
                </td></tr>-->
                    <tr>
                        <td>
                            <!--empty-->
                        </td>
                        <td>
                            <button type="submit"><?php esc_html_e( 'Generate',
									'yoinkinventory' ); ?></button>
                        </td>
                    </tr>
                </table>
            </form>
            <h3 class="howUse"><?php esc_html_e( 'How do I use this tool?',
					'yoinkinventory' ); ?></h3>
            <p>
				<?php esc_html_e( 'This tool helps you generate labeled QR code address/identifiers for each distinct storage location or
            \'stash\' in your warehouse. Each identifier needs to be unique but should also be easy for a human
            physically searching through the warehouse to locate.
            ', 'yoinkinventory' ); ?>
            </p>
            <p>
				<?php esc_html_e( 'Perfectly uniform warehouses probably don\'t exist for very long. Shelving acquired at different times
            may come in different sizes. Different rooms might have different architectural features to work around, leading
            to varying aisle lengths, etc.
            ', 'yoinkinventory' ); ?>
            </p>
            <p>
				<?php esc_html_e( 'Although different sections of the warehouse may differ from each other, particular
            sub-sections might have a fair amount of uniformity within them. E.g., each shelving unit of a given make and
            model will have the same heigh and width. You might even have multiple uniform aisles all composed of the same
            shelving units. This tool allows you to quickly and easily express a range of needed address-identifiers for
            patterned/repeated sections of your warehouse while also giving you the flexibility to define differing
            sub-sections independently.
            ', 'yoinkinventory' ); ?>
            </p>
            <h4><?php esc_html_e( 'Stash Dimensions Example',
					'yoinkinventory' ); ?></h4>
            <figure id="qrGenExampleRoom">
                <img src="<?php echo plugins_url( 'assets/genQr/exampleRoom.svg',
					__FILE__ ); ?>" alt="Example Room" width="300"
                     height="270"/>
                <figcaption>Figure 1</figcaption>
            </figure>
            <p>
				<?php esc_html_e( 'In figure 1, we can see that each set of shelves divides nicely into six (6) rows and four (4)
            columns of stashes. We also see that each aisle is divided into a number of stations. We can define each station
            as having two (2) sides--a left side and a right side*.
            ', 'yoinkinventory' ); ?>
            </p>
            <p id="fig2par2">
				<?php esc_html_e( 'Next we notice that one aisle is longer than the other, having five (5) stations instead of three (3).
            We have two different aisles that we need to describe separately. We can do this by submitting the form above
twice, once each with one of the following', 'yoinkinventory' ); ?>
                <b><?php esc_html_e( 'Stash Dimension',
						'yoinkinventory' ); ?></b>
				<?php esc_html_e( 'specifications:', 'yoinkinventory' ); ?>
            </p>
            <ul class="dimensionsExample">
                <li>5x2x6x4</li>
                <li>3x2x6x4</li>
            </ul>
            <p>
				<?php esc_html_e( 'We want the values corresponding to more general, further-flung entities earlier in the
            specification, ahead of the more specific/localized ones. Like a hotel/resort where individual room numbers may
            be preceded first with the building number, then the floor number; this address format can guide handlers
            through the physical space of the warehouse, bringing them closer to an indicated stash as they read through the
            text of one of the address-identifiers.
                ', 'yoinkinventory' ); ?>
            </p>
            <p>
				<?php esc_html_e( 'The first entry above will generate (5 times 2 times 6 times 4 equals) 240 QR code labels for us.
            The second submission will generate (3 times 2 times 6 times 4 equals) 144 distinct labels that can be printed.
            Longer addresses are taller and wider but even with moderate length, we can fit at least six labels on a
            standard 8.5" x 11" sheet of paper. To print all the labels for the example would take up to 64
            sheets. Note, the generated IDs won\'t actually be added to the database until and unless some inventory items
            are associated with the respective stash via e.g., Yoink Mobile.
                ', 'yoinkinventory' ); ?>
            </p>
            <p>
				<?php esc_html_e( '* Alternately, we could group the whole left side of each
                walkway as a distinct aisle from the group on the right side--that might be preferable e.g., if the sides are
                too far apart for people to easily browse at the same time.
                ', 'yoinkinventory' ); ?>
            </p>
            <h4><?php esc_html_e( 'Prefix Example', 'yoinkinventory' ); ?></h4>
            <p>
				<?php esc_html_e( 'We need to supply one other piece of information before we can generate QR code labels. Also, the
            difference in aisle length might not be the only non-uniformity in our warehouse. One way to think about such a
            divergent warehouse is as a hierarchy/tree with different stratums/branches representing the differing storage
            areas. Again, we place the more generalized/further-flung entities closer to the root/trunk and more
            specific/localized ones out towards the tips/leaves.
            ', 'yoinkinventory' ); ?>
            </p>
            <figure id="warehouseTree">
                <img src="<?php echo plugins_url( 'assets/genQr/layout-tree.svg',
					__FILE__ ); ?>" alt="Address Hierarchy" width="300"
                     height="270"/>
                <figcaption>Figure 2</figcaption>
            </figure>
            <p>
				<?php esc_html_e( 'E.g., the West Cityopolis Warehouse might be divided as in figure 2. The prefix we provide in the form
            should be a comma-separated set of numbers that will make up the first part of every address-identifier
            generated. E.g., if figure 1 depicts a space on the the third (3) floor, room number seven (7) then we\'d submit
            the following prefixes corresponding to the dimension entries above:
                ', 'yoinkinventory' ); ?>
            </p>
            <ul class="prefixExample">
                <li>3,7,1,1</li>
                <li>3,7,2,1</li>
            </ul>
            <p>
				<?php esc_html_e( 'In the first submission, we specify \'1\' in the 3rd position indicating aisle 1. In the second
            submission, we specify \'2\' in the 3rd position indicating aisle 2.
            ', 'yoinkinventory' ); ?>
            </p>
            <p>
				<?php esc_html_e( 'Both submissions have \'1\' in the 4th/last position indicating that we\'re starting from station 1
            in both aisles:
                ', 'yoinkinventory' ); ?>
            </p>
            <div class="importantNotice">
                <p><b>
						<?php esc_html_e( 'The last position of the prefix and first position of the dimensions both refer to the SAME level in
                the hierarchy/tree (e.g., floors or rooms or aisles or shelving units, etc.).
                    ', 'yoinkinventory' ); ?>
                    </b></p>
                <p>
					<?php esc_html_e( 'The last position of the prefix indicates which number to start counting from--allowing you to
                resume/extend the address-identifier range of a space at later date.
                    ', 'yoinkinventory' ); ?>
                </p>
                <p>
					<?php esc_html_e( 'The first position of the dimensions field indicates how many different numbers should be used at
                that part of the address (each successive iteration adds one).
                    ', 'yoinkinventory' ); ?>
                </p>
            </div>
            <p>
				<?php esc_html_e( "E.g., if the prefix is '2,1' and the dimensions are '3x1', the following addresses would be
            generated:", 'yoinkinventory' ); ?>
            </p>
            <ul class="outputExample">
                <li>2,1,1</li>
                <li>2,2,1</li>
                <li>2,3,1</li>
            </ul>
            <p>
				<?php esc_html_e( "...later you could extend this by entering '2,4' for the prefix and '3x1' for the dimensions which
            would produce:", 'yoinkinventory' ); ?>
            </p>
            <ul class="outputExample">
                <li>2,4,1</li>
                <li>2,5,1</li>
                <li>2,6,1</li>
            </ul>
            <h4><?php esc_html_e( 'When you distribute labels...',
					'yoinkinventory' ); ?></h4>
            <p>
				<?php esc_html_e( "...it's a good idea to have physically lower (closer to the center of the earth) positions use lower
            numbers. E.g., use 1 for the ground floor but also for the shelf closest to the floor at any position (you can
            use negative numbers for underground floors). The address-identifier numbers shouldn't necessarily imply any
            horizontal (North/South/East/West) direction, though. Try to order the counts to favor continuous, horse-shoe
            paths that avoid backtracking. When order pick lists are fetched, the system will try to order the recommended
            stashes accordingly. Future versions may use even more sophisticated path optimization algorithms.
                ", 'yoinkinventory' ); ?>
            </p>
        </div>
		<?php
	}
}

?>
