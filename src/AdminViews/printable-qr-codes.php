<?php

class YoinkInvPrintableQrCodes {
	public static function yoink_inv_display() {
		if ( ! current_user_can( 'yoink_generate_qr_codes' ) ) {
			wp_die( esc_html__( 'Unfortunately, you don\'t have permission to generate QR codes. Please ask your organization\'s administrator for access.',
				'yoinkinventory' ) );
		}

		?>
        <!DOCTYPE html>
        <html <?php language_attributes(); ?>>
        <head>
            <meta charset="UTF-8">
            <title><?php esc_html_e( 'Yoink Inventory: generated QR codes',
					'yoinkinventory' ) ?></title>
			<?php
			$style_text
				          = /** @lang CSS */
				"
                #maybeErrMessages {
                    color: red;
                    font-size: 2em;
                }
				.qr {
                    float:left;
                    border: dotted 1px #ddd;
                    white-space: nowrap;
				}
				.qr div {
                    margin: 0 auto;
				}
				.qr p {
                    text-align: center;
                    font-family: 'Times New Roman', Times, serif;
                    margin: 0 0.5em;
				}
				";
			$style_handle = 'yoinkGenQr';
			wp_register_style( $style_handle, false );
			wp_add_inline_style( $style_handle, $style_text );
			wp_print_styles( $style_handle );
			?>
        </head>
        <body>
        <ul id="maybeErrMessages"></ul>

		<?php
		wp_enqueue_script( 'jquery' );

		$jquery_qr_handle = 'jquery.qrcode';
		$jquery_qr_path   = plugins_url( 'assets/genQr/jquery.qrcode.min.js',
			__FILE__ );
		wp_register_script( $jquery_qr_handle, $jquery_qr_path );
		wp_enqueue_script( $jquery_qr_handle );

		$gen_qr_handle = 'yoinkGenQr';
		$gen_qr_path   = plugins_url( 'assets/genQr/generateQr.js', __FILE__ );
		wp_register_script( $gen_qr_handle, $gen_qr_path );
		wp_enqueue_script( $gen_qr_handle );

		wp_print_footer_scripts();
		?>

        </body>
        </html>
		<?php
	}
}

?>
