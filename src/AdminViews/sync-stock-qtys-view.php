<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvSyncStockQtysView {
	/**
	 * @throws Exception
	 */
	public static function yoink_inv_display() {
		if ( ! current_user_can( 'crud_multiple_warehouses' ) ) {
			wp_die( esc_html__( 'Unfortunately, you don\'t have permission to manage warehouse entries. Please ask your organization\'s administrator for access.',
				'yoinkinventory' ) );
		}
		$success_message = null;
		$fail_message    = null;
		if ( 'POST' === $_SERVER['REQUEST_METHOD'] ) {
			$syncAction = sanitize_key( $_POST['syncAction'] );
			switch ( $syncAction ) {
				case 'disable':
					update_option( 'yoink_sync_stock_qtys', false );
					$success_message = __( 'Synchronization disabled.',
						'yoinkinventory' );
					break;
				case 'init':
					$count_by_product_id = array();

					global $wpdb;
					$wpdb->query( 'SET TRANSACTION ISOLATION LEVEL SERIALIZABLE' );
					$wpdb->query( 'START TRANSACTION' );
					try {
						$inventory_counts = $wpdb->get_results(
							"SELECT product_id, SUM(quantity) AS qty
                                FROM {$wpdb->prefix}yoinkinventory_stashes
                                GROUP BY product_id" // Include order quantities in personal custody.
						);

						foreach ( $inventory_counts as $inventory_count ) {
							$product
								= wc_get_product( $inventory_count->product_id );

//                            error_log( 'product_id: '
//                                       . $inventory_count->product_id
//                                       . '; qty: ' . $product );
							$stock_id = $product->get_stock_managed_by_id();
							$qty      = intval( $inventory_count->qty );
							// error_log( 'product_id: ' . $inventory_count->product_id . '; qty: ' . $qty );
							if ( isset( $count_by_product_id[ $stock_id ] ) ) {
								$count_by_product_id[ $stock_id ]
									= $count_by_product_id[ $stock_id ]
									  + $qty;
							} else {
								$count_by_product_id[ $stock_id ] = $qty;
							}
						}

//                        ToDo: Consider adding compound index to woocommerce_order_itemmeta:
//                              CREATE INDEX yoinkinv_idx_meta_key_order_item_id ON {$wpdb->prefix}woocommerce_order_itemmeta (meta_key, order_item_id)
//                              CREATE INDEX yoinkinv_idx_order_item_id_meta_key ON {$wpdb->prefix}woocommerce_order_itemmeta (order_item_id, meta_key)
//                              Test does one work better than the other? Than existing, simpler indexes? Does table size affect the results?
						$committed_qtys = $wpdb->get_results(
							"SELECT M.order_item_id, M.meta_key, M.meta_value
                            FROM $wpdb->posts P
                                JOIN {$wpdb->prefix}woocommerce_order_items I
                                    ON P.ID = I.order_id
                                JOIN {$wpdb->prefix}woocommerce_order_itemmeta M
                                    ON I.order_item_id = M.order_item_id
                            WHERE P.post_type = 'shop_order'
                                AND P.post_status IN ('wc-processing', 'wc-on-hold')
                                AND M.meta_key IN ('_product_id', '_qty', '_variation_id')
                            ORDER BY order_item_id, meta_key" );

						$prev_order_item_id    = 0;
						$variation_id          = 0;
						$product_id            = 0;
						$qty                   = 0;
						$error_accumulator     = new WP_Error();
						$current_order_item_id = 0;

						foreach ( $committed_qtys as $cq ) {
							$current_order_item_id = $cq->order_item_id;
							if ( $current_order_item_id
							     != $prev_order_item_id
							) {
								self::update_counts_for_order_reservation( $error_accumulator,
									$count_by_product_id,
									$current_order_item_id, $product_id,
									$variation_id, $qty );

								$variation_id       = 0;
								$product_id         = 0;
								$qty                = 0;
								$prev_order_item_id = $cq->order_item_id;
							}

							switch ( $cq->meta_key ) {
								case '_product_id':
									$product_id = $cq->meta_value;
									break;
								case '_qty':
									$qty = $cq->meta_value;
									break;
								case '_variation_id':
									$variation_id = $cq->meta_value;
									break;
							}
						}

						self::update_counts_for_order_reservation( $error_accumulator,
							$count_by_product_id, $current_order_item_id,
							$product_id, $variation_id, $qty );

						foreach (
							$count_by_product_id as $product_id => $qty
						) {
							wc_update_product_stock( $product_id, $qty );
						}

						update_option( 'yoink_sync_stock_qtys', true );
						$wpdb->query( 'COMMIT' );

						if ( ! $error_accumulator->has_errors() ) {
							$success_message
								= __( 'Synchronization updated and enabled.',
								'yoinkinventory' );
						} else {
							$fail_message = implode( ' && ',
								$error_accumulator->get_error_messages() );
							error_log( $fail_message );
						}
					} catch ( Exception $e ) {
						error_log( $e );
						$fail_message
							= __( 'Synchronization update/initialization failed.',
							'yoinkinventory' );
						$wpdb->query( 'ROLLBACK' );
					}
					break;
				default:
					throw new Exception( 'Unimplmented syncAction value: '
					                     . $syncAction );
			}
		}
		?>
        <form class="yoink" method="post">
            <h1><?php esc_html_e( 'Synchronize Stock Quantities',
					'yoinkinventory' ); ?></h1>
			<?php
			if ( null != $success_message ) {
				?>
                <div class="success">
					<?php echo esc_html( $success_message ); ?>
                </div>
				<?php
			}

			if ( null != $fail_message ) {
				?>
                <div class="fail">
					<?php echo esc_html( $fail_message ); ?>
                </div>
				<?php
			}

			if ( get_option( 'yoink_sync_stock_qtys' ) ) {
				?>
                <button name="syncAction"
                        value="disable"><?php esc_html_e( 'Disable synchronization',
						'yoinkinventory' ); ?></button>
                <button name="syncAction"
                        value="init"><?php esc_html_e( 'Re-Initialize',
						'yoinkinventory' ); ?></button>
                <p>
					<?php
					esc_html_e( 'Woo Commerce stock quantities for products/variations that have "manage stock" enabled will
                continue to be updated automatically as Yoink Inventory stash quantities change. If, for any reason, they get
                out of sync; you can click the Re-Initialize button to recalculate current quantities for all managed products
                and variations.', 'yoinkinventory' );
					?>
                </p>
				<?php
			} else {
				?>
                <p>
					<?php
					esc_html_e( 'Synchronization overwrites Woo Commerce stock quantities of product/variations that have "manage stock"
                enabled. The new total is the sum of Yoink Inventory stash quantities, less outstanding order quantities.',
						'yoinkinventory' );
					?>
                </p>
                <button name="syncAction"
                        value="init"><?php esc_html_e( 'Enable synchronization',
						'yoinkinventory' ); ?></button>
				<?php
			}
			?>
            <p>
				<?php
				esc_html_e( 'It may take a moment to add up the quantities from all stashes, deduct reservations from all open orders
            and write the results for all applicable products/variations. If possible, it might be preferable to do the
            initialization during a period of lower inventory and order activity to avoid disrupting those processes.',
					'yoinkinventory' );
				?>
            </p>
        </form>
		<?php
	}

	/**
	 * @param WP_Error $error_accumulator
	 * @param array    $count_by_product_id
	 * @param          $current_order_item_id
	 * @param          $product_id
	 * @param          $variation_id
	 * @param          $qty
	 *
	 * @return void
	 */
	public static function update_counts_for_order_reservation(
		WP_Error $error_accumulator, array &$count_by_product_id,
		$current_order_item_id, $product_id,
		$variation_id, $qty
	) {
		if ( $product_id == 0 ) {
			$error_accumulator->add( 'missing_product_id',
				// translators: %d: The order_item_id being processed when the product_id exception occurred.
				sprintf( __( 'Warning: While processing order_item_id = %d, 
				    product_id = 0 was passed to update_counts_for_order_reservation.
				    You may have an incomplete order item entry in your database.',
					'yoinkinventory' ), $current_order_item_id ) );
            return;
		}

		if ( 0 != $variation_id ) {
			$product_id = $variation_id;
		}
		$product
			= wc_get_product( $product_id );

		$stock_id
			= $product->get_stock_managed_by_id();
		if ( isset( $count_by_product_id[ $stock_id ] ) ) {
			$count_by_product_id[ $stock_id ]
				= $count_by_product_id[ $stock_id ]
				  - intval( $qty );
		}
	}
}
