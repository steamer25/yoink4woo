<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvAddDeviceView {
	public static function yoink_inv_enqueue_assets() {
		wp_enqueue_script( 'qrcode' );
	}

	public static function yoink_inv_display() {
		?>
        <div class="yoink">
            <h1><?php esc_html_e( 'Scan here to refer the device to this site...',
					'yoinkinventory' ); ?></h1>
            <div id="sslAlert" class="alert" style="display:none;">
                <h4><?php esc_html_e( 'Insecure connection detected',
						'yoinkinventory' ); ?></h4>
                <p>
					<?php esc_html_e( 'You appear to be using an unsecured connection. Please click ',
						'yoinkinventory' ); ?>
                    <a id="attemptSslLink"><?php esc_html_e( 'here',
							'yoinkinventory' ); ?></a>
					<?php esc_html_e( 'to see if you can load the site via a fully secured connection.',
						'yoinkinventory' ); ?>
                </p>
                <p>
					<?php
					esc_html_e( 'It\'s not recommended, but if you need to (e.g., for testing), you can proceed. Yoink Mobile will
						use oAuth security which will prevent un-authorized users from writing to the system although anyone
						intercepting the messages will be able to read what is being changed along with any other data transmitted.',
						'yoinkinventory' );
					?>
                    <a href="<?php echo admin_url( 'admin.php?page=wc-settings&tab=api&section=keys',
						'http' ); ?>">
						<?php esc_html_e( 'WooCommerce settings tab.',
							'yoinkinventory' ); ?>
                    </a>
                </p>
            </div>
            <p>
				<?php
				esc_html_e( 'Please scan the QR code below from the setup page of the Yoink Mobile
					 app. That will refer your device to this site using the same site
					 address used in your browser\'s address bar above.',
					'yoinkinventory' );
				?>
            </p>

            <div id="yoinkReferQr"></div>

            <h1><?php esc_html_e( '...and then generate a consumer key.',
					'yoinkinventory' ); ?></h1>
            <p>
				<?php
				esc_html_e( 'After Yoink Mobile successfully connects to this server,
			    it\'ll prompt you for the following credentials:',
					'yoinkinventory' ); ?>
            <ul>
                <li><?php esc_html_e( 'a consumer key',
						'yoinkinventory' ) ?></li>
                <li><?php esc_html_e( 'a consumer secret',
						'yoinkinventory' ) ?></li>
            </ul>

            <p>
				<?php
				$woo_commerce_data
					                        = get_plugin_data( ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php' );
				$woo_commerce_version       = $woo_commerce_data['Version'];
				$woo_commerce_version_parts = explode( '.',
					$woo_commerce_version );
				$woo_commerce_major_version
					                        = intval( $woo_commerce_version_parts[0] );

				$woo_commerce_keys_tab = 'advanced';

				if ( $woo_commerce_major_version < 3
				     || ( $woo_commerce_major_version == 3
				          && intval( $woo_commerce_version_parts[1] <= 4 ) )
				) {
					$woo_commerce_keys_tab = 'api';
				}
				?>
				<?php esc_html_e( 'You can generate those values by clicking the',
					'yoinkinventory' ); ?>
                '<b><?php esc_html_e( 'Add key', 'yoinkinventory' ); ?></b>'
				<?php esc_html_e( 'button on the', 'yoinkinventory' ); ?>
                <a target="_blank"
                   href="<?php echo admin_url( sprintf( 'admin.php?page=wc-settings&tab=%s&section=keys',
					   $woo_commerce_keys_tab ) ); ?>">
					<?php esc_html_e( 'WooCommerce API settings tab',
						'yoinkinventory' ); ?>
                </a>
            </p>
            <p>
				<?php
				esc_html_e( 'Generate the key pair with', 'yoinkinventory' ); ?>
                <b><?php esc_html_e( '\'Read/Write\' permissions',
						'yoinkinventory' ); ?></b>.
				<?php esc_html_e( 'Once the pair has been generated, Woo Commerce will produce
					another QR code that you can scan from the next page of the
					Yoink Mobile setup process to transfer the credentials to your
					device.', 'yoinkinventory' ); ?>
            </p>
        </div>

        <script type="text/javascript">
			(function () {
				var $ = jQuery,
					proto = window.location.protocol,
					siteRef = proto + '//' + window.location.host;
				if (proto === 'http:') {

					$('#attemptSslLink').attr('href', 'https' + window.location.href.substring(4));
					$('#sslAlert').css('display', 'block');
				}
				console.log('Referring to: ' + siteRef);
				$('#yoinkReferQr').qrcode({
					size: 400,
					text: siteRef
				});
			})();
        </script>
		<?php
	}
}

?>