<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvDefaultAdminView {
	public static function yoink_inv_display() {
		?>
        <div class="yoink">
            <h1>Yoink Inventory</h1>

            <p>
				<?php esc_html_e( 'The Yoink Inventory plugin enables your Woo Commerce site to receive
				real-time inventory location and count updates from remote systems such as
				Yoink Mobile running on an Android or iOS device. Once you\'ve
				installed the app from the app store, you can use ',
					'yoinkinventory' ); ?>
                <a href="<?php echo admin_url( 'admin.php?page=add_yoink_device' ); ?>">
					<?php esc_html_e( 'this page',
						'yoinkinventory' ) ?></a> <?php esc_html_e( 'to get your
						device talking with this site.', 'yoinkinventory' ); ?>
            </p>
        </div>
		<?php
	}
}
