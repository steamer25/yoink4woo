<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvUnfilledItemsView {
	private static function yoink_inv_display_summaries() {
		global $wpdb;

		$orders_w_unfilled = $wpdb->get_results(
			"SELECT P.ID as order_id, P.post_date_gmt, M.meta_value AS customer_id, I.order_item_name AS shipping_method
			FROM $wpdb->posts P
				LEFT JOIN $wpdb->postmeta M
					ON P.ID = M.post_id
				JOIN {$wpdb->prefix}woocommerce_order_items I
					ON P.ID = I.order_id
			WHERE I.order_item_type = 'shipping'
				AND M.meta_key = '_customer_user'
				AND P.ID IN(
					SELECT DISTINCT(I.order_id)
					FROM {$wpdb->prefix}yoinkinventory_unfilled_items U
						JOIN {$wpdb->prefix}woocommerce_order_items I
							ON U.order_item_id = I.order_item_id)"
		);

		// 	"SELECT P.ID, P.post_date_gmt, C.display_name AS customer_name, I.order_item_name AS shipping_method,
		// 	S.state_id, S.user_id, U.display_name AS claimant
		// FROM {$wpdb->posts} P
		// 	JOIN {$wpdb->users} C
		// 		ON P.post_author = C.ID
		// 	JOIN {$wpdb->prefix}woocommerce_order_items I
		// 		ON P.ID = I.order_id
		// 	LEFT JOIN {$wpdb->prefix}yoinkinventory_order_state S
		// 		ON P.ID = S.order_id
		// 	LEFT JOIN {$wpdb->users} U
		// 		ON S.user_id = U.ID
		// WHERE P.post_type = 'shop_order' AND P.post_status = 'wc-processing'
		// 	AND I.order_item_type = 'shipping'
		// ORDER BY P.post_date_gmt DESC");

		?>
        <h1><?php esc_html_e( 'Review Unfilled Items',
				'yoinkinventory' ); ?></h1>
		<?php
		if ( null == $orders_w_unfilled ) {
			echo '<p>' . esc_html__( 'No orders with unfilled items.',
					'yoinkinventory' ) . '</p>';
		} else {
			include_once( YOINK_INV_ABSPATH . 'src/Includes/order-utils.php' );

			foreach ( $orders_w_unfilled as $row ) {
				$order_id = $row->order_id;
				// ToDo: Fix N+1
				$customer_display
					= yoink_inv_fetch_customer_display( $order_id,
					$row->customer_id );
				?>
                <div>
					<?php esc_html_e( 'Order', 'yoinkinventory' ); ?> <a
                            href="<?php echo admin_url( 'admin.php?page=yoink_unfilled_items&order_id='
							                            . intval( $order_id ) ); ?>">
                        #<?php echo intval( $order_id ); ?></a>
					<?php
					echo esc_html( sprintf(
					// translators: %1$s: customer display name; %2$s: order post date; %3$s: shipping method.
						__( 'by %1$s on %2$s via %3$s',
							'yoinkinventory' ),
						$customer_display,
						get_date_from_gmt( $row->post_date_gmt, 'Y-m-d' ),
						$row->shipping_method ) ); ?>
                </div>
				<?php
			}
			?>

            <h1><?php esc_html_e( 'Alerts Address', 'yoinkinventory' ); ?></h1>
            <p>
				<?php
				esc_html_e( 'By default, alerts for unfilled items will go to all 
				users who have the \'yoink_admin\' role (or \'administrators\' 
				if there are no yoink_admins). You can add an address here for
				better control over who receives alerts when orders are marked
				complete while they still have unfilled items.',
					'yoinkinventory' );
				?>
            </p>

			<?php
			$alerts_addy = get_option( 'yoink_unfilled_alerts_addy', '' );
			if ( '' == $alerts_addy ) {
				echo '<p>'
				     . esc_html__( 'Alert address removed. Default recipients restored.',
						'yoinkinventory' ) . '</p>';
			} else {
				echo '<p>' .
				     esc_html( sprintf(
				     // translators: %1$s: Email address where alerts will be sent
					     __( 'Alerts address set to: %1$s', 'yoinkinventory' ),
					     $alerts_addy ) )
				     . '</p>';
			}
			?>
            <form method="post">
                <input type="email" name="unfilled_alerts_addy"
                       value="<?php echo esc_attr( $alerts_addy ); ?>"/>
                <input class="btn" type="submit" name="Submit"
                       value="<?php esc_attr_e( 'Submit',
					       'yoinkinventory' ); ?>"/>
            </form>
			<?php
		}
	}

	private static function yoink_inv_display_order( $order_id ) {
		global $wpdb;

		$unfilled_items = $wpdb->get_results( $wpdb->prepare(
			"SELECT U.order_item_id, U.remaining_quantity, I.order_item_name
					FROM {$wpdb->prefix}yoinkinventory_unfilled_items U
						JOIN {$wpdb->prefix}woocommerce_order_items I
							ON U.order_item_id = I.order_item_id
					WHERE I.order_id = %d",
			$order_id ) );

		if ( null == $unfilled_items ) {
			?>
            <h1><?php
				echo esc_html( sprintf(
				// translators: %1$d: The order number
					__( 'No remaining unfilled items from order #%1$d',
						'yoinkinventory' ), intval( $order_id ) ) ); ?></h1>
			<?php
		} else {
			?>
            <div class="yoink">
                <h1><?php esc_html_e( 'Unfilled items',
						'yoinkinventory' ); ?></h1>
				<?php

				$order_summary = $wpdb->get_row( $wpdb->prepare(
					"SELECT P.post_date_gmt, M.meta_value AS customer_id, I.order_item_name AS shipping_method
					FROM {$wpdb->prefix}posts P
						JOIN {$wpdb->prefix}woocommerce_order_items I
							ON P.ID = I.order_id
						LEFT JOIN {$wpdb->prefix}postmeta M
							ON P.ID = M.post_id
					WHERE I.order_item_type = 'shipping' AND M.meta_key = '_customer_user' AND P.ID = %d",
					$order_id ) );

				include_once( YOINK_INV_ABSPATH
				              . 'src/Includes/order-utils.php' );
				$customer_display
					= yoink_inv_fetch_customer_display( $order_id,
					$order_summary->customer_id );
				?>
                <p>
					<?php
					esc_html_e( '...from order', 'yoinkinventory' );
					?>
                    <a href="<?php echo admin_url( 'post.php?action=edit&post='
					                               . intval( $order_id ) ); ?>">
                        #<?php echo intval( $order_id ); ?></a>
					<?php
					echo esc_html( sprintf(
					// translators: %1$s: customer display name; %2$s: order post date; %3$s: shipping method.
						__( 'by %1$s on %2$s via %3$s',
							'yoinkinventory' ),
						$customer_display,
						get_date_from_gmt( $order_summary->post_date_gmt,
							'Y-m-d' ),
						$order_summary->shipping_method ) ); ?>
                    ?>
                </p>
                <p>
					<?php
					esc_html_e( 'No taking activity was recorded (e.g., via Yoink 
					Mobile) for the following items when the order was
					originally being filled.',
						'yoinkinventory' );
					?>
                </p>
                <p>
					<?php
					esc_html_e( 'Please do one of the following:',
						'yoinkinventory' );
					?>
                </p>
                <ul>
                    <li>
						<?php
						esc_html_e( 'Change the order status back to processing and
						finish filling the order as usual (e.g., using the pick
						list in Yoink Mobile)',
							'yoinkinventory' );
						?>
                    </li>
                    <!-- <li>
                        <?php
					//_e( 'Commandeer the items (e.g., via Yoink Mobile) and ship them.', 'yoinkinventory' );
					?>
                    </li>
                    <li>
                        <?php
					//_e( 'Ship pre-received items before they\'re recorded into a stash location ' .
					//	'(e.g., via Yoink Mobile)', 'yoinkinventory' );
					?>
                    </li> -->
                    <li>
						<?php
						esc_html_e( 'Issue a refund for the items.',
							'yoinkinventory' );
						?>
                    </li>
                </ul>

                <p>
					<?php
					esc_html_e( 'Either way, when you\'re done, return here and
					reduce the remaining quantities. Setting all items to zero
					will clear this reminder.',
						'yoinkinventory' );
					?>
                </p>

                <form method="post">
                    <!-- <input type="hidden" name="order_id" value="<?php echo esc_attr( $order_id ); ?>"></input> -->
                    <table>
                        <thead>
                        <tr>
                            <th class="unf_name"><?php esc_html_e( 'Unfilled item',
									'yoinkinventory' ); ?></th>
                            <th><?php esc_html_e( 'Remaining, unfilled quantity',
									'yoinkinventory' ); ?></th>
                        </tr>
                        </thead>
						<?php
						foreach ( $unfilled_items as $row ) {
							?>
                            <tr>
                                <td class="unf_name">
                                    <input name="order_item_id[]" type="hidden"
                                           value="<?php echo esc_attr( $row->order_item_id ); ?>"/>
									<?php esc_html( $row->order_item_name ); ?>
                                </td>
                                <td>
                                    <input name="remaining_qty[]" type="number"
                                           min="0"
                                           max="<?php echo esc_attr( $row->remaining_quantity ); ?>"
                                           value="<?php echo esc_attr( $row->remaining_quantity ); ?>"/>
                                </td>
                            </tr>
							<?php
						}
						?>
                        <tr>
                            <td class="unf_name">
                            </td>
                            <td>
                                <input class="btn" type="submit" name="Submit"
                                       value="<?php esc_attr_e( 'Submit',
									       'yoinkinventory' ); ?>"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
			<?php
		}
	}

	private static function yoink_inv_handle_order_post() {
		// $order_id = intval( $_POST['order_id'] );
		if ( is_array( $_POST['order_item_id'] ) ) {
			$order_item_ids = array_map( 'intval', $_POST['order_item_id'] );
		} else {
			$order_item_ids = [ intval( $_POST['order_item_id'] ) ];
		}
		if ( is_array( $_POST['remaining_qty'] ) ) {
			$remaining_qtys = array_map( 'intval', $_POST['remaining_qty'] );
		} else {
			$remaining_qtys = [ intval( $_POST['remaining_qty'] ) ];
		}

		if ( count( $order_item_ids ) != count( $remaining_qtys ) ) {
			wp_die( esc_html( sprintf(
			// translators: %1$d: the number of posted order items; %2$d: the number of posted quantity values
				__( 'The number of posted order item IDs (%1$d) does not match the number of remaining quantities (%2$d).',
					'yoinkinventory' ),
				count( $order_item_ids ), count( $remaining_qtys ) ) ) );
		}

		global $wpdb;

		$num_items = count( $order_item_ids );
		for ( $i = 0; $i < $num_items; $i ++ ) {
			// echo $order_item_ids[ $i ] . ' -&gt; ' . $remaining_qtys[ $i ] . '<br/>';
			$remaining_qty = $remaining_qtys[ $i ];
			$order_item_id = $order_item_ids[ $i ];
			if ( $remaining_qty < 1 ) {
				$wpdb->query( $wpdb->prepare(
					"DELETE FROM {$wpdb->prefix}yoinkinventory_unfilled_items
						WHERE order_item_id = %d",
					$order_item_id ) );
			} else {
				$wpdb->query( $wpdb->prepare(
					"UPDATE {$wpdb->prefix}yoinkinventory_unfilled_items
						SET remaining_quantity = %d
						WHERE order_item_id = %d"
					, $remaining_qty, $order_item_id ) );
			}
		}
	}

	private static function yoink_inv_persist_alerts_addy() {
		// ToDo:
		//   1) Disable WordPress phpcs rules (rename phpcs.ruleset.xml).
		//   2) Finish addressing items in hotmail (current task: Generic function (and/or define) names
		//        All plugins must have unique function names, namespaces, defines, and classnames. )
		//   3) Checkpoint work on this branch, 'openSourceFixes'
		//   4) Fix N+1 above
		//   4) After everything is good, submit plugin or...
		//   5) Re-enable WordPress phpcs rules. See how hard it is to fix
		//      everything. Maybe fix in tandem with submission or see if they balk
		//      over old formatting.
		//   98) Note 'introductory pricing'
		//   99) Once WP plugin in registry and Flutter app released, give cold-call
		//       users a discount code so we can tell which ones accept.
		$addy = sanitize_email( trim( $_POST['unfilled_alerts_addy'] ) );
		if ( '' == $addy ) {
			if ( $addy == trim( $_POST['unfilled_alerts_addy'] ) ) {
				delete_option( 'yoink_unfilled_alerts_addy' );
			} else {
				wp_die( esc_html( sprintf(
				// translators: %1$s: the malformed email address
					__( 'Received an invalid email address for unfilled alerts: \'%1$s\'',
						'yoinkinventory' ),
					$_POST['unfilled_alerts_addy'] ) ) );
			}
		} else {
			update_option( 'yoink_unfilled_alerts_addy', $addy );
		}
	}

	public static function yoink_inv_display() {
		if ( ! current_user_can( 'yoink_review_unfilled' ) ) {
			?>
            <h1><?php esc_html_e( 'Access Denied.', 'yoinkinventory' ); ?></h1>
			<?php
			return;
		}

		if ( isset( $_POST['order_item_id'] ) ) {
			self::yoink_inv_handle_order_post();
			self::yoink_inv_display_summaries();
		} elseif ( isset( $_POST['unfilled_alerts_addy'] ) ) {
			self::yoink_inv_persist_alerts_addy();
			self::yoink_inv_display_summaries();
		} elseif ( isset( $_GET['order_id'] ) ) {
			self::yoink_inv_display_order( intval( $_GET['order_id'] ) );
		} else {
			self::yoink_inv_display_summaries();
		}
	}
}
