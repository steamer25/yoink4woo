<?php
// Copyright: © 2017 Brett Stime.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// ToDo: Use printf with each invocation of esc_html under entire AdminViews/ dir.

class YoinkInvWarehouseView {
	private static function yoink_inv_maybe_add_required_field_error(
		$is_required, &$errors, $key, $fieldMeta
	) {
		if ( $is_required ) {
			$errors[ $key ] = $fieldMeta['label'] . ' '
			                  . __( 'is a required field.', 'yoinkinventory' );
		}
	}

//	private static function yoink_inv_is_unset(
//		$is_required, &$errors, $key, $fieldMeta
//	) {
//		if ( isset( $_POST[ $key ] ) ) {
//			return false;
//		} else {
//			self::yoink_inv_maybe_add_required_field_error( $is_required,
//				$errors, $key, $fieldMeta );
//
//			return true;
//		}
//	}

//	private static function yoink_inv_is_empty(
//		$value, $is_required, &$errors, $key, $fieldMeta
//	) {
//		if ( '' == $value ) {
//			self::yoink_inv_maybe_add_required_field_error( $is_required,
//				$errors, $key, $fieldMeta );
//
//			return true;
//		}
//
//		return false;
//	}

//	private static function yoink_inv_try_get_posted_text_field(
//		&$errors, $key, $fieldMeta
//	) {
//		// YoinkInvWooPlugin::yoink_inv_debug_log($key . '=>' . $value);
//
//		$is_required = ! empty( $fieldMeta['required'] );
//
//		if ( self::yoink_inv_is_unset( $is_required, $errors, $key,
//			$fieldMeta )
//		) {
//			return null;
//		}
//
//		$value = sanitize_text_field( trim( $_POST[ $key ] ) );
//
//		if ( self::yoink_inv_is_empty( $value, $is_required, $errors, $key,
//			$fieldMeta )
//		) {
//			return null;
//		}
//
//		return $value;
//	}

//	private static function yoink_inv_try_get_posted_addy(
//		&$errors, $key, $fieldMeta
//	) {
//		// YoinkInvWooPlugin::yoink_inv_debug_log($key . '=>' . $value);
//
//		$is_required = ! empty( $fieldMeta['required'] );
//
//		if ( self::yoink_inv_is_unset( $is_required, $errors, $key,
//			$fieldMeta )
//		) {
//			return null;
//		}
//
//		$value = sanitize_email( trim( $_POST[ $key ] ) );
//
//		if ( self::yoink_inv_is_empty( $value, $is_required, $errors, $key,
//			$fieldMeta )
//		) {
//			return null;
//		}
//
//		return $value;
//	}

	private static function add_error_if_required_but_empty(
		$errors, $fieldMeta, $value, $invalid_code_prefix
	) {
		if ( $fieldMeta['required'] && $value == '' ) {
			$errors->add( $invalid_code_prefix . '_is_required',
				// translators: %s: The label for the required field that was not provided in the request.
				sprintf( __( '%s is a required field',
					'yoinkinventory' ), $fieldMeta['label'] ) );
		}
	}

	private static function nullify_if( $predicate_result, $value ) {
		if ( $predicate_result ) {
			return null;
		}

		return $value;
	}

	private static function yoink_inv_display_warehouse_details_form(
		$warehouse_id, $warehouse_name, $alerts_addy, $address1,
		$address2, $city, $state, $postcode, $country, $errors = null
	) {
		$countries         = WC()->countries;
		$countryFieldMeta
		                   = $countries->get_default_address_fields()['country'];
		$addressFieldMetas = $countries->get_address_fields( $country,
			'shipping_' );

		$address1FieldMeta                = $addressFieldMetas['shipping_address_1'];
		$address1FieldMeta['label_class'] = array( 'first' );

		$address2FieldMeta                = $addressFieldMetas['shipping_address_2'];
		$address2Label                    = __( 'Address 2', 'yoinkinventory' );
		$address2FieldMeta['label']       = $address2Label;
		$address2FieldMeta['label_class'] = array( 'first' );

		$cityFieldMeta                = $addressFieldMetas['shipping_city'];
		$cityFieldMeta['label_class'] = array( 'first' );

		$stateFieldMeta = $addressFieldMetas['shipping_state'];

		$postcodeFieldMeta = $addressFieldMetas['shipping_postcode'];

		$countryFieldMeta['label_class'] = array( 'first' );

		if ( null == $warehouse_id ) {
			?>
            <h1><?php esc_html_e( 'Add a warehouse', 'yoinkinventory' ); ?></h1>
		<?php } else { ?>
            <h1><?php esc_html_e( 'Edit warehouse', 'yoinkinventory' ); ?></h1>
		<?php } ?>
		<?php if ( null != $warehouse_id ) { ?>
            <input type="hidden" name="warehouse_id"
                   value="<?php echo esc_attr( $warehouse_id ); ?>"/>
		<?php } ?>
        <label for="warehouse_name"><?php esc_html_e( 'Name',
				'yoinkinventory' ); ?>
            <abbr class="required" title="required">*</abbr>
            <input id="warehouse_name" name="warehouse_name" type="text"
                   value="<?php echo esc_attr( $warehouse_name ); ?>"/>
        </label>

        <label for="alerts_addy"><?php esc_html_e( 'Alerts Email Address',
				'yoinkinventory' ); ?>
            <!-- <abbr class="required" title="required">*</abbr> -->
            <input id="alerts_addy" name="alerts_addy" type="text"
                   value="<?php echo esc_attr( $alerts_addy ); ?>"/>
        </label>
        <!-- <?php error_log( 'aa: ' . esc_html( $alerts_addy ) ); ?> -->

        <fieldset name="location" class="location">
            <legend><?php esc_html_e( 'Location',
					'yoinkinventory' ); ?></legend>
            <input type="hidden" name="prevCountry"
                   value="<?php echo esc_attr( $country ); ?>"/>
			<?php woocommerce_form_field( 'shipping_address_1',
				$address1FieldMeta, $address1 ); ?><br/>
			<?php woocommerce_form_field( 'shipping_address_2',
				$address2FieldMeta, $address2 ); ?><br/>
			<?php woocommerce_form_field( 'shipping_city', $cityFieldMeta,
				$city ); ?>
			<?php woocommerce_form_field( 'shipping_state', $stateFieldMeta,
				$state ); ?>
			<?php woocommerce_form_field( 'shipping_postcode',
				$postcodeFieldMeta, $postcode ); ?><br/>
			<?php woocommerce_form_field( 'shipping_country', $countryFieldMeta,
				$country ); ?>
        </fieldset>

		<?php
		$onlyPassedCountry = $country != null && '' == $warehouse_name
		                     && '' == $address1
		                     && '' == $address2
		                     && '' == $city
		                     && '' == $state
		                     && '' == $postcode
		                     && '' == $alerts_addy;
		if ( ( is_wp_error( $errors ) && $errors->has_errors() )
		     && ! $onlyPassedCountry
		) {
			echo '<ul class="problems">';
			foreach ( $errors->get_error_messages() as $error ) {
				echo '<li>' . esc_html( $error ) . '</li>';
			}
			echo '</ul>';
		}
		?>

        <p class="submit">
            <input type="submit" name="Submit"
                   value="<?php esc_attr_e( 'Submit', 'yoinkinventory' ); ?>"/>
        </p>
		<?php
	}

	public static function yoink_inv_display() {
		if ( ! current_user_can( 'crud_multiple_warehouses' ) ) {
			wp_die( esc_html__( 'Sorry, you are not allowed to add warehouses or edit their locations.',
				'yoinkinventory' ) );
		}
		?>

        <form class="yoink warehouse" method="post">
			<?php

			$countries = WC()->countries;

			$countryFieldMeta
				    = $countries->get_default_address_fields()['country'];
			$errors = new WP_Error();

			$hideDefaultDisplay = false;
			if ( 'POST' === $_SERVER['REQUEST_METHOD'] ) {
				$country             = null;
				$didFindCountryMatch = false;
				if ( isset( $_POST['shipping_country'] ) ) {
					$country
						= sanitize_text_field( $_POST['shipping_country'] );
				}
				if ( ! isset( $countries->get_countries()[ $country ] ) ) {
					$errors->add( 'unknown_country_code',
						// translators: %s: A requested country code.
						sprintf( __( 'The country code \'%s\' didn\'t match that of any known country.',
							'yoinkinventory' ), $country ) );
				}

				$addressFieldMetas = $countries->get_address_fields( $country,
					'shipping_' );

				$address1FieldMeta                = $addressFieldMetas['shipping_address_1'];
				$address1FieldMeta['label_class'] = array( 'first' );

				$address2FieldMeta                = $addressFieldMetas['shipping_address_2'];
				$address2Label                    = __( 'Address 2',
					'yoinkinventory' );
				$address2FieldMeta['label']       = $address2Label;
				$address2FieldMeta['label_class'] = array( 'first' );

				$cityFieldMeta                = $addressFieldMetas['shipping_city'];
				$cityFieldMeta['label_class'] = array( 'first' );

				$stateFieldMeta = $addressFieldMetas['shipping_state'];

				$postcodeFieldMeta
					= $addressFieldMetas['shipping_postcode'];

				$countryFieldMeta['label_class'] = array( 'first' );
				// YoinkInvWooPlugin::yoink_inv_debug_log($addressFieldMetas);

				$name = '';
				if ( isset( $_POST['warehouse_name'] ) ) {
					$name = sanitize_text_field( $_POST['warehouse_name'] );
				}
				if ( $name == '' ) {
					$errors->add( 'missing_warehouse_name',
						__( 'Name is a required field',
							'yoinkinventory' ) );
				}

				$alerts_addy = '';
				if ( isset( $_POST['alerts_addy'] ) ) {
					$alerts_addy = sanitize_email( $_POST['alerts_addy'] );
				}

//					$name
//						= self::yoink_inv_try_get_posted_text_field( $errors_arr,
//						'warehouse_name', array(
//							'label'    => __( 'Name', 'yoinkinventory' ),
//							'required' => true,
//						) );
//					$alerts_addy
//						= self::yoink_inv_try_get_posted_addy( $errors_arr,
//						'alerts_addy', array(
//							'label'    => __( 'Alerts Email Address',
//								'yoinkinventory' ),
//							'required' => false,
//						) );
				$address1 = '';
				if ( isset( $_POST['shipping_address_1'] ) ) {
					$address1
						= sanitize_text_field( $_POST['shipping_address_1'] );
				}
				self::add_error_if_required_but_empty(
					$errors, $address1FieldMeta, $address1, 'shipping_address_1'
				);

				$address2 = '';
				if ( isset( $_POST['shipping_address_2'] ) ) {
					$address2
						= sanitize_text_field( $_POST['shipping_address_2'] );
				}
				self::add_error_if_required_but_empty( $errors,
					$address2FieldMeta, $address2, 'shipping_address_2' );

				$city = '';
				if ( isset( $_POST['shipping_city'] ) ) {
					$city = sanitize_text_field( $_POST['shipping_city'] );
				}
				self::add_error_if_required_but_empty( $errors, $cityFieldMeta,
					$city, 'shipping_city' );

				$state = '';
				if ( isset( $_POST['shipping_state'] ) ) {
					$state = sanitize_text_field( $_POST['shipping_state'] );
				}
				self::add_error_if_required_but_empty( $errors, $stateFieldMeta,
					$state, 'shipping_state' );

				$postcode = '';
				if ( isset( $_POST['shipping_postcode'] ) ) {
					$postcode
						= sanitize_text_field( $_POST['shipping_postcode'] );
				}
				self::add_error_if_required_but_empty( $errors,
					$postcodeFieldMeta, $postcode, 'shipping_postcode' );

//				$address1
//					= self::yoink_inv_try_get_posted_text_field( $errors_arr,
//					'shipping_address_1', $address1FieldMeta );
//				$address2
//					= self::yoink_inv_try_get_posted_text_field( $errors_arr,
//					'shipping_address_2', $address2FieldMeta );
//				$city
//					= self::yoink_inv_try_get_posted_text_field( $errors_arr,
//					'shipping_city', $cityFieldMeta );
//				$state
//					= self::yoink_inv_try_get_posted_text_field( $errors_arr,
//					'shipping_state', $stateFieldMeta );
//				$postcode
//					= self::yoink_inv_try_get_posted_text_field( $errors_arr,
//					'shipping_postcode', $postcodeFieldMeta );

				$warehouse_id_or_error = null;
				// error_log('a');
				if ( isset( $_POST['warehouse_id'] ) ) {
					$warehouse_id_or_error
						= yoink_inv_try_parse_positive_int( $_POST['warehouse_id'] );
					if ( is_wp_error( $warehouse_id_or_error ) ) {
						$errors->merge_from( $warehouse_id_or_error );
						$warehouse_id_or_error = null;
					}
				} else {
					$errors->add( 'warehouse_id_is_required',
						__( 'A required field, wareshouse_id, was not included in the request',
							'yoinkinventory' ) );
				}

				if ( ! $errors->has_errors() ) {
					include_once( YOINK_INV_ABSPATH
					              . 'src/Classes/db-entities/yoink-warehouse.php' );
					$warehouse = new YoinkInvWarehouse( $warehouse_id_or_error,
						$name,
						self::nullify_if( ! is_email( $alerts_addy ),
							$alerts_addy ),
						$address1, $city,
						self::nullify_if( $state == '', $state ),
						self::nullify_if( $postcode == '', $postcode ),
						$country,
						self::nullify_if( $address2 == '', $address2 ) );
					$warehouse->yoink_inv_save();
				} else {
					$hideDefaultDisplay = true;
					self::yoink_inv_display_warehouse_details_form(
						$warehouse_id_or_error, $name, $alerts_addy, $address1,
						$address2, $city, $state, $postcode, $country,
						$errors );
				}
			} elseif ( isset( $_GET['warehouse_id'] ) ) {
				$hideDefaultDisplay    = true;
				$warehouse_id_or_error = intval( $_GET['warehouse_id'] );
				global $wpdb;
				$warehouse = $wpdb->get_row( $wpdb->prepare(
					"SELECT name, alerts_email_addy, address1, address2, city, state, postcode, country_code
						FROM {$wpdb->prefix}yoinkinventory_warehouses
						WHERE warehouse_id = %d",
					$warehouse_id_or_error ) );
				$_POST['shipping_country']
				           = $warehouse->country_code; // Need $_POST to have country for WC field meta;

				self::yoink_inv_display_warehouse_details_form( $warehouse_id_or_error,
					$warehouse->name, $warehouse->alerts_email_addy,
					$warehouse->address1, $warehouse->address2,
					$warehouse->city, $warehouse->state,
					$warehouse->postcode,
					$warehouse->country_code, null );
			}

			if ( ! $hideDefaultDisplay ) {
				global $wpdb;
				$warehouses = $wpdb->get_results(
					"SELECT warehouse_id, name, city, state, country_code
				FROM {$wpdb->prefix}yoinkinventory_warehouses" );

				if ( count( $warehouses ) > 0 ) {
					?>
                    <h1><?php esc_html_e( 'Existing warehouse(s)',
							'yoinkinventory' ); ?></h1>
                    <table class="warehouses">
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                            <th>Country</th>
                        </tr>
						<?php foreach ( $warehouses as $warehouse ) { ?>
                            <tr>
                                <td>
                                    <a href="?page=manage_yoink_warehouses&warehouse_id=<?php echo $warehouse->warehouse_id; ?>">
										<?php echo esc_html( $warehouse->name ); ?>
                                    </a>
                                </td>
                                <td><?php echo esc_html( $warehouse->city )
								               . ', '
								               . esc_html( $warehouse->state ); ?></td>
                                <td><?php echo esc_html( $warehouse->country_code ); ?></td>
                            </tr>
						<?php } ?>
                    </table>
				<?php } ?>

                <h1><?php esc_html_e( 'To add a new warehouse, start by choosing a country',
						'yoinkinventory' ); ?></h1>
				<?php woocommerce_form_field( 'shipping_country',
					$countryFieldMeta ); ?>

                <input type="submit" class="btn"
                       value="<?php esc_attr_e( 'Next',
					       'yoinkinventory' ); ?>"/>
			<?php } ?>
        </form>
		<?php
	}
}
