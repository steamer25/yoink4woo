<?php

namespace YoinkInv\Classes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvCsvStashEntry {
	public $row;
	public $stash_parts;
	public $stash_part_count;
	public $display;
	public $user_id = null;
	public $order_id = null;

	public function __construct( $row ) {
		$this->row              = $row;
		$stash_id               = $row->stash_id;
		$this->stash_parts      = array_map( function ( $x ) {
			return intval( $x );
		}, explode( ',', $stash_id ) );
		$this->stash_part_count = count( $this->stash_parts );
		$this->display          = implode( ', ', $this->stash_parts );
	}
}