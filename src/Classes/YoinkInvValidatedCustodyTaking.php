<?php

namespace YoinkInv\Classes;

use WP_Error;

defined( 'ABSPATH' ) || exit;

// Copyright: © 2023 Brett Stime.
class YoinkInvValidatedCustodyTaking {
	private $_stash_id;
	private $_order_item_id;
	private $_product_id;
	private $_quantity;

	public function __construct(
		$stash_id, $order_item_id, $product_id, $quantity
	) {
		$this->_stash_id      = $stash_id;
		$this->_order_item_id = $order_item_id;
		$this->_product_id    = $product_id;
		$this->_quantity      = $quantity;
	}

	public static function sanitize_list_from_encoded_json(
		$is_taking_for_order, $maybe_json_string
	) {
		$decoded_takings_or_wp_error
			= yoink_inv_try_parse_sequential_array_json( $maybe_json_string );
		if ( is_wp_error( $decoded_takings_or_wp_error ) ) {
			return $decoded_takings_or_wp_error;
		}

		$list_error_summaries = array();
		$list_error_details   = array();
		$taking_objs          = array();

		foreach (
			array_values( $decoded_takings_or_wp_error ) as $i =>
			$decoded_taking
		) {
			$invalid_params = array();

			$maybe_quantity
				= yoink_inv_try_parse_positive_int( $decoded_taking->quantity );
			if ( is_wp_error( $maybe_quantity ) ) {
				$invalid_params['quantity']
					= $maybe_quantity->get_error_messages();
			}

			$maybe_product_id
				= yoink_inv_try_parse_positive_int( $decoded_taking->productId );
			if ( is_wp_error( $maybe_product_id ) ) {
				$invalid_params['productId']
					= $maybe_product_id->get_error_messages();
			}

			if ( $is_taking_for_order ) {
				$maybe_order_item_id
					= yoink_inv_try_parse_positive_int( $decoded_taking->orderItemId );
				if ( is_wp_error( $maybe_order_item_id ) ) {
					$invalid_params['orderItemId']
						// translators: %s: A value that was expected to be a positive integer
						= sprintf( __( 'The value \'%s\' was expected to be a positive integer when taking for an order.',
						'yoinkinventory' ), $decoded_taking->orderItemId );
				}
			} else {
				$maybe_order_item_id = null;
			}

			$maybe_stash_id
				= yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid( strval( $decoded_taking->stashId ) );
			if ( is_wp_error( $maybe_stash_id ) ) {
				$invalid_params['stashId']
					= $maybe_stash_id->get_error_messages();
			}

			if ( $invalid_params ) {
				$list_error_summaries[ $i ] = new WP_Error(
					'yoink_inv_invalid_taking_request',
					/* translators: %s: List of invalid parameters. */
					sprintf( __( 'Invalid parameter(s): %s', 'yoinkinventory' ),
						implode( ', ', array_keys( $invalid_params ) ) ),
					array(
						'params' => $invalid_params
					)
				);
				$list_error_details[ $i ]   = __( 'Invalid taking entry',
					'yoinkinventory' );
				continue;
			}

			$taking_objs[]
				= new YoinkInvValidatedCustodyTaking( $maybe_stash_id,
				$maybe_order_item_id, $maybe_product_id, $maybe_quantity );
		}
//
//
//		$maybe_errs = new YoinkInvMultiErrorException();


		if ( count( $list_error_summaries ) > 0 ) {
			return new WP_Error(
				'rest_invalid_param',
				/* translators: %s: List of invalid entry indexes. */
				sprintf( __( 'Invalid list entry indexes: %s',
					'yoinkinventory' ),
					implode( ', ', array_keys( $list_error_summaries ) ) ),
				array(
					'status'  => 400,
					'params'  => $list_error_details,
					'details' => $list_error_summaries,
				)
			);

//			return new WP_Error(
//				'yoink_inv_invalid_custody_taking',
//				// translators: %1$s: A value that was expected to be a stash ID
//				sprintf( __( 'The string \'%1$s\' is not a valid stash ID',
//					'yoinkinventory' ), $some_string )
//			);
		}

		return $taking_objs;
	}

	public function get_stash_id() {
		return $this->_stash_id;
	}

	public function get_order_item_id() {
		return $this->_order_item_id;
	}

	public function get_product_id() {
		return $this->_product_id;
	}

	public function get_quantity() {
		return $this->_quantity;
	}
}