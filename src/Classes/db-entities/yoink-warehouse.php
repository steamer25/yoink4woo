<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvWarehouse {
	private $_id;
	private $_name;
	private $_alerts_addy;
	private $_address1;
	private $_address2;
	private $_city;
	private $_state;
	private $_postcode;
	private $_country_code;

	public function __construct(
		$id, $name, $alerts_addy, $address1, $city, $state, $postcode,
		$country_code,
		$address2 = null
	) {
		if ( null == $name ) {
			throw new InvalidArgumentException( "Warehouse name can not be null" );
		}

		if ( null == $address1 ) {
			throw new InvalidArgumentException( "Warehouse address1 can not be null" );
		}

		if ( null == $city ) {
			throw new InvalidArgumentException( "Warehouse city can not be null" );
		}

		if ( null == $country_code ) {
			throw new InvalidArgumentException( "Warehouse country can not be null" );
		}

		$this->_id           = $id;
		$this->_name         = $name;
		$this->_alerts_addy  = $alerts_addy;
		$this->_address1     = $address1;
		$this->_city         = $city;
		$this->_state        = $state;
		$this->_postcode     = $postcode;
		$this->_country_code = $country_code;
		$this->_address2     = $address2;
	}

	public function yoink_inv_save() {
		global $wpdb;

		if ( null == $this->_id ) {
			$wpdb->insert( $wpdb->prefix . 'yoinkinventory_warehouses', array(
				'name'              => $this->_name,
				'address1'          => $this->_address1,
				'address2'          => $this->_address2,
				'city'              => $this->_city,
				'state'             => $this->_state,
				'postcode'          => $this->_postcode,
				'country_code'      => $this->_country_code,
				'alerts_email_addy' => $this->_alerts_addy,
			), array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) );
		} else {
			$wpdb->update( "{$wpdb->prefix}yoinkinventory_warehouses",
				array(
					'name'              => $this->_name,
					'address1'          => $this->_address1,
					'address2'          => $this->_address2,
					'city'              => $this->_city,
					'state'             => $this->_state,
					'postcode'          => $this->_postcode,
					'country_code'      => $this->_country_code,
					'alerts_email_addy' => $this->_alerts_addy,
				),
				array(
					'warehouse_id' => $this->_id,
				),
				array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ),
				array( '%d' ) );
		}
	}
}
