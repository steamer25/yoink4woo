<?php

// Copyright: © 2023 Brett Stime.

//namespace YoinkInv;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function yoink_inv_format_timestamp_as_mysql_datetime( $ts ) {
	$dt = new DateTime();
	$dt->setTimestamp( $ts );
	return $dt->format( 'Y-m-d H:i:s' );
}
