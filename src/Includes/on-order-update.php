<?php

// Copyright: © 2017 Brett Stime.

namespace YoinkInv;

use WC_Email;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function yoink_inv_on_order_update( $order_id ) {
	// error_log( "YoinkInv\yoink_inv_on_order_update called. \$order_id: {$order_id};" );

	$order_status = wc_clean( sanitize_text_field( $_POST['order_status'] ) );
	if ( 'wc-completed' == $order_status ) {
		global $wpdb;

		$taken_state   = 2;
		$shipped_state = 3;

		$unfilled_requirements = array();
		$fill_count_by_item_id = array();

//		ToDo: Performance test to see if an index is worthwhile. Consider
//            telemetry. Closing orders (read side here) should be less frequent
//            than yoinking items (write side in REST API). Order context should
//            already be small to scan for item state.
		$fill_count_rows = $wpdb->get_results( $wpdb->prepare(
			"SELECT order_item_id, SUM(quantity) as qSum
				FROM {$wpdb->prefix}yoinkinventory_order_item_state
				WHERE order_id = %d AND state_id IN ({$taken_state}, {$shipped_state})
				GROUP BY order_item_id",
			$order_id ) );
		foreach ( $fill_count_rows as $fill_count_row ) {
			$fill_count_by_item_id[ $fill_count_row->order_item_id ]
				= intval( $fill_count_row->qSum );
		}

		$requirement_rows = $wpdb->get_results( $wpdb->prepare(
			"SELECT I.order_item_id, M.meta_value AS qty
				FROM {$wpdb->prefix}woocommerce_order_items I
					LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta M
						ON I.order_item_id = M.order_item_id
				WHERE I.order_id = %d AND I.order_item_type = 'line_item' AND M.meta_key = '_qty'",
			$order_id ) );

		foreach ( $requirement_rows as $requirement_row ) {
			$order_item_id = $requirement_row->order_item_id;
			if ( isset( $fill_count_by_item_id[ $order_item_id ] ) ) {
				$fill_count = $fill_count_by_item_id[ $order_item_id ];
			} else {
				$fill_count = 0;
			}

			$qty_needed = intval( $requirement_row->qty );
			if ( $qty_needed > $fill_count ) {
				$unfilled_requirements[] = array(
					'order_item_id'        => $order_item_id,
					'remaining_qty_needed' => $qty_needed - $fill_count,
				);
			}
		}

		if ( count( $unfilled_requirements ) > 0 ) {
			foreach ( $unfilled_requirements as $req ) {
				$remaining_qty = $req['remaining_qty_needed'];
				$wpdb->query( $wpdb->prepare(
					"INSERT INTO {$wpdb->prefix}yoinkinventory_unfilled_items
						(order_item_id, remaining_quantity) VALUES
						(%d, %d)
						ON DUPLICATE KEY UPDATE remaining_quantity = %d",
					$req['order_item_id'], $remaining_qty, $remaining_qty ) );
			}
			// $wpdb->get_results( $wpdb->prepare(
			// 		"SELECT
			// 		FROM {$wpdb->prefix}"
			// ) );
			$subject = __( 'Unfulfilled line items in order #',
					'yoinkinventory' ) . $order_id;
			$body    = __( 'Order #',
					'yoinkinventory' ) . $order_id . ' ' .
			           __( 'was marked as complete but some line items were
			           not signed off via e.g., Yoink Mobile. Please view the details at',
				           'yoinkinventory' )
			           . ' ' .
			           admin_url( 'admin.php?page=yoink_unfilled_items&order_id='
			                      . $order_id );

			$alert_addy = get_option( 'yoink_unfilled_alerts_addy', null );

			$wc_mail             = new WC_Email();
			$wc_mail->email_type = 'plain';
			// add_filter( 'wp_mail_content_type', yoink_inv_text_mail );

			if ( null == $alert_addy ) {
				$admins = get_users( array(
					'role'   => 'yoink_admin',
					'fields' => array( 'user_email' ),
				) );
				$addys  = array();
				foreach ( $admins as $admin ) {
					$addys[] = $admin->user_email;
				}
				if ( count( $addys ) > 0 ) {
					$wc_mail->send( $addys, $subject, $body, '', array() );
				} else {
					$admins = get_users( array(
						'role'   => 'administrator',
						'fields' => array( 'user_email' ),
					) );
					$addys  = array();
					foreach ( $admins as $admin ) {
						$addys[] = $admin->user_email;
					}
					if ( count( $addys ) > 0 ) {
						$wc_mail->send( $addys, $subject, $body, '', array() );
					}
				}
			} else {
				$wc_mail->send( $alert_addy, $subject, $body, '', array() );
			}
			// remove_filter( 'wp_mail_content_type', yoink_inv_text_mail );
		}
	}
}

function yoink_inv_text_mail() {
	return 'text/plain';
}
