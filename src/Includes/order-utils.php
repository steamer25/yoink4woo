<?php

// Copyright: © 2017 Brett Stime.

//namespace YoinkInv;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function yoink_inv_fetch_customer_display( $order_id, $customer_id ) {
	global $wpdb;

	// error_log( '$order_id: ' . $order_id );
	// error_log( '$customer_id: ' . $customer_id );

	if ( null != $customer_id && intval( $customer_id ) > 0 ) {
		// error_log( 'selecting display_name' );
		return $wpdb->get_var( $wpdb->prepare(
			"SELECT display_name
				FROM $wpdb->users
				WHERE ID = %d",
			$customer_id ) );
	} else {
		$order_meta         = $wpdb->get_results( $wpdb->prepare(
			"SELECT meta_key, meta_value
				FROM $wpdb->postmeta
				WHERE post_id = %d AND meta_key IN ('_billing_first_name', '_billing_last_name', '_billing_company')
				LIMIT 3",
			$order_id ) );
		$billing_first_name = null;
		$billing_last_name  = null;
		$billing_company    = null;

		foreach ( $order_meta as $m ) {
			switch ( $m->meta_key ) {
				case '_billing_first_name':
					$billing_first_name = $m->meta_value;
					break;
				case '_billing_last_name':
					$billing_last_name = $m->meta_value;
					break;
				case '_billing_company':
					$billing_company = $m->meta_value;
					break;
			}
		}

		if ( $billing_first_name || $billing_last_name ) {
			// error_log( 'billing name' );
			return trim( sprintf( '%1$s %2$s', $billing_first_name,
				$billing_last_name ) );
		} elseif ( $billing_company ) {
			// error_log( 'billing company' );
			return trim( $billing_company );
		} else {
			// error_log( 'guest' );
			return __( 'Guest', 'yoinkinventory' );
		}
	}
}

function yoink_inv_project_order_summary_row( $row ) {
	$order_id = intval( $row->ID );

	// ToDo: Bulk fetch customer displays instead of one at a time.
	$customer_display
		= yoink_inv_fetch_customer_display( $order_id,
		$row->customer_id );

	return array(
		// 14 bytes
		$order_id,
		// 9 bytes
		( new DateTime( $row->post_date_gmt ) )->getTimestamp(),
		// 10 bytes
		$customer_display,
		// 257 bytes
		$row->shipping_method,
		// 130 bytes
		intval( $row->state_id ),
		//2 bytes
		intval( $row->user_id ),
		// 9 bytes
		$row->claimant,
		// 130 bytes
	);
}
