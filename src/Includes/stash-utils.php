<?php

// Copyright: © 2023 Brett Stime.

//namespace YoinkInv;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function yoink_inv_remove_whitespace( $value ) {
	$result = preg_replace( '/\s+/u', '', $value );

//		YoinkInvWooPlugin::yoink_inv_debug_log( 'Removing whitespace. Before->' . $value . '<- After->' . $result. '<-');
	return $result;
}

function yoink_inv_is_valid_stash_id( $stash_id ) {
//	$stash_id = urldecode( $stash_id );
//		YoinkInvWooPlugin::yoink_inv_debug_log( 'Validating stash_id ->' . $stash_id . '<-');

	if ( preg_match( '/^\s*\d+(?:\s*,\s*\d+)*\s*$/u', $stash_id ) ) {
//			YoinkInvWooPlugin::yoink_inv_debug_log( 'Valid regular stash!');
		return true;
	}
	// if (preg_match('/^\s*u\d+(?:o\d+)?\s*$/u', $stash_id)) {
	// 	return true;
	// }
	// YoinkInvWooPlugin::yoink_inv_debug_log( 'Checking if the stash_id is a user custody' );
	if ( preg_match( '/^\s*u\d+\s*$/u', $stash_id ) ) {
//			YoinkInvWooPlugin::yoink_inv_debug_log( 'Valid personal custody stash!');
		return true;
	}

//		YoinkInvWooPlugin::yoink_inv_debug_log( 'Invalid stash ID!');
	return false;
}

function yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid(
	$some_string
) {
	$cleaned = yoink_inv_remove_whitespace( $some_string );

	if ( ! yoink_inv_is_valid_stash_id( $cleaned ) ) {
		return new WP_Error(
			'yoink_inv_invalid_stash_id_string',
			// translators: %1$s: A value that was expected to be a stash ID
			sprintf( __( 'The string \'%1$s\' is not a valid stash ID',
				'yoinkinventory' ), $some_string )
		);
	}

	return $cleaned;
}