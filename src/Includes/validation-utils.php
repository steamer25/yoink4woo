<?php

// Copyright: © 2023 Brett Stime.
//namespace YoinkInv;

//fwrite(STDOUT, 'TestA' );

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//include_once( YOINK_INV_ABSPATH . 'src/Includes/stash_utils.php' );

//function yoink_inv_is_non_negative( $value ) {
//	return is_numeric( $value ) && $value >= 0;
//}
function yoink_inv_is_sequential_array( $x ) {
	if ( ! is_array( $x ) ) {
		return false;
	}
	if ( $x === [] ) {
		return true;
	}
	if ( function_exists( 'array_is_list' ) ) {
		return array_is_list( $x );
	}

	return array_keys( $x ) === range( 0, count( $x ) - 1 );
}

function yoink_inv_try_parse_non_negative_int( $value ) {
	if ( is_numeric( $value ) ) {
		$parsed = intval( $value, 10 );
		if ( $parsed >= 0 ) {
			return $parsed;
		}
	}

	return new WP_Error(
		'yoink_inv_expected_non_negative_int',
		// translators: %s: A value that was expected to be a non-negative integer
		sprintf( __( 'The value \'%s\' was expected to be a non-negative integer',
			'yoinkinventory' ), $value )
	);
}


//function yoink_inv_is_positive_int( $value ) {
//	return is_numeric( $value ) && intval( $value, 10 ) > 0;
//}
function yoink_inv_try_parse_positive_int( $value ) {
	if ( is_numeric( $value ) ) {
		$parsed = intval( $value, 10 );
		if ( $parsed > 0 ) {
			return $parsed;
		}
	}

	return new WP_Error(
		'yoink_inv_expected_positive_int',
		// translators: %s: A value that was expected to be a positive integer
		sprintf( __( 'The value \'%s\' was expected to be a positive integer',
			'yoinkinventory' ), $value )
	);
}

function yoink_inv_try_parse_json( $maybe_json_string ) {
	$decoded = json_decode( $maybe_json_string );
	if ( json_last_error() != JSON_ERROR_NONE ) {
		return new WP_Error(
			'yoink_inv_invalid_json',
			// translators: %1$s: A value that was expected to be a stash ID. %2$d: The status number returned from the underlying system. %3$s: The error message from returned from the underlying system.
			sprintf( __( 'Error decoding JSON. Decode status: %1$d. Error message: %2$s. Input text: \'%3$s\'',
				'yoinkinventory' ), json_last_error(),
				json_last_error_msg(), $maybe_json_string )
		);
	}

	return $decoded;
}

function yoink_inv_try_parse_sequential_array_json( $maybe_json_string ) {
	$decoded_json_or_wp_error = yoink_inv_try_parse_json( $maybe_json_string );
	if( is_wp_error( $decoded_json_or_wp_error ) ) {
		return $decoded_json_or_wp_error;
	}

	if ( ! yoink_inv_is_sequential_array( $decoded_json_or_wp_error ) ) {
		return new WP_Error(
			'yoink_inv_expected_json_array',
			// translators: %1$s: The encoded string that was sent as part of the request.
			sprintf( __( 'The string \'%1$s\' was expected to be an array',
				'yoinkinventory' ), $maybe_json_string )
		);
	}

	return $decoded_json_or_wp_error;
}

function yoink_inv_sanitize_bool_default_false( $should_page_back_param ) {
	return isset( $should_page_back_param ) && $should_page_back_param;
}
