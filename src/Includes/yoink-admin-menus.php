<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'YoinkInvAdminMenus', false ) ) {
	class YoinkInvAdminMenus {
		private static function yoink_inv_get_mappings() {
			return array(
				'yoink_unfilled_items'    => array(
					'file'       => 'yoink-unfilled-items',
					'class'      => 'YoinkInvUnfilledItemsView',
					'title'      => __( 'Review Unfilled Items',
						'yoinkinventory' ),
					'capability' => 'yoink_review_unfilled',
				),
				'manage_yoink_warehouses' => array(
					'file'       => 'yoink-warehouse-view',
					'class'      => 'YoinkInvWarehouseView',
					'title'      => __( 'Edit Warehouse Locations',
						'yoinkinventory' ),
					'capability' => 'yoink_crud_multiple_warehouses',
				),
				'generate_yoink_qr_codes' => array(
					'file'       => 'generate-qr-codes-form-view',
					'class'      => 'YoinkInvGenerateQrCodesFormView',
					'title'      => __( 'Generate QR Codes', 'yoinkinventory' ),
					'capability' => 'yoink_generate_qr_codes',
					'hasAssets'  => true,
				),
				'add_yoink_device'        => array(
					'file'       => 'yoink-add-device-view',
					'class'      => 'YoinkInvAddDeviceView',
					'title'      => __( 'Add Yoink Mobile Device',
						'yoinkinventory' ),
					'capability' => 'yoink_register_device',
					'hasAssets'  => true,
				),
				'sync_stock_qtys'         => array(
					'file'       => 'sync-stock-qtys-view',
					'class'      => 'YoinkInvSyncStockQtysView',
					'title'      => __( 'Synchronize Woo Stock Quantities',
						'yoinkinventory' ),
					'capability' => 'yoink_sync_qtys',
				),
			);
		}

		public function __construct() {
			// grant_super_admin(1);
			add_action( 'admin_menu', array( $this, 'yoink_inv_admin_menu' ),
				9 );
		}

		public function yoink_inv_admin_menu() {
			// global $menu;

			// // if ( current_user_can( 'add_warehouse' ) ) {
			// 	$menu[] = array( '', 'read', 'separator-yoinkinventory', '', 'wp-menu-separator yoinkinventory' );
			// // }
			$yoink_inv_title = __( 'Yoink Inventory', 'yoinkinventory' );
			$yoink_inv_icon
			                 = 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTUwIiBoZWlnaHQ9IjE1MCIgdmlld0JveD0iMCAwIDE0MC42MjUgMTQwLjYyNSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZyBzdHlsZT0iZGlzcGxheTppbmxpbmUiPjxwYXRoIHN0eWxlPSJkaXNwbGF5OmlubGluZTtjb2xvcjojMDAwO2ZpbGw6I2E3YWFhZDtmaWxsLW9wYWNpdHk6MTstaW5rc2NhcGUtc3Ryb2tlOm5vbmUiIGQ9Im0tMTI3MC4yMy0xNzMuMTczIDc2LjY2NiAxNTEuMDEtMTMyLjYzMy00Ni41MjQtMTEuNzc2IDI3LjM3NCAxMjkuMTE1IDY2LjQyMmMxMi41MDktOS42MTIgMjUuNDcyLTE5LjE1NyAzNi4wOTItMjYuOTAzIDguMjc2LTYuMDM1IDEyLjY3My05LjMzIDE2Ljc0MS0xMi40MDhsLTI1LjE4NS00OS42MSAzNC4zOSA0Mi4wMzljMy42MjctMy4zMjUgOS43NTYtOS4xNzUgMTkuNjA4LTE4Ljc0OCAxLjkwMy0xLjg0OSA0LjYxLTQuMzkgNi42NTQtNi4zNzNsLS44NDgtMzYuNDczIDcuNiAzMC4wMDRjOC44MjctOC41MyAxNy40NS0xNi45MzQgMjcuMTYtMjYuMDE1IDEwLjg3OC0xMC4xNzIgMjEuNzY0LTIwLjA3OCAzMS41Ni0yOC40MjEgMS41MDQtMS4yODEgMi44NzctMi4zNzggNC4zMjYtMy41OGwxMS4wMzktMjEuNjI1LTMuNzA3IDE1LjY5N2M1LjAwNS0zLjkyIDkuNi03LjI4NCAxMy41NzktOS42NCAyLjI5OC0xLjM2IDQuNDAzLTIuNDQ2IDYuNjQ4LTMuMiAyLjI0Ni0uNzUzIDQuODctMS41NiA4LjQyNi0uMTI4YTIzLjcyMyAyMy43MjMgMCAwIDEgNC41MjYgMi40MzdsMjguMDQ0LTExOC43NzUtMjcuODM4LTEwLjIyNC04My4yMDUgMTYyLjk5Mi0zMS42OTEtMTI1LjA4My0yOS40MDggNC4wMTYgMy4yNzYgMTQwLjc2Ni05NC4yOC0xMTUuMjQ3em0tMTI0LjQxNyAyNDcuODUgNS45NDMgMjkuMjUgMTMxLjA3Mi0yMS4yNTRjLS4xMzctLjIyNS0uNC0uNTItLjUyOC0uNzQtMS43NDMtMy4wMjItMS40NTgtNi4zMjctLjc1OC04LjYzNi43LTIuMzEgMS43OTMtNC4xNzQgMy4xMDQtNi4wNzggMi42MjQtMy44MDggNi4yODItNy43MTYgMTAuNzk3LTEyLjA0OCA3LjYyMi03LjMxMiAxNy44NzItMTUuNyAyOC42NjEtMjQuMTc0em00NjguNTk1LTI0MS45MTEtNjIuNzEyIDYyLjU5OGE2OTEuOTU3IDY5MS45NTcgMCAwIDEgMTEuMjE3IDE0LjI3NWM0LjYxIDYuMDIgOC45IDExLjgxMyAxMy4yNjcgMTcuNjY4bDYwLjM4NS03NC41NzJ6TS0xMzI3LjM1IDE3Ny4zNGwxNy4zNiAyNC4yMDggMTAyLjE2My02Mi4yNmMtNS41NS01Ljk1My04LjU4NS05LjI2Mi0xNS4xNTgtMTYuMzU5LTYuMDQ4LTYuNTMtMTEuNjM1LTEyLjc0MS0xNi45NzQtMTguNzI5em0xMzQuMTgtMjIuMjM0LTEuMzM0IDMuODYzYzEuNTUzLS4xNiAzLjEwOS0uMzIyIDQuNTktLjM1MS0uNDgxLS41NDYtMi42MDktMi43OTgtMy4yNTUtMy41MTJ6bTI0Ny4yODMtMjAzLjYxM2MuNzg1Ljk4OSAyLjI2MyAzLjAyNCAyLjg5NCAzLjc3Ni4zMjMuMzg1LjkyNCAxLjAwMSAxLjI5NyAxLjQyN2E1My4zNSA1My4zNSAwIDAgMSAyLjU5LTYuMTJ6bS0yNzMuNjggMjc5Ljk2MS0xNC4wODUgNDAuNzM4IDI1Ljk4OCAxMy45NCA1LjU1Ni03LjI4M2MtLjA5OS0zLjI1Mi0uMDUyLTYuMTM4LjExLTguMDkyLS44NTgtMS44MDYtMS43OS0zLjY1LTMuNDQ2LTcuNzI4LTEuMTU0LTIuODQyLTIuMjgtNS44MTMtMy4xMi04Ljg5YTI5Ljk5MSAyOS45OTEgMCAwIDEtLjgzNC00LjM4OCAxMjkuMzA2IDEyOS4zMDYgMCAwIDEtMTAuMTY5LTE4LjI5N3ptMTEwLjcyNiAxLjI4IDIyLjYzOCAyOC43NzMgMTIuNzc1IDcuMjA1IDEwLjY5OC02OC4zMTVjLTcuMTc5IDYuMS0xNC4xNjMgMTEuODg4LTIwLjQ2MyAxNi43MjctNS4zIDQuMDcxLTEwLjE1NCA3LjU1My0xNC40NjggMTAuMjQ5LTQuMDM5IDIuNTI0LTcuNDA0IDQuMzkyLTExLjE4IDUuMzZ6bTIyNi4wODEtMTg4LjUzYy0uNjc1IDEuMTU4LTEuMzA0IDIuMzIzLTIuMTEyIDMuNDY5LTIuNzMxIDMuODctNi4zNTcgNy44NjItMTAuNjgzIDEyLjEyOS04LjY1NCA4LjUzMy0yMC4xMjcgMTguMDcyLTMyLjIxMiAyNy41NzQtMy40MjIgMi42OS02LjY1MSA1LjAyMi0xMC4xMjUgNy42ODNsMTA0Ljg2IDEuOTkxYy4yMzgtMS4wNjYuNDk5LTIuNTIyLjcyOC0zLjUzNS45NjItNC4yNDEgMS43MDktNy41ODggMi40MTctMTAuNDgtOC42MTUtOC44NDQtMTYuNTIzLTIwLjQ1NC0yMi40NTctMzEuMzQ0em0tMTA3LjE5IDkwLjcxYy01LjM3IDQuODMxLTEzLjQzOCAxMi4yODUtMjMuNjU4IDIxLjY4MWwxNDEuNTggNjkuMjU4YzQuMzQ0LTExLjU4NiA4Ljc4Ny0yMi4zNTkgMTMuMDMzLTMxLjQ5My4xNC0uMzAzLjI3LS41MzQuNDEtLjgzNGwtOTYuOTI0LTg1LjFjLTEwLjI4NyA3LjY4Mi0xNy43OSAxMy40NjktMjQuMjY1IDE4LjQ0OGwzMy40ODggMjkuNDAxem0tMzIuMTc3IDI5LjM4NmMtOC4zMjQgNy42MDgtMTYuNzI4IDE1LjMzLTI1LjcxNyAyMy4yNTQtMi43MjIgMi40LTUuMzU4IDQuNjE1LTguMDU4IDYuOTUzbDUxLjk5IDk5LjU4NiAyNy4xNy0xMi4yOTN6IiB0cmFuc2Zvcm09InJvdGF0ZSgzOS45ODIgLTEwMy4xMjcgMzQxLjg3Nykgc2NhbGUoLjE0OTMxKSIvPjxwYXRoIHN0eWxlPSJkaXNwbGF5OmlubGluZTtjb2xvcjojMDAwO2ZpbGw6I2E3YWFhZDtmaWxsLXJ1bGU6ZXZlbm9kZDstaW5rc2NhcGUtc3Ryb2tlOm5vbmUiIGQ9Ik00ODMuODg5IDI3My44MjZjLTE1LjI3NC0uMzUyLTM0LjgxLjM0OS01NC42MzkgMS40MjJzLTM5LjkxNyAyLjUyNi01Ni4wNSAzLjcwOWMtMTYuMTM0IDEuMTgzLTI4Ljg5NCAyLjA4LTMxLjg3OCAyLjA4LTMuMTY1IDAtMTQuMTk5LS43NDYtMjcuOTEyLTEuNjc2LTEzLjcxMy0uOTMtMzAuNTM3LTIuMDI3LTQ2Ljk5LTIuNjkzLTE2LjQ1NC0uNjY2LTMyLjQ4OC0uOTE2LTQ0Ljk3NS0uMDgtNi4yNDMuNDE4LTExLjU1NyAxLjA2Mi0xNi4wMTMgMi4yOTUtMi4yMjkuNjE2LTQuMjY2IDEuMzQyLTYuMjg2IDIuNjYyLTIuMDIgMS4zMi00LjM2IDMuNjY4LTQuOTY2IDcuMTAzLS44OTUgNS4wNzEtLjgyNSAxMi4xNjMtLjYzMSAyMS42OTIuMTkzIDkuNTI5LjY4IDIwLjk0OCAxLjI0MiAzMi4zMjYgMS4xMjMgMjIuNzU2IDIuNTM1IDQ1Ljk3OCAyLjUzNSA1MS40MzQgMCA1LjE1NC0xLjgzNyAyOC43MzItMS41MjUgNTEuMjguMTU2IDExLjI3NS44MjMgMjIuNDg3IDIuNjkxIDMxLjk2NiAxLjg2OCA5LjQ3OCA0LjIyNSAxOC4xNTMgMTIuNDgzIDIyLjQ3OCAzLjcxOCAxLjk0OCA3LjcgMi42OSAxMi43MzggMy4zOTcgNS4wMzguNzA2IDEwLjk5NSAxLjE1NyAxNy42NzIgMS40NDMgMTMuMzU0LjU3MiAyOS41NjYuNDY0IDQ2LjA4Ni4wNTUgMzIuOTY0LS44MTcgNjcuMjI5LTIuODUgODAuNzczLTMuMjUyIDEyLjA1Ny4wMDUgNDQuOTAyIDIuNjc3IDc1LjYwNCAzLjY0NCAxNS4zNjUuNDg0IDMwLjI4Ni41NDcgNDIuNC0uNDMxIDYuMDU3LS40OSAxMS4zOTktMS4yMTkgMTUuOTc5LTIuNDNzOC43MDMtMi41MzYgMTEuODk0LTYuNzA5YzQuNTM3LTUuOTMzIDUuNDg0LTEzLjIzIDYuMTU4LTIxLjg2LjY3NC04LjYyOS41MDUtMTguNTY0LjAxOC0yOC42NjEtLjk3NS0yMC4xOTUtMy4yMzMtNDEuMzgtMy4yMzMtNTAuOTIgMC05Ljc0NyAxLjgxOC0zNC4xMTQgMi44NDItNTcuMTY4LjUxMy0xMS41MjguODI3LTIyLjgwNy41OS0zMi4xODgtLjIzNy05LjM4LS4xMy0xNi4wNTQtMy40Ny0yMi4zMTYtMS44MDUtMy4zODMtNC4zMzUtNC40NTEtNi41NC01LjMxNy0yLjIwNC0uODY1LTQuNTE1LTEuMzg2LTcuMTUtMS44Mi01LjI3LS44NjgtMTEuODEtMS4yODktMTkuNDQ3LTEuNDY1em0tLjMzMiAxNC40OGM3LjI3LjE2OCAxMy4zNzMuNjA2IDE3LjQyNSAxLjI3NCAxLjY2NS4yNzQgMi42NzEuNTc4IDMuNDYuODMyLjI2MyAxLjQyOCAxLjM4IDcuMDU1IDEuNTc0IDE0LjY5Ny4yMiA4Ljc1Ny0uMDc1IDE5LjgxMy0uNTggMzEuMTgtMS4wMSAyMi43MzMtMi44NTYgNDYuMzkzLTIuODU2IDU3LjgxIDAgMTEuNjI0IDIuMzA1IDMyLjA0MiAzLjI1IDUxLjYxOC40NzIgOS43ODguNiAxOS4yODEuMDEgMjYuODM4LS41OSA3LjU1Ni0yLjQ2OCAxMy4yMDMtMy4yMjMgMTQuMTkuNTQ2LS43MTQtLjg4My42NTQtNC4wOTMgMS41MDMtMy4yMS44NS03Ljg5NyAxLjU0OC0xMy40NDQgMS45OTYtMTEuMDkzLjg5Ni0yNS42NzMuODY2LTQwLjc3NS4zOS0zMC4yMDQtLjk1LTYyLjEtMy42NTItNzYuMTIxLTMuNjUyaC0uMTA2bC0uMTA3LjAwMmMtMTQuMTk3LjQxOC00OC4xNjggMi40NDctODAuODYgMy4yNTYtMTYuMzQ1LjQwNS0zMi4zMzYuNS00NS4xMDctLjA0Ny02LjM4NS0uMjczLTExLjk3Mi0uNzEyLTE2LjI4LTEuMzE2LTQuMzA3LS42MDQtNy40NzQtMS41OTItOC4wMjgtMS44ODMtLjI5LS4xNTItMy40MjctNC41MDMtNC45OTMtMTIuNDUtMS41NjYtNy45NDUtMi4yNzItMTguNTU3LTIuNDIyLTI5LjM2Mi0uMjk4LTIxLjYxMiAxLjUyOC00My4yMTIgMS41MjgtNTEuMDgyIDAtNy41NjktMS40MzctMjkuNTI0LTIuNTUzLTUyLjE0OS0uNTU4LTExLjMxMi0xLjAzOC0yMi42MzQtMS4yMjYtMzEuOTA0LS4xNi03Ljg0OS4wMzYtMTMuODMuMjktMTYuODQyLjM3LS4xNDYuMzgtLjE5OC45NzEtLjM2MSAyLjcwNy0uNzQ5IDcuMzk2LTEuNDIyIDEzLjEyMS0xLjgwNSAxMS40NS0uNzY3IDI3LjIxMy0uNTU3IDQzLjQyMi4xIDE2LjIwOS42NTYgMzIuOTEyIDEuNzQ2IDQ2LjU5NiAyLjY3NCAxMy42ODMuOTI3IDIzLjkxNyAxLjcwNyAyOC44OTMgMS43MDcgNS4xNTYgMCAxNi44MTUtLjkzNSAzMi45MzctMi4xMTggMTYuMTIyLTEuMTgyIDM2LjEyMy0yLjYzIDU1Ljc3My0zLjY5MyAxOS42NTEtMS4wNjQgMzguOTg0LTEuNzM3IDUzLjUyNC0xLjQwMnoiIHRyYW5zZm9ybT0ibWF0cml4KC4xNDkzIDAgMCAuMTQ5MyAxNC43NjMgLTguMjUpIi8+PHBhdGggc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6I2E3YWFhZDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6I2E3YWFhZDtzdHJva2Utd2lkdGg6MS41NTk3NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiBkPSJNMTkzLjExMyAzNzEuMDY4Yy40OTcgMTEuMjcgNC40MzIgMzUuMjE0IDMuMTQ3IDQ3LjcxMiAxMi40NjUtLjI5NiAzMDkuNDQ1LTQuMTMgMzIwLjI3Ny0yLjIzNC44ODItMTkuNTEtLjgyMS00My45NjYtLjgyMS00My45NjYtOC43NjMuODI1LTMyMi42MDMtMS41MTItMzIyLjYwMy0xLjUxMnoiIHRyYW5zZm9ybT0ibWF0cml4KC4xNDkzIDAgMCAuMTQ5MyAxNC43NjMgLTguMjUpIi8+PHBhdGggc3R5bGU9ImNvbG9yOiMwMDA7ZGlzcGxheTppbmxpbmU7ZmlsbDojYTdhYWFkO2ZpbGwtcnVsZTpldmVub2RkOy1pbmtzY2FwZS1zdHJva2U6bm9uZSIgZD0iTTE2OS4zNDMgNDA0LjAyNGMtMS44MiAxLjI2OC00LjY1IDMuNzYzLTcuNTc5IDcuNjY1LTUuOTQgNy45MS0xMi40NzYgMjEuNDctMTQuMjM4IDQzLjAybC0uNjc0IDguMjktMy43MjkgMS4xMTZjLS41NDMgMS4zNC0xLjEzNSAyLjgyLTEuNzI5IDQuNjYtMS41NTcgNC44MjctMi43ODQgOS43MjUtMi43ODQgOS43MjVsLTEuMDA1IDQuMDcyLTMuMjk5IDIuNTg4cy0yMi44MTIgMTUuOTI0LTE0LjA1NCA1Mi40MTRsLjQ1NCAxLjkzOC0uMTcyIDEuOTg2Yy0uMzY0IDQuMDk1LTEuNTAyIDI3LjAyOC0yLjQyOCA0Ny43NDItLjkgMjAuMTAyLTEuNjM1IDM3LjgwNC0xLjY4IDM4LjkgNC42MzUgMjUuNzQ0IDEuMzEyIDQ5Ljk0OS01LjMxIDcyLjkyLS45MTIgMTUuNjY5LS44NzcgMTcuODY0LTIuMzMgMzguNjQyLTMuNDUxIDQ5LjMxNy04LjI1IDEwNS40MzctMTMuODQ2IDEzMy40MTVsLTQuODA3LS45NjljNS40NTItMjcuMjY0IDEwLjMxMy04My41MzIgMTMuNzYtMTMyLjc5Ljc0Mi0xMC42MTYuNTgxLTEwLjEyMyAxLjE4OS0xOS43NDQtMy45MTggMTAuNzAzLTguMjk0IDIxLjE1NS0xMi43NDIgMzEuMzIxLTE2LjQzNiAzNy41NjctMzIuODc1IDcyLjE0My0zMS4xMzcgMTA1LjM5M2wuMDg2IDEuNjU2LS4zNTYgMS42MTljLS43MjUgMy4yNy0yLjE3NyA4LjE4OC00LjI1NSAxNS45MTgtMi4wNzggNy43My00LjYyOSAxNy40ODUtNi45MyAyNy40OTUtMi4zIDEwLjAxLTQuMzQ5IDIwLjMwNC01LjQ0NCAyOC44MTktLjI3MyAyLjExNi0uMjM3IDMuNjcyLS4zNDQgNS41NTUgMTEzLjgwOS0yMC42MSAxNjUuMzQyLTIwLjQ5OSAyNzMuOTY4IDIuMTIyIDUuNzUtMTAuMSAyMy43MzEtNDIuMDk2IDMxLjEyNS02NC42MjlsMS4zLTMuOTQ5IDMuNDQ2LTIuMzNjMjUuOTY2LTE3LjYwNyA1Ni41ODUtMzQuNTI2IDgyLjUxLTUwLjM0MiAyNS45MjQtMTUuODE1IDQ3LjA4Ny0zMS44NDIgNTIuNTYtNDAuODVsMi45MDctNC43ODIgNS41MzEtLjkwOGMzMi43ODUtNS4zOTYgNDcuNzg4LTE3LjkxNiA3OC42MjItMzAuNDc1bDQuNTUtMS44NTIgMy4zNzIgMS4zNWMuNTEzLjAyNiAxLjI5NS4wNSAyLjM2NyAwIDIuMzY3LS4xMTEgNS42NzMtLjUyNCA5LjAzOC0xLjM4NiA2LjczMi0xLjcyNiAxMy4xMjYtNS4xNTcgMTYuMzEtMTAuMjlsMi4yNDUtMy42MyA0LjAzNS0xLjQyMmMzNS4yMDQtMTIuNDc3IDU4LjA3OS0yNy40NjMgNzAuNDMtNDIuMjExIDEwLjc5Ni0xMi44OTQgMTQuMjY3LTI1LjAzNSAxMi4yNjMtMzguMjE0LTkuOTQyLTIuODQ4LTIzLjkzNy0uODgzLTM4LjAzIDQuMTQ1LTE2LjUxIDUuODkyLTMxLjg5NiAxNC45MjctMzkuODU2IDE5LjQ3NWwtMjkuNTkyIDE2LjkxMWEyMTMxNi45MTQgMjEzMTYuOTE0IDAgMCAxLTIxLjYyIDM0LjM2M2wtLjQ0Mi43MzYtODMuNjg3IDQ3LjE2NS0zLjIzNy01LjczOSA4Mi4xOS00Ni4zNDRjLjc0NS0xLjIxMSAyMS4xOS0zNC4zNTYgMzIuNDEzLTUxLjUwN2w0Ny45MDEtNzkuNTU0Ljc2LS44NDZjMzAuNDU0LTM0LjIzIDM1Ljg3LTY4LjU1NiAzMi4wMDgtODAuODE3LS45NjUtMy4wNjUtMi4yMTUtNC41MTItMi45MTgtNS4wNTItLjcwNC0uNTQtLjkxNS0uNzQ5LTIuNjM3LS41NC0zLjQuNDEzLTEyLjgxNyA1LjI1Mi0yMy45MTQgMTkuNjU5bC0xMi45MjYgMTkuMDMzYy00LjY1MyAxMC44ODYtMTAuMDU1IDIzLjQxMi0xOS4xNTUgNDQuMDAxLTIyLjYzIDUxLjE5OS00OC4xODYgMTA3LjE0Mi02MS4wODUgMTI3LjgyM2wtLjU1Mi44ODMtLjk1Ny40MDVjLTcuNTI1IDMuMjIxLTIwLjYxOCAxMS4zNDMtMzYuMjYzIDIyLjIzNC0xNS42NDQgMTAuODkxLTMzLjk2NyAyNC41ODItNTIuNjEgMzkuMTA4LTM3LjI4NyAyOS4wNTQtNzUuODc0IDYxLjUxNS05Ni43ODUgODEuNjUxbC00LjU4Ni00Ljc1OGMyMS4yNzctMjAuNDkgNTkuODg3LTUyLjk0NCA5Ny4zMTEtODIuMTA1IDE4LjcxMi0xNC41OCAzNy4xMTctMjguMzI1IDUyLjkwNS0zOS4zMTcgMTUuMTk2LTEwLjU3OCAyNy43NC0xOC40NyAzNi4zNS0yMi4zNDQgMTIuMjU3LTE5LjkxNSAzNy44MzgtNzUuNTc0IDYwLjIzOC0xMjYuMjUzIDQuNjItMTAuNDUzIDcuMTk3LTE2LjQ1IDEwLjM2My0yMy43M2w2LjQyNi00Ny41MjEtLjIzMy0uMTM1czYuNjg2LTE4LjM4IDExLjgxLTM5LjU4N2MyLjU2MS0xMC42MDMgNC42OS0yMS44NzIgNS40Mi0zMS40MDcuNTI2LTYuODY5LjAyOC0xMi43NDItLjczNi0xNi40Mi05LjM3NS00LjQwNi0xNC41NzUtNS4zMzgtMTYuMzg0LTUuMjEzLTIuMDgxLjE0NC0yLjQ4Ny42NjctNi44NTUgMy45MTItMy4wNDUgMi4yNjItMTAuNzYyIDEzLjMwMy0xNS43OTYgMjMuMjAzLTUuMDM0IDkuOS04LjQ1IDE4LjYwNC04LjQ1IDE4LjYwNGwtOS4xNzIgMjMuNDg0LjYuNDE3Yy0uNzY5IDEuMTI1LTEuNDI0IDIuNDg1LTIuMTgyIDMuNjQzbC0uMTEuMjgyLS4wMjUtLjA2MmMtMTkuMDM2IDI5LjEzNS0zNC41MDcgNjcuODc0LTM4LjExNSA4OS41MjRsLS42MTQtLjA5OC4zNTYgMi4wMjQtNy40MDcgNC4yNTVjLjU4Ny0uMzM3LTUuOTU2IDQuMTE3LTEyLjg2NSA5LjM3YTc4NS4xMzUgNzg1LjEzNSAwIDAgMC0yMy43NTQgMTguODEyYy04LjI1NCA2LjgxMy0xNi4yMTQgMTMuNzQtMjEuNjA4IDE5LjA1Ny0yLjA1OSAyLjAzLTMuMjM4IDMuNDQxLTQuMzMgNC43MzRsNi41IDIxLjA0NC0yMi4zOC0xMC4xNDJjLS45NzMuMTkyLTIuMzguNTE0LTQuNDUzIDEuMTktNC43NjggMS41NTYtMTEuNjU2IDQuMzU5LTE5LjYzMyA4LjA5NC0xNS45NTYgNy40Ny0zNi40NzIgMTguNzA1LTU2LjQzNyAzMS42ODlsLTQuMzU0IDIuODMzLTUuMDY1LTEuMTljLjg0NC4xOTgtNC43NzctLjQtMTEuMzY4LS42NjJzLTE1LjE4MS0uNjE0LTI0LjkwNy0xLjUyYy0xNS4zMjctMS40My0zMy40OTktNC4yNDYtNTEuNjE4LTEwLjQyNSA5Ljg4NiA4LjYwNSAyMC41MzIgMTUuOTQ0IDMxLjg2MSAxOS4wMzNsLTEuNzY2IDYuNDYzYy0xNy43MTItNC44My0zMi42MzUtMTcuOTIzLTQ1LjQ2LTMxLjU3OGwtMS44NTMtLjg0Ny0xLjgyNy0yLjI5M2MtLjE3LS4yMTMtMy4zOTItMy41NjQtNy4xNzQtNy4wMTUtMy43ODItMy40NS04LjQzMi03LjUxLTEyLjkxNC0xMS4zNTYtOC45NjItNy42OTItMTcuMjU0LTE0LjUyLTE3LjI1NC0xNC41MmwtMS43MTctMS40MjItMTguODYyLTMyLjkwMy0yMS4zNjMtNTYuODMgMy42NjctMTE3LjUyMWMuMDAxLS4wMjUuMTItMi42Ni0yLjQwNC03LjA2NC0yLjMyNC00LjA1NS02LjgtOC45NjYtMTcuOTc4LTEyLjE5em0zNzEuNSA4LjgwNWMtMi4zMzguNDkxLTUuMDM1IDEuMTc5LTcuNjkgMi4yOTMtMy4xNTMgMS4zMjQtNS43ODggMy4wODctNy4yMzUgNC43Ny0xLjQ0NyAxLjY4NS0yLjE2NiAyLjk4Ny0yLjA2IDYuMzA0bC4wMjQuNDMtLjAyNC40MjljLS44OTkgMjMuNTE5IDYuMjQ3IDU3LjQxNCAyNC45OCA4My4yOTRsMy41NDUgNC44OTMtMS43NTQgNS43ODhjLTIuNDc4IDguMTYtNC41MDYgMjcuMDg3LTQuNTEzIDM5LjI1NiA2LjQ3Mi0yNC40NCAxOS40NTMtNTYuNDI5IDM3LjEzNC04Mi4zbC04LjQ3NC0xOS45NDFjLTEuOTUxLTQuNTg5LTEwLjExLTE2LjMxLTE4LjMxLTI2LjUxNC02LjM2NS03LjkyMS0xMS4zMzUtMTMuNjM5LTE1LjYyMy0xOC43MDJ6IiB0cmFuc2Zvcm09Im1hdHJpeCguMTQ5MyAwIDAgLjE0OTMgMTQuNzYzIC04LjI1KSIvPjwvZz48L3N2Zz4=';
			add_menu_page( $yoink_inv_title,
				$yoink_inv_title, 'yoink_register_device', 'yoink_inventory',
				function () {
					include_once( YOINK_INV_ABSPATH
					              . 'src/AdminViews/yoink-default-admin-view.php' );
					YoinkInvDefaultAdminView::yoink_inv_display();
				}, $yoink_inv_icon, 56, false );

			$mappings = self::yoink_inv_get_mappings();
			foreach ( $mappings as $slug => $details ) {
				$required_capability = $details['capability'];
				if ( current_user_can( $required_capability ) ) {
					// ToDo: Only include the page file when it needs to be rendered.
					include_once( YOINK_INV_ABSPATH . 'src/AdminViews/'
					              . $details['file'] . '.php' );
					$title = $details['title'];
					// YoinkInvWooPlugin::yoink_inv_debug_log("Adding '" . $title . "' to menu");
					add_submenu_page( 'yoink_inventory', $title, $title,
						$details['capability'],
						$slug,
						array( $details['class'], 'yoink_inv_display' ) );
				}
			}

			$qr_load_hook = add_submenu_page( null, 'Generate QR Codes',
				'Generate QR Codes', 'yoink_generate_qr_codes',
				'print-yoink-qr-codes', ' ' );
			add_action( "load-$qr_load_hook", function () {
				YoinkInvWooPlugin::yoink_inv_debug_log( 'Load action for generate-qr-codes-view' );
				include_once( YOINK_INV_ABSPATH
				              . 'src/AdminViews/printable-qr-codes.php' );
				YoinkInvPrintableQrCodes::yoink_inv_display();
				exit( 200 );
			} );

			// ToDo: Remove from menu to avoid accidental clicks to a large DB dump
			//       (set parent_slug to null). Instead, put a button on some page and
			//       surround it with negative space.
			$csv_load_hook = add_submenu_page( 'yoink_inventory', 'Export CSV',
				'Export CSV',
				'yoink_export_data', 'export_csv', ' ' );
			add_action( "load-$csv_load_hook", function () {
				YoinkInvWooPlugin::yoink_inv_debug_log( 'Load action for export_csv' );
//				include_once( YOINK_INV_ABSPATH
//				              . 'src/AdminViews/csv-snapshot.php' );
				YoinkInv\AdminViews\YoinkInvExportCsv::yoink_inv_display();
				exit( 200 );
			} );
		}

		public static function yoink_inv_enqueue_assets() {
			$screen    = get_current_screen();
			$screen_id = $screen ? $screen->id : '';
			// YoinkInvWooPlugin::yoink_inv_debug_log('Screen: ' . $screen_id);
			// YoinkInvWooPlugin::yoink_inv_debug_log($screen);

			$prefix     = 'yoink-inventory_page_';
			$len_prefix = strlen( $prefix );
			if ( substr( $screen_id, 0, $len_prefix ) === $prefix ) {
				$submenu_slug = substr( $screen_id, $len_prefix );
				// YoinkInvWooPlugin::yoink_inv_debug_log('Checking assets for ' . $submenu_slug);
				$config = self::yoink_inv_get_mappings()[ $submenu_slug ];

				if ( isset( $config['hasAssets'] ) ) {
					$config['class']::yoink_inv_enqueue_assets();
				}
			}
		}
	}
}

return new YoinkInvAdminMenus();
