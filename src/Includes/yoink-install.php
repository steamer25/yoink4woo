<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class YoinkInvInstall {
	public static function yoink_inv_install() {
		YoinkInvWooPlugin::yoink_inv_debug_log( "Installing" );

		self::yoink_inv_create_roles();

		global $wpdb;

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			$collate = $wpdb->get_charset_collate();
		}

		// ToDo: dbDelta doesn't support FOREIGN KEYs. Create/update the table manually. E.g., on yoinkinventory_stashes:
		// FOREIGN KEY fk_warehouse_id (warehouse_id) REFERENCES {$wpdb->prefix}yoinkinventory_warehouses(warehouse_id)

		$ddl = array(
			"CREATE TABLE {$wpdb->prefix}yoinkinventory_warehouses (
  warehouse_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(191) NOT NULL,
	address1 varchar(255) NOT NULL,
	address2 varchar(255) NULL,
	city varchar(255) NOT NULL,
	state varchar(255) NULL,
	postcode varchar(255) NULL,
	country_code char(2) NOT NULL
	alerts_email_addy varchar(255) NULL,
  PRIMARY KEY (warehouse_id),
  UNIQUE KEY idx_uniq_name (name)
) ENGINE = InnoDB $collate;",

			"CREATE TABLE {$wpdb->prefix}yoinkinventory_stashes (
  warehouse_id BIGINT UNSIGNED NULL,
  stash_id varchar(187) NOT NULL,
  product_id BIGINT UNSIGNED NOT NULL,
  quantity BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (warehouse_id, stash_id, product_id),
  INDEX idx_product_id_warehouse_id_qty (product_id, warehouse_id, quantity),
  INDEX idx_warehouse_product_qty (warehouse_id, product_id, quantity)
) ENGINE = InnoDB $collate;",

			"CREATE TABLE {$wpdb->prefix}yoinkinventory_warehouse_log (
  warehouse_id BIGINT UNSIGNED NOT NULL,
  user_id varchar(255) NOT NULL,
  stash_id varchar(187) NOT NULL,
  product_id BIGINT UNSIGNED NOT NULL,
  quantity INT SIGNED NOT NULL,
  action TINYINT UNSIGNED NOT NULL,
  notes longtext NULL,
  event_time datetime NOT NULL,
	INDEX idx_event_time (event_time)
) ENGINE = InnoDB $collate;",

			"CREATE TABLE {$wpdb->prefix}yoinkinventory_order_state (
  order_id BIGINT UNSIGNED NOT NULL,
	state_id TINYINT UNSIGNED NOT NULL,
	user_id BIGINT UNSIGNED NULL,
  PRIMARY KEY (order_id)
) ENGINE = InnoDB $collate;",

//			ToDo: Consider removing order_id from yoinkinventory_order_item_state (since it's already in woocommerce_order_items)
			"CREATE TABLE {$wpdb->prefix}yoinkinventory_order_item_state (
  order_id BIGINT UNSIGNED NOT NULL,
  order_item_id BIGINT UNSIGNED NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL,
  state_id TINYINT UNSIGNED NOT NULL,
  quantity BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (order_id, order_item_id, user_id, state_id)
) ENGINE = InnoDB $collate;",

			"CREATE TABLE {$wpdb->prefix}yoinkinventory_unfilled_items (
  order_item_id BIGINT UNSIGNED NOT NULL,
  remaining_quantity BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (order_item_id)
) ENGINE = InnoDB $collate;",

			<<<SQL
CREATE TABLE {$wpdb->prefix}yoinkinventory_personal_custody (
user_id BIGINT UNSIGNED NOT NULL,
product_id BIGINT UNSIGNED NOT NULL,
quantity BIGINT UNSIGNED NOT NULL,
PRIMARY KEY (user_id, product_id)
) ENGINE = InnoDB $collate;
SQL
		);

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $ddl );

		$posts_index_name = 'yoinkinv_idx_post_type_status';
		$posts_index_exists_query = <<<SQL
SELECT COUNT(1) IndexIsThere
FROM INFORMATION_SCHEMA.STATISTICS
WHERE table_schema=DATABASE()
  AND table_name='$wpdb->posts'
  AND index_name='$posts_index_name';
SQL;

		$posts_index_exists = $wpdb->get_var( $posts_index_exists_query );

		if ( $posts_index_exists < 1 ) {
			$index_posts_query
				= "CREATE INDEX $posts_index_name ON $wpdb->posts (post_type, post_status)";

			$wpdb->query( $index_posts_query );
		}

//        ToDo: Consider adding compound indices to additional WP/WC tables:
//              CREATE INDEX $yoinkinv_index_name ON {$wpdb->prefix}woocommerce_order_items (order_id, order_item_type, order_item_name)
//                woocommerce_order_items query count at least: 4
//              CREATE INDEX $yoinkinv_index_name ON {$wpdb->prefix}woocommerce_order_items (order_item_type)
//                query count at least: 1
//              CREATE INDEX $yoinkinv_index_name ON $wpdb->postmeta (post_id, meta_key)
//                postmeta query count at least: 3
//              CREATE INDEX $yoinkinv_index_name ON {$wpdb->prefix}woocommerce_order_itemmeta (order_item_id, meta_key)
//                query count at least: 2
//              CREATE INDEX $yoinkinv_index_name ON $wpdb->posts (post_type, post_modified_gmt)
//              CREATE INDEX $yoinkinv_index_name ON $wpdb->posts (post_title)

		add_option( 'yoinkinventory_version', '1.0.0' );
	}

	private static function yoink_inv_upsert_role_capabilities(
		$wp_roles, $role_name, $role_desc, $capabilities
	) {
		$role = $wp_roles->get_role( $role_name );

		if ( null == $role ) {
			add_role( $role_name, $role_desc, $capabilities );
		} else {
			foreach ( $capabilities as $cap ) {
				$role->add_cap( $cap );
			}
		}
	}

	public static function yoink_inv_create_roles() {
		global $wp_roles;

		if ( ! class_exists( 'WP_Roles' ) ) {
			return;
		}

		if ( ! isset( $wp_roles ) ) {
			$wp_roles = new WP_Roles();
		}

		YoinkInvWooPlugin::yoink_inv_debug_log( 'Creating roles' );
		self::yoink_inv_upsert_role_capabilities( $wp_roles, 'yoink_handler',
			__( 'Yoink Handler', 'yoinkinventory' ), array(
				'yoink_receive_items'      => true,
				'yoink_ship_items'         => true,
				'yoink_check_items_in_out' => true,
			) );

		self::yoink_inv_upsert_role_capabilities( $wp_roles,
			'yoink_warehouse_manager',
			__( 'Yoink Warehouse Manager', 'yoinkinventory' ), array(
				'yoink_receive_items'         => true,
				'yoink_ship_items'            => true,
				'yoink_check_items_in_out'    => true,
				'yoink_set_absolute_quantity' => true,
				'yoink_register_device'       => true,
				'yoink_generate_qr_codes'     => true,
				'yoink_export_data'           => true,
				'yoink_review_unfilled'       => true,
			) );

		self::yoink_inv_upsert_role_capabilities( $wp_roles, 'yoink_admin',
			__( 'Yoink Admin', 'yoinkinventory' ), array(
				'yoink_receive_items'            => true,
				'yoink_ship_items'               => true,
				'yoink_check_items_in_out'       => true,
				'yoink_set_absolute_quantity'    => true,
				'yoink_crud_multiple_warehouses' => true,
				'yoink_review_unfilled'          => true,
				'yoink_register_device'          => true,
				'yoink_generate_qr_codes'        => true,
				'yoink_export_data'              => true,
				'yoink_sync_qtys'                => true,
			) );

		$admin_capabilities = array(
			'yoink_crud_multiple_warehouses',
			'yoink_receive_items',
			'yoink_ship_items',
			'yoink_check_items_in_out',
			'yoink_set_absolute_quantity',
			'yoink_review_unfilled',
			'yoink_register_device',
			'yoink_generate_qr_codes',
			'yoink_export_data',
			'yoink_sync_qtys',
		);
		$admin_role         = $wp_roles->get_role( 'administrator' );
		foreach ( $admin_capabilities as $cap ) {
			$admin_role->add_cap( $cap );
		}

		// foreach ( $wp_roles->get_role('administrator')->capabilities as $cap => $grant) {
		// 			echo '<div style="position: relative; left: 190px;">';
		// echo '<h1>'.$cap . '::'.$grant.'</h1>';
		// echo '</div>';
		// }
	}
}
