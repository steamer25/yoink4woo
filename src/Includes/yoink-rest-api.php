<?php

// Copyright: © 2017 Brett Stime.

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use YoinkInv\Classes\YoinkInvValidatedCustodyTaking;
use YoinkInv\Classes\YoinkInvValidatedCommandeerTaking;
use YoinkInv\Classes\YoinkInvValidatedAbsoluteStashAdjustment;

include_once( YOINK_INV_ABSPATH . 'src/Includes/mysql-utils.php' );
include_once( YOINK_INV_ABSPATH . 'src/Includes/stash-utils.php' );
include_once( YOINK_INV_ABSPATH . 'src/Includes/validation-utils.php' );
include_once( YOINK_INV_ABSPATH . 'src/Includes/yoink-consts.php' );

function yoink_inv_sanitize_stash_id_from_url_or_return_err_if_invalid(
	$string
) {
	return yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid( urldecode( $string ) );
}

class YoinkInvRestApi {
	public function __construct() {
		// YoinkInvWooPlugin::yoink_inv_debug_log('YoinkInvRestApi::__construct() called');
		add_action( 'rest_api_init', function () {
			error_log( 'yoink-rest-api:: rest_api_init' );
			// ToDo: Move the longer methods into separate files and only import them when called.

			$rest_api_namespace         = 'wc-yinv-api';
			$rest_api_namespace_plus_v1 = $rest_api_namespace . '/v1';
			// YoinkInvWooPlugin::yoink_inv_debug_log('on rest_api_init action');
			register_rest_route( $rest_api_namespace, '/feature-level', array(
				'methods'             => 'GET',
				'permission_callback' => '__return_true',
				'callback'            => function () {
					return [ 1, 0, 0 ];
				},
			) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/user-capabilities',
				array(
					'methods'             => 'GET',
					'permission_callback' => function () {
						YoinkInvWooPlugin::yoink_inv_debug_log( 'User capability check (sign in) for user ID: '
						                                        . get_current_user_id() );

						return get_current_user_id() > 0;
					},
					'callback'            => function () {
						$capabilities
							       = get_userdata( get_current_user_id() )->allcaps;
						$just_true = array();

						$user = wp_get_current_user();

						foreach ( $capabilities as $cap => $tf ) {
							if ( $tf ) {
								$just_true[] = $cap;
							}
						}

						return array(
							'user_id'           => $user->ID,
							'user_display_name' => $user->display_name,
							'capabilities'      => $just_true,
						);
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1, '/warehouses',
				array(
					'methods'             => 'GET',
					'args'                => array(
						'page_from_warehouse_name' => array(
							'description'       => __( 'Set the last warehouse name 
						you received in the prior response to get the next page',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => false,
							'sanitize_callback' => 'sanitize_key'
						),
						'should_page_back'         => array(
							'description' => __( 'For pagination. If omitted, order will be returned starting with the earliest postings after page_from_warehouse_name. If set true, a page of prior warehouses will be returned up until page_from_warehouse_name',
								'yoinkinventory' ),
							'type'        => 'boolean',
							'required'    => false,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_receive_items' )
						       || current_user_can( 'yoink_check_items_in_out' )
						       || current_user_can( 'yoink_set_absolute_quantity' )
						       || current_user_can( 'yoink_ship_items' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {

						$should_page_back
							= yoink_inv_sanitize_bool_default_false( $request['should_page_back'] );

						if ( $should_page_back
						     && ! isset( $request['page_from_warehouse_name'] )
						) {
							return new WP_Error( 'yoink_inv_unexpected_pagination_direction',
								__( 'The should_page_back param can only be used when page_from_warehouse_name is set',
									'yoinkinventory' ),
								array( 'status' => 400 )
							);
						}

						global $wpdb;

						$max_row_count = 5000;
						$query_args    = array();
						$query         = <<<SQL
SELECT warehouse_id, name
FROM {$wpdb->prefix}yoinkinventory_warehouses
SQL;

						if ( isset( $request['page_from_warehouse_name'] ) ) {
							$query .= ' WHERE name ';
							if ( $should_page_back ) {
								$query .= "<";
							} else {
								$query .= ">";
							}

							$query .= ' %s ORDER BY name';
							$query_args[]
							       = $request['page_from_warehouse_name'];

							if ( $should_page_back ) {
								$query .= ' DESC';
							}
						}

						$query        .= " LIMIT %d";
						$query_args[] = $max_row_count;

						$rows = $wpdb->get_results( $wpdb->prepare( $query,
							$query_args ), ARRAY_N );

						$projected_rows = array();
						if ( $should_page_back ) {
							$index = count( $rows );
							while ( $index ) {
								$row              = $rows[ -- $index ];
								$projected_rows[] = array(
									intval( $row[0], 10 ),
									$row[1]
								);
							}
						} else {
							foreach ( $rows as $row ) {
								$projected_rows[] = array(
									intval( $row[0], 10 ),
									$row[1]
								);
							}
						}

						return array(
							'max_count'  => $max_row_count,
							'warehouses' => $projected_rows,
						);
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/products/modifiedSince/(?P<last_modified>[\d]*)', array(
					'methods'             => 'GET',
					'args'                => array(
						'last_modified' => array(
							'description' => __( 'Only returns products modified more recently than the time specified',
								'yoinkinventory' ),
							'type'        => 'integer',
							'required'    => true,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_receive_items' )
						       || current_user_can( 'yoink_check_items_in_out' )
						       || current_user_can( 'yoink_set_absolute_quantity' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						global $wpdb;

//						ToDo: Call again with max(resultModifieds) - 1ms
//						      Secondary sort by productId?
						$qry = <<<SQL
SELECT ID, post_name, post_title, post_content, post_excerpt, post_modified_gmt
FROM $wpdb->posts P
WHERE P.post_type IN ('product', 'product_variation')
  AND post_modified_gmt >= %s
LIMIT 320
SQL;

						if ( isset( $request['last_modified'] ) ) {
							$ts_last_modified
								= intval( $request['last_modified'], 10 );
						} else {
							$ts_last_modified = 0;
						}
						$last_modified_sql_arg
							= yoink_inv_format_timestamp_as_mysql_datetime( $ts_last_modified );
						// error_log('last_modified: ' . $ts_last_modified);
//						$dt_last_modified = new DateTime();
//						$dt_last_modified->setTimestamp( $ts_last_modified );
//							= $dt_last_modified->format( 'Y-m-d H:i:s' );
						// error_log('last_modified_sql_arg: ' . $last_modified_sql_arg );
						// error_log($wpdb->prepare($qry, $last_modified_sql_arg));
						$rows           = $wpdb->get_results( $wpdb->prepare( $qry,
							$last_modified_sql_arg ) );
						$max_dt         = $ts_last_modified;
						$projected_rows = array();

						foreach ( $rows as $row ) {
							$dt_modified
								= ( new DateTime( $row->post_modified_gmt ) )->getTimestamp();
							if ( $dt_modified > $max_dt ) {
								$max_dt = $dt_modified;
							}
							$projected_rows[] = array(
								intval( $row->ID, 10 ),
								$row->post_name,
								$row->post_title,
								$row->post_content
								//$row->post_excerpt,
							);
						}

						return array(
							'max_dt' => $max_dt,
							'rows'   => $projected_rows,
						);
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/existing-stashes/(?P<warehouse_id>\d+)/(?P<product_id>\d+)',
				array(
					'methods'             => 'GET',
					'args'                => array(
						'warehouse_id' => array(
							'description'       => __( 'Which warehouse do you need a stash listing for?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'product_id'   => array(
							'description'       => __( 'Which product do you need a stash listing for?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'offset'       => array(
							'description'       => __( 'How many rows should the query skip?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => false,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_receive_items' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						global $wpdb;

						$max_rows = 20000;
						$query    = <<<SQL
SELECT stash_id, quantity
FROM {$wpdb->prefix}yoinkinventory_stashes
WHERE warehouse_id = %d AND product_id = %d
ORDER BY quantity DESC
LIMIT %d
SQL;

						$query_args = array(
							$request['warehouse_id'],
							$request['product_id'],
							$max_rows
						);
						if ( isset( $request['offset'] ) ) {
							$query        .= "\nOFFSET %d";
							$query_args[] = $request['offset'];
						}
//						elseif ( isset( $request['page_until'] ) ) {
//							$query   = $query
//							           . "\nWHERE name < %s ORDER BY name DESC";
//							$query_args[]
//							         = sanitize_key( $request['page_until'] );
//							$reverse = true;
//						}

						$rows = $wpdb->get_results( $wpdb->prepare( $query,
							$query_args ) );

						$stash_quantities = array();

						foreach ( $rows as $row ) {
							$stash_quantities[] = array(
								$row->stash_id,
								intval( $row->quantity )
							);
						}

						return array(
							'stash_quantities' => $stash_quantities,
							'max_count'        => $max_rows
						);
					},
				) );
//
//			register_rest_route( $rest_api_namespace, '/test-post-validation',
//				array(
//					'methods'             => 'POST',
//					'callback'            => function ( WP_REST_Request $request
//					) {
//						return 'You win: ' . $request['stash_id'] . '<-';
//					},
//					'permission_callback' => function () {
//						return current_user_can( 'yoink_receive_items' );
//					},
//					'args'                => array(
//						'warehouse_id'   => array(
//							'type'              => 'integer',
//							'required'          => true,
//							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
//						),
//						'stash_id'       => array(
//							'required'          => true,
//							'type'              => 'string',
//							'sanitize_callback' => 'yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid',
//						),
//						'product_id'     => array(
//							'type'              => 'integer',
//							'required'          => true,
//							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
//						),
//						'quantity_added' => array(
//							'type'              => 'integer',
//							'required'          => true,
//							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
//						),
//					),
//				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/receive-inventory',
				array(
					'methods'             => 'POST',
					'callback'            => array(
						$this,
						'yoink_inv_on_receive_inventory_post'
					),
					'permission_callback' => function () {
						YoinkInvWooPlugin::yoink_inv_debug_log( 'User receiving inventory. User ID: '
						                                        . get_current_user_id() );

						return current_user_can( 'yoink_receive_items' );
					},
					'args'                => array(
						'warehouse_id'   => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'stash_id'       => array(
							'required'          => true,
							'type'              => 'string',
							'sanitize_callback' => 'yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid',
						),
						'product_id'     => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'quantity_added' => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						)
					),
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/orders-for-processing/(?P<warehouse_id>\d+)', array(
					'methods'             => 'GET',
					'args'                => array(
						'warehouse_id'          => array(
							'description'       => __( 'Which warehouse are you filling orders from?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'page_from_post_gmt_ts' => array(
							'description' => __( 'For pagination. Returns orders posted after the time specified or before if is_until == true',
								'yoinkinventory' ),
							'type'        => 'integer',
							'required'    => false,
						),
						'page_from_order_id'    => array(
							'description'       => __( 'For pagination. Returns orders posted after the ID specified or before if is_ascending == true',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => false,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'should_page_back'      => array(
							'description' => __( 'For pagination. If omitted, order will be returned starting with the earliest postings after from_post_gmt_ts. If set true, a page of prior orders will be returned up until from_post_gmt_ts',
								'yoinkinventory' ),
							'type'        => 'boolean',
							'required'    => false,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_ship_items' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						$page_from_post_gmt_ts_param
							= $request['page_from_post_gmt_ts'];
						$page_from_post_gmt_ts_is_set
							= isset( $page_from_post_gmt_ts_param );

						$page_from_order_id = $request['page_from_order_id'];
						$page_from_order_id_is_set
						                    = isset( $page_from_order_id );

						$sanitized_should_page_back
							= yoink_inv_sanitize_bool_default_false( $request['should_page_back'] );
//						$should_page_back_param = $request['should_page_back'];
//						$should_page_back
//						                        = isset( $should_page_back_param )
//						                          && $should_page_back_param;

						if ( $page_from_post_gmt_ts_is_set
						     xor $page_from_order_id_is_set
						) {
							return new WP_Error( 'yoink_inv_invalid_pagination_combo',
								__( 'When either `from_post_gmt_ts` or `from_order_id` is set, the other must be as well',
									'yoinkinventory' ),
								array( 'status' => 400 )
							);
						} elseif ( $sanitized_should_page_back
						           && ! $page_from_post_gmt_ts_is_set
						) {
							return new WP_Error( 'yoink_inv_unexpected_pagination_direction',
								__( 'The should_page_back param can only be used when both page_from_post_gmt_ts and page_from_order_id are set',
									'yoinkinventory' ),
								array( 'status' => 400 )
							);
						}

						global $wpdb;

						$query = <<< SQL
SELECT P.ID, P.post_date_gmt,
    M.meta_value as customer_id, I.order_item_name as shipping_method,
	S.state_id, S.user_id, U.display_name as claimant
FROM {$wpdb->posts} P
	LEFT JOIN {$wpdb->prefix}postmeta M
		ON P.ID = M.post_id
	JOIN {$wpdb->prefix}woocommerce_order_items I
		ON P.ID = I.order_id
	LEFT JOIN {$wpdb->prefix}yoinkinventory_order_state S
		ON P.ID = S.order_id
	LEFT JOIN $wpdb->users U
		ON S.user_id = U.ID
WHERE P.post_type = 'shop_order'
	AND P.post_status = 'wc-processing'
	AND M.meta_key = '_customer_user'
	AND I.order_item_type = 'shipping'
SQL;

						$query_args = array();

						if ( $page_from_post_gmt_ts_is_set ) {
							$page_from_post_time_sql
								          = yoink_inv_format_timestamp_as_mysql_datetime( intval( $page_from_post_gmt_ts_param ) );
							$query        .= ' AND ((P.post_date_gmt == %s AND P.ID ';
							$query_args[] = $page_from_post_time_sql;

							if ( $sanitized_should_page_back ) {
								$query .= '>';
							} else {
								$query .= '<';
							}

							$query        .= ' %d) OR P.post_date_gmt ';
							$query_args[] = $page_from_order_id;

							if ( $sanitized_should_page_back ) {
								$query .= '>';
							} else {
								$query .= '<';
							}

							$query        .= ' %s) ';
							$query_args[] = $page_from_post_time_sql;
						}

						$query .= 'ORDER BY ';
						if ( $sanitized_should_page_back ) {
							$query .= 'P.post_date_gmt ASC, P.ID ASC';
						} else {
							$query .= 'P.post_date_gmt DESC, P.ID DESC';
						}

						$max_row_count = 1800;
						$query         .= ' LIMIT %d';
						$query_args[]  = $max_row_count;

						$rows = $wpdb->get_results( $wpdb->prepare( $query,
							$query_args ) );

						$projected_rows = array();

						include_once( YOINK_INV_ABSPATH
						              . 'src/Includes/order-utils.php' );

						if ( $sanitized_should_page_back ) {
							$index = count( $rows );
							while ( $index ) {
								$row = $rows[ -- $index ];
								$projected_rows[]
								     = yoink_inv_project_order_summary_row( $row );
							}
						} else {
							foreach ( $rows as $row ) {
								$projected_rows[]
									= yoink_inv_project_order_summary_row( $row );
							}
						}

						return array(
							'orderSummaries' => $projected_rows,
							'max_count'      => $max_row_count
						);
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/order/(?P<order_id>\d+)/items-by-product-name/(?P<warehouse_id>\d+)',
				array(
					'methods'             => 'GET',
					'args'                => array(
						'order_id'                  => array(
							'description'       => __( 'Which order are you trying to load?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'warehouse_id'              => array(
							'description'       => __( 'Which warehouse are you filling orders from? Used to sort line items and stashes.',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'page_from_order_item_name' => array(
							'description'       => __( 'For pagination. Returns line items after the product name or before if should_page_back = true',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => false,
							'sanitize_callback' => 'sanitize_key',
						),
						'should_page_back'          => array(
							'description' => __( 'For pagination. If omitted, line items will be returned starting with the earliest product name after page_from_order_item_name. If set true, a page of line items will be returned up until page_from_order_item_name',
								'yoinkinventory' ),
							'type'        => 'boolean',
							'required'    => false,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_ship_items' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						return $this->yoink_inv_get_order_details_by_product_name( $request );
					}
				) );

			register_rest_route( $rest_api_namespace_plus_v1, '/claim-order',
				array(
					'methods'             => 'POST',
					'args'                => array(
						'order_id' => array(
							'description'       => __( 'Which order are you trying to claim?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'force'    => array(
							'description' => __( 'Overwrite if claimed by another user',
								'yoinkinventory' ),
							'type'        => 'boolean',
							'default'     => false,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_ship_items' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						return $this->yoink_inv_claim_order( $request['order_id'],
							get_current_user_id(),
							filter_var( $request['force'],
								FILTER_VALIDATE_BOOLEAN ) );
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1, '/take-custody',
				array(
					'methods'             => 'POST',
					'args'                => array(
						'warehouse_id' => array(
							'description'       => __( 'Which warehouse are you filling orders from?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'order_id'     => array(
							'description'       => __( 'Optional order ID if shipping instead of moving',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => false,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'takings'      => array(
							'description' => __( 'A JSON array. Each object should have: stashId, productId and quantity',
								'yoinkinventory' ),
							'type'        => 'string',
							'required'    => true,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_ship_items' )
						       || current_user_can( 'yoink_check_items_in_out' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						$order_id = $request['order_id'];

//					include_once( YOINK_INV_ABSPATH
//					              . 'src/Classes/YoinkInvValidatedCustodyTaking.php' );
						// Not sanitized via callback because we need the order_id, too.
						$wp_error_or_sanitized_takings
							= YoinkInvValidatedCustodyTaking::sanitize_list_from_encoded_json(
							$order_id != null,
							$request['takings'] );
						if ( is_wp_error( $wp_error_or_sanitized_takings ) ) {
							return $wp_error_or_sanitized_takings;
						}

						$user_id = get_current_user_id();

						$warehouse_id = $request['warehouse_id'];
						global $wpdb;
						$stashes_table = $wpdb->prefix
						                 . 'yoinkinventory_stashes';

						$null_record_takings    = array();
						$record_too_low_entries = array();

						$wpdb->query( 'START TRANSACTION' ); // ToDo: Should we start a shorter transaction for each taking?
						try {
							foreach (
								$wp_error_or_sanitized_takings as $taking
							) {
								$quantity_taken = $taking->get_quantity();
								$stash_id       = $taking->get_stash_id();
								$product_id     = $taking->get_product_id();

								$previous_quantity
									= $wpdb->get_var( $wpdb->prepare(
									"SELECT quantity
								FROM $stashes_table
								WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d
								FOR UPDATE",
									$warehouse_id, $stash_id, $product_id ) );

								if ( null == $previous_quantity ) {
									$null_record_takings[] = $taking;
								} else {
									$previous_quantity
										= intval( $previous_quantity );
									if ( $previous_quantity
									     <= $quantity_taken
									) {
										$record_too_low_entries[] = array(
											'taking'       => $taking,
											'previous_qty' => $previous_quantity
										);
										// ToDo: Lookup product name and return warning.
										// ToDo: Allow updating/refreshing pick list stashes in case of long pick times.
										//       Maybe reject these unless user is authorized to override.
										// ToDo: Possibly cross-lookup order and see if they actually need as many as claimed
										//       For now, mostly rely on authentication, client-side validation and logging/audits.
										$wpdb->query( $wpdb->prepare(
											"DELETE FROM $stashes_table
											WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d",
											$warehouse_id, $stash_id,
											$product_id ) );
									} else {
										$new_quantity = $previous_quantity
										                - $quantity_taken;
										$wpdb->update(
											$stashes_table,
											array(
												'quantity' => $new_quantity,
											),
											array(
												'warehouse_id' => $warehouse_id,
												'stash_id'     => $stash_id,
												'product_id'   => $product_id,
											),
											array( '%d' ),
											array( '%d', '%s', '%d' )
										);
									}
								}

								if ( null == $order_id ) {
									$insert_personal_custody_query = <<<SQL
INSERT INTO {$wpdb->prefix}yoinkinventory_personal_custody ( user_id, product_id, quantity )
VALUES(%d, %d, %d)
ON DUPLICATE KEY UPDATE quantity = quantity + %d
SQL;

									$wpdb->query( $wpdb->prepare( $insert_personal_custody_query,
										$user_id, $product_id, $quantity_taken,
										$quantity_taken ) );
								} else {
									$wpdb->query( $wpdb->prepare(
										"INSERT INTO {$wpdb->prefix}yoinkinventory_order_item_state
							( order_id, order_item_id, user_id, state_id,
								quantity )
										VALUES(%d, %s, %d, %d, %d)
										ON DUPLICATE KEY UPDATE quantity
								= quantity + %d",
										$order_id, $taking->get_order_item_id(),
										$user_id,
										YOINKINV_ITEM_STATUS_YOINKED_FOR_ORDER,
										$quantity_taken,
										$quantity_taken ) );
								}
							}
							$wpdb->query( 'COMMIT' );

							$result = array(
								'success' => true,
							);

							if ( count( $null_record_takings ) > 0
							     || count( $record_too_low_entries ) > 0
							) {
								$warnings               = array();
								$warning_product_id_set = array();
								foreach (
									$null_record_takings as $null_record_taking
								) {
									$warning_product_id_set[ $null_record_taking->get_product_id() ]
										= null;
								}
								foreach (
									$record_too_low_entries as
									$record_too_low_entry
								) {
									$warning_product_id_set[ $record_too_low_entry['taking']->get_product_id() ]
										= null;
								}

								$warning_product_ids
									= array_keys( $warning_product_id_set );

								$warning_product_id_placeholders = implode( ',',
									array_fill( 0,
										count( $warning_product_ids ),
										'%d' ) );

								$product_query = <<<SQL
SELECT ID, post_title
FROM $wpdb->posts
WHERE ID IN ($warning_product_id_placeholders)
SQL;
								$product_rows
								               = $wpdb->get_results( $wpdb->prepare( $product_query
									, $warning_product_ids ) );

								$product_titles_by_id = array();
								foreach ( $product_rows as $product_row ) {
									$product_titles_by_id[ $product_row->ID ]
										= $product_row->post_title;
								}

								$unknown_product_display
									= __( 'Unknown--no product entry',
									'yoinkinventory' );
								foreach (
									$null_record_takings as $null_record_taking
								) {
									if ( isset( $product_titles_by_id[ $null_record_taking->get_product_id() ] ) ) {
										$product_display
											= $product_titles_by_id[ $null_record_taking->get_product_id() ];
									} else {
										$product_display
											= $unknown_product_display;
									}

									$warnings[]
										// translators: %1$d: The quantity of items the user claimed to have taken, %2$d: the identifier of the product type claimed, %3$s: the title/name of the product claimed, %4$s: the stash identifier from which the items were taken
										= sprintf( __( 'Claimed %1$d of product ID %2$d, \'%3$s\', from stash ID %4$s which did not have a previously recorded quantity (null)',
										'yoinkinventory' ),
										$null_record_taking->get_quantity(),
										$null_record_taking->get_product_id(),
										$product_display,
										$null_record_taking->get_stash_id() );
								}

								foreach (
									$record_too_low_entries as
									$record_too_low_entry
								) {
									$record_too_low_taking
										= $record_too_low_entry['taking'];
									$previous_qty
										= $record_too_low_entry['previous_qty'];
									if ( isset( $product_titles_by_id[ $record_too_low_taking->get_product_id() ] ) ) {
										$product_display
											= $product_titles_by_id[ $record_too_low_taking->get_product_id() ];
									} else {
										$product_display
											= $unknown_product_display;
									}

//								ToDo: Record audit/history logs for each stash adjustment
//								ToDo: Log to database and provide a report in the admin section
//								ToDo: Remember where products were (used to be). Keep one or two MRU so can suggest when receiving refills, even if currently empty
									$warnings[]
										// translators: %1$d: The quantity of items the user claimed to have taken, %2$d: the identifier of the product type claimed, %3$s: the title/name of the product claimed, %4$s: the stash identifier from which the items were taken, %5$d: the previously recorded quantity.
										= sprintf( __( 'Claimed %1$d of product ID %2$d, \'%3$s\', from stash ID %4$s which was only expected to have an available quantity of %5$d',
										'yoinkinventory' ),
										$record_too_low_entry->get_quantity(),
										$record_too_low_entry->get_product_id(),
										$product_display,
										$record_too_low_entry->get_stash_id(),
										$previous_qty );
								}

								if ( null != $order_id ) {
									$maybe_order_context
										= sprintf(
									// translators: %d: The order number the user is claiming to fill items for
										__( 'filling order #%d',
											'yoinkinventory' ), $order_id );
								} else {
									$maybe_order_context = __( 'working',
										'yoinkinventory' );
								}

								$simple_warning_context
									= sprintf(
								// translators: %1$s: The goal of the user e.g., filling items for a particular order or just working in a more general/unspecified sense, %2$d: the numeric identifier of the warehouse where the user was working, %3$d: the ID of the user who was claiming items in the warehouse
									__( 'While %1$s in the warehouse ID %2$d, user ID %3$d posted the following taking claims which weren\'t expected to work:',
										'yoinkinventory' ),
									$maybe_order_context,
									$warehouse_id, $user_id );

								$f_format_warning = function ( $w ) {
									return "* $w";
								};
								$warning_lines    = implode( "\n",
									array_map( $f_format_warning, $warnings ) );

								error_log( $simple_warning_context . "\n\n"
								           . $warning_lines );

								$warehouse_query = <<<SQL
SELECT name, alerts_email_addy
FROM {$wpdb->prefix}yoinkinventory_warehouses
WHERE warehouse_id = %d
SQL;

								$warehouse_row = $wpdb->get_row( $wpdb->prepare(
									$warehouse_query,
									$warehouse_id ) );

								$alerts_addy
									= $warehouse_row->alerts_email_addy;

								if ( null != $alerts_addy ) {
									$warehouse_name = $warehouse_row->name;
									$user_display
									                = wp_get_current_user()->display_name;

									$subject
										= sprintf(
									// translators: %1$s: The goal of the user e.g., filling items for a particular order or just working in a more general/unspecified sense, %2$s: the name of the warehouse where the user was working, %3$d: the numeric identifier of the warehouse where the user was working, %4$s: the display name of the user who was claiming items in the warehouse
										__( 'While %1$s in the warehouse named \'%2$s\' (ID %3$d), %4$s posted the following taking claims which weren\'t expected to work:',
											'yoinkinventory' ),
										$maybe_order_context,
										$warehouse_name,
										$warehouse_id, $user_display );

									$wc_mail             = new WC_Email();
									$wc_mail->email_type = 'plain';
									$wc_mail->send( $alerts_addy, $subject,
										$warning_lines,
										'', array() );
								}

								$result['warnings'] = $warnings;
							}

							return $result;
						} catch ( Exception $e ) {
							$wpdb->query( 'ROLLBACK' );
							error_log( sprintf( "Error in take-custody: %s\n%s",
								$e->getMessage(), $e->getTraceAsString() ) );
							throw $e;
						}
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/stash-listing/(?P<warehouse_id>\d+)/(?P<stash_id>[^/]+)',
				array(
					'methods'             => 'GET',
					'args'                => array(
						'warehouse_id'            => array(
							'description'       => __( 'Which warehouse is the stash in?',
								'yoinkinventory' ),
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'stash_id'                => array(
							'description'       => __( 'Which stash are you listing?',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_sanitize_stash_id_from_url_or_return_err_if_invalid',
						),
						'page_from_product_title' => array(
							'description'       => __( 'For pagination. Returns product listings either alphabetically after this parameter or before if should_page_back == true',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => false,
							'sanitize_callback' => 'sanitize_key',
						),
						'should_page_back'        => array(
							'description' => __( 'For pagination. If omitted, product listings will be returned starting with the next alphabetical title after page_from_product_title. If set true, a page of product listings with alphabetically preceding titles will be returned up until page_from_product_title',
								'yoinkinventory' ),
							'type'        => 'boolean',
							'required'    => false,
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_check_items_in_out' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						$should_page_back
							= yoink_inv_sanitize_bool_default_false( $request['should_page_back'] );

						if ( $should_page_back
						     && ! isset( $request['page_from_product_title'] )
						) {
							return new WP_Error( 'yoink_inv_invalid_pagination_combo',
								__( 'The `page_from_product_title` param must be set when `should_page_back` is true',
									'yoinkinventory' ), array(
									'status' => 400
								) );
						}

						$should_page_back
							= yoink_inv_sanitize_bool_default_false( $request['should_page_back'] );

						global $wpdb;

						$query = <<<SQL
SELECT S.product_id, P.post_title, S.quantity
FROM {$wpdb->prefix}yoinkinventory_stashes S
JOIN $wpdb->posts P
	ON S.product_id = P.ID
WHERE S.warehouse_id = %d AND S.stash_id = %s
SQL;
						if ( isset( $request['page_from_product_title'] ) ) {
							$query .= ' AND P.post_title ';
							if ( $should_page_back ) {
								$query .= '>';
							} else {
								$query .= '<';
							}
							$query .= ' %s';
							$query_params[]
							       = $request['page_from_product_title'];
						}

						$max_row_count = 3700;
						$query_params  = array(
							$request['warehouse_id'],
							$request['stash_id'],
						);
						$query         .= ' ORDER BY P.post_title';
						if ( $should_page_back ) {
							$query .= ' DESC';
						}

						$query          .= ' LIMIT %d';
						$query_params[] = $max_row_count;
						$rows
						                = $wpdb->get_results( $wpdb->prepare( $query,
							$query_params ) );

						$stash_entries = array();

						if ( $should_page_back ) {
							$index = count( $rows );
							while ( $index ) {
								$row             = $rows[ -- $index ];
								$stash_entries[] = array(
									intval( $row->product_id ),
									$row->post_title,
									intval( $row->quantity )
								);
							}
						} else {
							foreach ( $rows as $row ) {
								$stash_entries[] = array(
									intval( $row->product_id ),
									$row->post_title,
									intval( $row->quantity )
								);
							}
						}

						return array(
							'stash_entries' => $stash_entries,
							'max_count'     => $max_row_count
						);
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/custody-listing', array(
					'methods'             => 'GET',
					'permission_callback' => function () {
						return current_user_can( 'yoink_check_items_in_out' );
					},
					'args'                => array(
						'page_from_product_title' => array(
							'description'       => __( 'For pagination. Returns product listings either alphabetically after this parameter or before if should_page_back == true',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => false,
							'sanitize_callback' => 'sanitize_key',
						),
						'should_page_back'        => array(
							'description' => __( 'For pagination. If omitted, product listings will be returned starting with the next alphabetical title after page_from_product_title. If set true, a page of product listings with alphabetically preceding titles will be returned up until page_from_product_title',
								'yoinkinventory' ),
							'type'        => 'boolean',
							'required'    => false,
						),
					),
					'callback'            => function ( WP_REST_Request $request
					) {
						$should_page_back
							= yoink_inv_sanitize_bool_default_false( $request['should_page_back'] );

						if ( $should_page_back
						     && ! isset( $request['page_from_product_title'] )
						) {
							return new WP_Error( 'yoink_inv_invalid_pagination_combo',
								__( 'The `page_from_product_title` param must be set when `should_page_back` is true',
									'yoinkinventory' ), array(
									'status' => 400
								) );
						}

						global $wpdb;

						$query = <<<SQL
SELECT C.product_id, P.post_title, C.quantity
FROM {$wpdb->prefix}yoinkinventory_personal_custody C
	JOIN $wpdb->posts P
		ON C.product_id = P.ID
WHERE C.user_id = %d
SQL;

						$query_params = array(
							get_current_user_id()
						);

						if ( isset( $request['page_from_product_title'] ) ) {
							$query .= ' AND P.post_title ';
							if ( $should_page_back ) {
								$query .= '>';
							} else {
								$query .= '<';
							}
							$query .= ' %s';
							$query_params[]
							       = $request['page_from_product_title'];
						}

						$query .= ' ORDER BY P.post_title';

						if ( $should_page_back ) {
							$query .= ' DESC';
						}

						$query         .= ' LIMIT %d';
						$max_row_count = 3700;

						/** @noinspection DuplicatedCode */
						$query_params[] = $max_row_count;

						$rows = $wpdb->get_results( $wpdb->prepare( $query,
							$query_params ) );

						$custody_items = array();

						if ( $should_page_back ) {
							$index = count( $rows );
							while ( $index ) {
								$row             = $rows[ -- $index ];
								$custody_items[] = array(
									intval( $row->product_id ),
									$row->post_title,
									intval( $row->quantity )
								);
							}
						} else {
							foreach ( $rows as $row ) {
								$custody_items[] = array(
									intval( $row->product_id ),
									$row->post_title,
									intval( $row->quantity )
								);
							}
						}

						return array(
							'custody_items' => $custody_items,
							'max_count'     => $max_row_count
						);
					},
				) );

//			ToDo: Consider allowing a batch of items into a given stash
			register_rest_route( $rest_api_namespace_plus_v1,
				'/deposit-from-custody',
				array(
					'methods'             => 'POST',
					'args'                => array(
						'warehouse_id'         => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'product_id'           => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'quantity'             => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'destination_stash_id' => array(
							'required'          => true,
							'type'              => 'string',
							'sanitize_callback' => 'yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid',
						),
					),
					'permission_callback' => function () {
						return current_user_can( 'yoink_check_items_in_out' );
					},
					'callback'            => function ( WP_REST_Request $request
					) {
						global $wpdb;

//						$custody_stash_id = 'u' . get_current_user_id();
						$current_user_id = get_current_user_id();
						$warehouse_id    = $request['warehouse_id'];
						$product_id      = $request['product_id'];
						$request_qty     = $request['quantity'];
						$destination_stash_id
						                 = $request['destination_stash_id'];

						$ret_val       = array();
						$stashes_table = $wpdb->prefix
						                 . 'yoinkinventory_stashes';

						$current_qty_query = <<<SQL
SELECT C.quantity
FROM {$wpdb->prefix}yoinkinventory_personal_custody C
WHERE C.user_id = %d AND C.product_id = %d
FOR UPDATE
SQL;

						$wpdb->query( 'START TRANSACTION' );

						try {
							$current_qty
								= intval( $wpdb->get_var( $wpdb->prepare(
								$current_qty_query,
//								"
//								SELECT S.quantity
//								FROM $stashes_table S
//								WHERE S.stash_id = %s AND S.product_id = %d
//								FOR UPDATE",
								$current_user_id, $product_id ) ) );
							if ( $current_qty == null ) {
								$current_qty = 0;
							}

//							$wpdb->query( $wpdb->prepare(
//								"",
//								$custody_stash_id, $product_id ) );

//							$deposit_qty = $request_qty;
//							$ret_val     = array();

							if ( $current_qty <= $request_qty ) {
								$delete_custody_query = <<<SQL
DELETE FROM {$wpdb->prefix}yoinkinventory_personal_custody
WHERE user_id = %d AND product_id = %d
SQL;
								$wpdb->query( $wpdb->prepare( $delete_custody_query,
									$current_user_id, $product_id ) );

								if ( $current_qty != $request_qty ) {
									$ret_val = array(
										// translators: %1$d: current quantity
										'warn'                => sprintf( __( 'Only recorded %1$d items deposited since that\'s how many you had on record.',
											'yoinkinventory' ), $current_qty ),
										'untransferred_count' => $request_qty
										                         - $current_qty
									);
								}
								$deposit_qty = $current_qty;
							} else {
								$leftover_qty = $current_qty - $request_qty;

								$reduce_custody_query = <<<SQL
UPDATE {$wpdb->prefix}yoinkinventory_personal_custody
SET quantity = %d
WHERE user_id = %d AND product_id = %d
SQL;
								$wpdb->query( $wpdb->prepare( $reduce_custody_query,
									$leftover_qty, $current_user_id,
									$product_id ) );
								$deposit_qty = $request_qty;
							}

							$wpdb->query( $wpdb->prepare(
								"INSERT INTO $stashes_table(warehouse_id, stash_id, product_id, quantity)
								VALUES(%d, %s, %d, %d)
								ON DUPLICATE KEY UPDATE quantity = quantity + %d",
								$warehouse_id, $destination_stash_id,
								$product_id, $deposit_qty, $deposit_qty ) );

							$wpdb->query( 'COMMIT' );
						} catch ( Exception $e ) {
							$wpdb->query( 'ROLLBACK' );
							throw $e; // ToDo: Log errors
						}

						$ret_val['new_total']
							= intval( $wpdb->get_var( $wpdb->prepare(
							"SELECT quantity
								FROM $stashes_table
								WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d",
							$warehouse_id, $destination_stash_id,
							$product_id ) ) );

//							ToDo: Email warehouse admin if any warnings
						$ret_val['deposited_qty'] = $deposit_qty;

						// error_log( print_r( $ret_val, true ) );
						return $ret_val;
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/commandeer-items',
				array(
					'methods'             => 'POST',
					'permission_callback' => function () {
						return current_user_can( 'yoink_set_absolute_quantity' );
					},
					'args'                => array(
						'warehouse_id' => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'takings'      => array(
							'description'       => __( 'A JSON array. Each object should have: stashId, productId and quantity',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => true,
							'sanitize_callback' => function ( $request_string
							) {
								return YoinkInvValidatedCommandeerTaking::sanitize_list_from_encoded_json( $request_string );
							}
						),
					),
					'callback'            => function ( WP_REST_Request $request
					) {
						global $wpdb;

						$takings = $request['takings'];

						$warehouse_id  = $request['warehouse_id'];
						$stashes_table = $wpdb->prefix
						                 . 'yoinkinventory_stashes';

						$wpdb->query( 'START TRANSACTION' );
						try {
							foreach ( $takings as $taking ) {
								// ToDo: Double-check other endpoints/usages for validation
								$stash_id       = $taking->get_stash_id();
								$product_id     = $taking->get_product_id();
								$quantity_taken = $taking->get_quantity();

								$existing_qty = $wpdb->get_var( $wpdb->prepare(
									"SELECT quantity
									FROM $stashes_table
									WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d
									FOR UPDATE",
									$warehouse_id, $stash_id, $product_id ) );

								if ( $existing_qty > $quantity_taken ) {
									$wpdb->query( $wpdb->prepare(
										"UPDATE {$stashes_table}
										SET quantity = quantity - %d
										WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d",
										$quantity_taken, $warehouse_id,
										$stash_id, $product_id ) );
									wc_update_product_stock( $product_id,
										$quantity_taken,
										'decrease' ); // 'increase' 'decrease':
								} else {
									$wpdb->query( $wpdb->prepare(
										"DELETE FROM {$stashes_table}
										WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d",
										$warehouse_id, $stash_id,
										$product_id ) );
									wc_update_product_stock( $product_id,
										$existing_qty,
										'decrease' ); // 'increase' 'decrease':
								}
							}

							$wpdb->query( 'COMMIT' );

							return array(
								'success' => true,
							);
						} catch ( Exception $e ) {
							$wpdb->query( 'ROLLBACK' );
							throw $e;
						}
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/absolute-stash-quantities', array(
					'methods'             => 'POST',
					'permission_callback' => function () {
						return current_user_can( 'yoink_set_absolute_quantity' );
					},
					'args'                => array(
						'warehouse_id' => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'adjustments'  => array(
							'description'       => __( 'A JSON array. Each object should have: stashId, productId and quantity',
								'yoinkinventory' ),
							'type'              => 'string',
							'required'          => true,
							'sanitize_callback' => function ( $request_string
							) {
//								include_once( YOINK_INV_ABSPATH
//								              . 'src/Classes/YoinkInvValidatedAbsoluteStashAdjustment.php' );

								return YoinkInvValidatedAbsoluteStashAdjustment::sanitize_list_from_encoded_json( $request_string );
							}
						),
					),
					'callback'            => function ( WP_REST_Request $request
					) {
						global $wpdb;

						$warehouse_id = $request['warehouse_id'];
						$adjustments  = $request['adjustments'];

						$stashes_table = $wpdb->prefix
						                 . 'yoinkinventory_stashes';

						$wpdb->query( 'START TRANSACTION' );
						try {
							foreach ( $adjustments as $adjustment ) {
								$stash_id   = $adjustment->get_stash_id();
								$product_id = $adjustment->get_product_id();
								$quantity   = $adjustment->get_quantity();

//								error_log( sprintf( 'absolute-stash-quantities: $stash_id=%s, $product_id=%d, $quantity=%d',
//									$stash_id, $product_id, $quantity ) );

								$existing_quantity_query = <<<SQL
SELECT quantity
FROM $stashes_table
WHERE warehouse_id = %d
  AND stash_id = %s
  AND product_id = %d
FOR UPDATE
SQL;

								$prior_qty
									= $wpdb->get_var( $wpdb->prepare(
									$existing_quantity_query,
									$warehouse_id, $stash_id, $product_id ) );
								if ( null == $prior_qty ) {
									$prior_qty = 0;
								}

								if ( $quantity > 0 ) {
									$update_stash_query = <<<SQL
UPDATE $stashes_table
SET quantity = %d
WHERE warehouse_id = %d
	AND stash_id = %s
	AND product_id = %d
SQL;

									$wpdb->query( $wpdb->prepare(
										$update_stash_query,
										$quantity, $warehouse_id, $stash_id,
										$product_id ) );
								} else {
									$quantity     = 0;
									$delete_query = <<<SQL
DELETE FROM $stashes_table
WHERE warehouse_id = %d
  AND stash_id = %s
  AND product_id = %d
SQL;

									$wpdb->query( $wpdb->prepare(
										$delete_query,
										$warehouse_id, $stash_id,
										$product_id ) );
								}

								$delta = $quantity - $prior_qty;
								if ( $delta < 0 ) {
									wc_update_product_stock( $product_id,
										abs( $delta ), 'decrease' );
								} else {
									wc_update_product_stock( $product_id,
										$delta, 'increase' );
								}
							}

							$wpdb->query( 'COMMIT' );

							return array(
								'success' => true,
							);
						} catch ( Exception $e ) {
							$wpdb->query( 'ROLLBACK' );
							throw $e;
						}
					},
				) );

			register_rest_route( $rest_api_namespace_plus_v1,
				'/quantity-exception',
				array(
					'methods'             => 'POST',
					'permission_callback' => function () {
						return current_user_can( 'yoink_ship_items' );
					},
					'args'                => array(
						'warehouse_id'   => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'stash_id'       => array(
							'type'              => 'string',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_sanitize_stash_id_string_or_return_err_if_invalid',
						),
						'product_id'     => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'expected_qty'   => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_positive_int',
						),
						'actual_qty'     => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_non_negative_int',
						),
						'quantity_taken' => array(
							'type'              => 'integer',
							'required'          => true,
							'sanitize_callback' => 'yoink_inv_try_parse_non_negative_int'
						),
						'taking_reason'  => array(
							'type'              => 'string',
							'required'          => true,
							'sanitize_callback' => 'sanitize_text_field'
						)
					),
					'callback'            => function ( WP_REST_Request $request
					) {
						global $wpdb;

						$warehouse_id   = $request['warehouse_id'];
						$stash_id       = $request['stash_id'];
						$product_id     = $request['product_id'];
						$actual_qty     = $request['actual_qty'];
						$quantity_taken = $request['quantity_taken'];
						$taking_reason  = $request['taking_reason'];

						$wpdb->query( 'START TRANSACTION' );

						try {
							$current_qty = $wpdb->get_var( $wpdb->prepare(
								"SELECT quantity
								FROM {$wpdb->prefix}yoinkinventory_stashes
								WHERE warehouse_id = %d
								  AND stash_id = %s
								  AND product_id = %d
								FOR UPDATE",
								$warehouse_id, $stash_id, $product_id ) );

							if ( null != $current_qty ) {
								$current_qty = intval( $current_qty );
							} else {
								$current_qty = 0;
							}

							$not_equal = false;
							if ( $actual_qty > $current_qty ) {
								wc_update_product_stock( $product_id,
									$actual_qty - $current_qty, 'increase' );
								$not_equal = true;
							} elseif ( $actual_qty < $current_qty ) {
								wc_update_product_stock( $product_id,
									$current_qty - $actual_qty, 'decrease' );
								$not_equal = true;
							}

							if ( $not_equal ) {
								if ( $actual_qty < 1 ) {
									$wpdb->query( $wpdb->prepare(
										"DELETE FROM {$wpdb->prefix}yoinkinventory_stashes
									WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d",
										$warehouse_id, $stash_id,
										$product_id ) );
								} else {
									$wpdb->query( $wpdb->prepare(
										"INSERT INTO {$wpdb->prefix}yoinkinventory_stashes
							( warehouse_id, stash_id, product_id, quantity ) VALUES
							                                                 (%d, %s, %d, %d)
									ON DUPLICATE KEY UPDATE quantity = %d",
										$warehouse_id, $stash_id, $product_id,
										$actual_qty, $actual_qty ) );
								}
							}

							$wpdb->query( 'COMMIT' );

						} catch ( Exception $e ) {
							$wpdb->query( 'ROLLBACK' );
							error_log( 'Error while adjusting counts for posted quantity exception. '
							           . $e->getMessage() );
							throw $e;
						}

						if ( $quantity_taken < 1 && ! $not_equal ) {
							return array(
								'warn' => array(
									'code'    => 'wasActuallyEnough',
									'message' => __( 'Actually the stash was already expected to contain that quantity. The 
									counts must have been updated recently.',
										'yoinkinventory' )
								)
							);
						}

						$warehouse_row = $wpdb->get_row( $wpdb->prepare(
							"SELECT name, alerts_email_addy
							FROM {$wpdb->prefix}yoinkinventory_warehouses
							WHERE warehouse_id = %d",
							$warehouse_id ) );

						$alerts_addy = $warehouse_row->alerts_email_addy;

						if ( null != $alerts_addy ) {
							$warehouse_name = $warehouse_row->name;
							$user_display
							                = wp_get_current_user()->display_name;
							$expected_qty   = $request['expected_qty'];
							$product_title  = $wpdb->get_var( $wpdb->prepare(
								"SELECT post_title
								FROM $wpdb->posts
								WHERE ID = %d",
								$product_id ) );

							$subject = __( 'Yoink: unexpected quantity',
								'yoinkinventory' );
							$body    = sprintf(
							// translators: %1$s: The reason the warehouse handler was visiting the inventory location, %2$s: the name of the warehouse handler, %3$d: the number of products they expected to find at the inventory location, %4$s: the title of the product with the unexpected quantity, %5$s: the name of the warehouse, %6$s: the "stash" identifier for the inventory location, %7$d: the quantity the warehouse handler took (towards fulfilling the purpose described in %1$s), %8$d: the actual product quantity remaining at the location
								__( 'While trying to %1$s, %2$s expected to find %3$d %4$s(s) at %5$s: %6$s. They took %7$d, leaving %8$d at that location. The quantity for the web store was adjusted.',
									'yoinkinventory' ), $taking_reason,
								$user_display, $expected_qty, $product_title,
								$warehouse_name, $stash_id, $quantity_taken,
								$actual_qty );

//							error_log( 'current_qty = ' . $current_qty
//							           . '; expected_qty = ' . $expected_qty );
							if ( $current_qty != $expected_qty ) {
								$body .= "\n\n"
								         . __( 'Some time probably passed after',
										'yoinkinventory' ) . ' ' . $user_display
								         .
								         __( 'loaded the order but before they arrived at the stash. The centrally recorded quantity was',
									         'yoinkinventory' ) . ' '
								         . $current_qty . '. ';
							}

//							$body .= "\n\n"
//							         . __( 'The system was updated with the actual quantity found on arrival at the stash.',
//									'yoinkinventory' );
							//  . __( 'The updated quantity on record was ', 'yoinkinventory' )
							$wc_mail             = new WC_Email();
							$wc_mail->email_type = 'plain';

							error_log( 'quantity exception body: ' . $body );

//							xdebug_break();
							// add_filter( 'wp_mail_content_type', array( $this, 'yoink_inv_text_mail' ) );
							$wc_mail->send( $alerts_addy, $subject, $body, '',
								array() );
							// remove_filter( 'wp_mail_content_type', array( $this, 'yoink_inv_text_mail' ) );
						}

						return array(
							'success' => true,
						);
					},
				) );
		} );
	}

	public
	function yoink_inv_get_order_details_by_product_name(
		WP_REST_Request $request
	) {
		$should_page_back
			= yoink_inv_sanitize_bool_default_false( $request['should_page_back'] );

		$page_from_order_item_name_isset
			= isset( $request['page_from_order_item_name'] );
		if ( $should_page_back && ! $page_from_order_item_name_isset
		) {
			return new WP_Error( 'yoink_inv_unexpected_pagination_direction',
				__( 'The should_page_back param can only be used when page_from_order_item_name is set',
					'yoinkinventory' ),
				array( 'status' => 400 )
			);
		}

		global $wpdb;

		// $DUPLICATE_KEY_ERR_NO = 1062;
		$warehouse_id = $request['warehouse_id'];
		$order_id     = $request['order_id'];

		$order_meta = $wpdb->get_row( $wpdb->prepare(
			"SELECT post_status, post_type
					FROM $wpdb->posts
					WHERE ID = %d",
			$order_id
		) );

		if ( 'shop_order' != $order_meta->post_type ) {
			return new WP_Error( 'yoink_inv_non_order_post_requested',
				sprintf(
				// translators: %s: The type of post that actually corresponds to the requested ID.
					__( 'The requested post_type does not appear to correspond to an order (actually requested a post of type \'%s\')',
						'yoinkinventory' ), $order_meta->post_type ),
				array(
					'status' => 415 // Try 406 if that doesn't work?
				) );
		}

		if ( 'wc-processing' != $order_meta->post_status ) {
			return new WP_Error( 'yoink_inv_invalid_order_status', sprintf(
			// translators: %s: The actual current status of the order.
				__( 'Order is no longer in processing status (actual status of order is \'%s\').',
					'yoinkinventory' ), $order_meta->post_status ), array(
				'status' => 410
			) );
		}

		// ToDo: Consider if we want to support/load claim stashes that might belong to other warehouses
		$states_by_item  = array();
		$usernames_by_id = array();

		$order_state_row = $wpdb->get_row( $wpdb->prepare(
			"SELECT user_id
			FROM {$wpdb->prefix}yoinkinventory_order_state
			WHERE order_id = %d",
			$order_id
		) );

		if ( null == $order_state_row ) {
			$order_claim_user_id = null;
		} else {
			$order_claim_user_id = absint( $order_state_row->user_id );
			if ( null != $order_claim_user_id ) {
				$usernames_by_id[ $order_claim_user_id ] = null;
			}
		}

		// ToDo: Paginate queries in this function
		$item_states = $wpdb->get_results( $wpdb->prepare(
			"SELECT order_item_id, user_id, state_id, quantity
					FROM {$wpdb->prefix}yoinkinventory_order_item_state
					WHERE order_id = %d",
			$order_id
		) );

		foreach ( $item_states as $item_state ) {
			$item_id = $item_state->order_item_id;
			if ( ! isset( $states_by_item[ $item_id ] ) ) {
				$states_by_item[ $item_id ] = array(
					$item_state,
				);
			} else {
				$states_by_item[ $item_id ][] = $item_state;
			}
			$usernames_by_id[ absint( $item_state->user_id ) ] = null;
		}

		if ( count( $usernames_by_id ) > 0 ) {
			$distinct_user_ids = array_keys( $usernames_by_id );

			$user_rows = $wpdb->get_results( $wpdb->prepare(
				"SELECT ID, display_name
						FROM $wpdb->users
						WHERE ID IN( " . implode( ',',
					array_fill( 0, count( $distinct_user_ids ), '%d' ) )
				. ')', $distinct_user_ids )
			);

			foreach ( $user_rows as $user_row ) {
				$usernames_by_id[ $user_row->ID ] = $user_row->display_name;
			}
		}

		// $claim_qty_by_product_id = array();
		// foreach ( $claim_stashes as $claim_stash ) {
		// 	$claim_qty_by_product_id[ $claim_stash->product_id ] = $claim_stash->quantity;
		// }

		$max_item_count = 130;

//		ToDo: Search this project for `woocommerce_`, see if there's an API that we can call instead of reading tables directly.
//		      Test against older and newer versions of WooCommerce
		$order_item_query        = <<<SQL
SELECT order_item_id, order_item_name
FROM {$wpdb->prefix}woocommerce_order_items
WHERE order_id = %d
	AND order_item_type = 'line_item'
SQL;
		$order_item_query_params = array(
			$order_id,
		);
		if ( $page_from_order_item_name_isset ) {
			$order_item_query .= 'AND order_item_name ';
			if ( $should_page_back ) {
				$order_item_query .= '<';
			} else {
				$order_item_query .= '>';
			}
			$order_item_query .= ' %s';

			$order_item_query_params[] = $request['page_from_order_item_name'];
		}

		$order_item_query          .= 'ORDER BY order_item_name LIMIT %d';
		$order_item_query_params[] = $max_item_count;

		$line_item_rows = $wpdb->get_results( $wpdb->prepare( $order_item_query,
			$order_item_query_params ) );

//		$order_item_names_by_id = array();
		$line_item_results       = array();
		$distinct_order_item_ids = array();

		if ( $should_page_back ) {
			$index = count( $line_item_rows );
			while ( $index ) {
				$line_item_row             = $line_item_rows[ -- $index ];
				$order_item_id
				                           = intval( $line_item_row->order_item_id );
				$distinct_order_item_ids[] = $order_item_id;
				$line_item_results[]       = array(
					// 16 + 9 + 1 bytes
					'order_item_id' => $order_item_id,
					// 15 + 255 + 1 bytes
					'product_name'  => $line_item_row->order_item_name,
				);
			}
		} else {
			foreach ( $line_item_rows as $line_item_row ) {
				$order_item_id             = intval( $line_item_row->order_item_id );
				$distinct_order_item_ids[] = $order_item_id;
				$line_item_results[]       = array(
					// 16 + 9 + 1 bytes
					'order_item_id' => $order_item_id,
					// 15 + 255 + 1 bytes
					'product_name'  => $line_item_row->order_item_name,
				);
			}
		}

		$dibs_state_id = 1;

//		$distinct_order_item_ids = array_keys( $order_item_names_by_id );

		$order_item_meta_query = <<<SQL
SELECT order_item_id, meta_key, meta_value
FROM {$wpdb->prefix}woocommerce_order_itemmeta P
WHERE meta_key IN( '_product_id', '_variation_id', '_qty' )
	AND order_item_id IN(
SQL;

		$order_item_meta_query        .= implode( ', ',
				array_fill( 0, count( $distinct_order_item_ids ), '%d' ) )
		                                 . ')';
		$order_item_meta_query_params = $distinct_order_item_ids;

		$order_item_metas
			= $wpdb->get_results( $wpdb->prepare( $order_item_meta_query,
			$order_item_meta_query_params ) );

		$product_ids_by_order_item_id     = array();
		$variation_ids_by_order_item_id   = array();
		$quantity_values_by_order_item_id = array();

		foreach ( $order_item_metas as $meta ) {
			$meta_value = $meta->meta_value;
			if ( '_product_id' == $meta->meta_key ) {
				$product_ids_by_order_item_id[ $meta->order_item_id ]
					= $meta_value;
			} elseif ( '_variation_id' == $meta->meta_key ) {
				$variation_ids_by_order_item_id[ $meta->order_item_id ]
					= $meta_value;
			} elseif ( '_qty' == $meta->meta_key ) {
				$quantity_values_by_order_item_id[ $meta->order_item_id ]
					= intval( $meta_value );
			} else {
				return new WP_Error( 'yoink_inv_unexpected_error', sprintf(
				// translators: %s: The meta key that wasn't handled.
					__( 'Unexpected meta key: \'%s\'', 'yoinkinventory' )
					, $meta->meta_key ), array(
					'status' => 500
				) );
			}
		}

		$needed_product_ids_set = array();

		foreach ( $line_item_results as &$line_item_result ) {
			$order_item_id      = intval( $line_item_result['order_item_id'] );
			$line_item_warnings = array();

			if ( isset( $quantity_values_by_order_item_id[ $order_item_id ] ) ) {
				$order_quantity
					= $quantity_values_by_order_item_id[ $order_item_id ];
			} else {
				$order_quantity       = null;
				$line_item_warnings[] = __( 'Missing quantity record',
					'yoinkinventory' );
			}

			$variation_id = '0';
			if ( isset( $variation_ids_by_order_item_id[ $order_item_id ] ) ) {
				$variation_id
					= $variation_ids_by_order_item_id[ $order_item_id ];
			}

			$product_id = null;
			if ( $variation_id != '0' ) {
				$product_id                     = intval( $variation_id );
				$line_item_result['product_id'] = $product_id;
			} elseif ( isset( $product_ids_by_order_item_id[ $order_item_id ] ) ) {
				$product_id
					                            = intval( $product_ids_by_order_item_id[ $order_item_id ] );
				$line_item_result['product_id'] = $product_id;
			} else {
				$line_item_result['product_id'] = null;
				$line_item_warnings[]
				                                = __( 'Neither variation ID nor product ID record found',
					'yoinkinventory' );
			}

//			$item_id = intval( $line_item_row->order_item_id );
//
//			// error_log( '$item_id: ' . $item_id );
//			// ToDo: Avoid repeated calls in loop--use batch query. Old: Either use prepared statement (via direct connection) or stored procedure for repeated calls in loop.
//			$metas = $wpdb->get_results( $wpdb->prepare(
//				"SELECT meta_key, meta_value
//				FROM {$wpdb->prefix}woocommerce_order_itemmeta P
//				WHERE order_item_id = %d
//				  AND meta_key IN( '_product_id', '_variation_id', '_qty' )",
//				$item_id
//			) );

			// error_log('lineItem metas:');
			// YoinkInvWooPlugin::yoink_inv_debug_log($metas);

//			$product_id = 0;
//			foreach ( $metas as $meta ) {
//				if ( '_product_id' == $meta->meta_key ) {
//					if ( 0
//					     == $product_id
//					) { // Only if we haven't already taken a variation ID.
//						$product_id = intval( $meta->meta_value );
//					}
//					continue;
//				}
//				if ( '_variation_id' == $meta->meta_key ) {
//					if ( '0'
//					     != $meta->meta_value
//					) { // Takes precedence over non-variation product ID when non-zero.
//						$product_id = intval( $meta->meta_value );
//					}
//					continue;
//				}
//				if ( '_qty' == $meta->meta_key ) {
//					$order_quantity = intval( $meta->meta_value );
//					continue;
//				}
//			}

//			(26 + 23 + 271 + 27 + 1 comma + 2 curly) = 350
//			$line_item_result = array(
//				// 16 + 9 + 1 bytes
//				'order_item_id'  => $item_id,
//				// 13 + 9 + 1 bytes
//				'product_id'     => $product_id,
//				// 15 + 255 + 1 bytes
//				'product_name'   => $line_item_row->order_item_name,
//				// 17 + 9 + 1 bytes
//				'order_quantity' => $order_quantity,
//			);

			$already_taken = 0;
			if ( isset( $states_by_item[ $order_item_id ] ) ) {
				$item_states = $states_by_item[ $order_item_id ];
				$adjustments = array();

				// assume 10 adjustment entries * (20 + 133 + 14 + 15 + 1 comma + 2 curly) = 1850
				foreach ( $item_states as $item_state ) {
					$state_id = intval( $item_state->state_id );
					$quantity = intval( $item_state->quantity );
					$user_id  = intval( $item_state->user_id );

					if ( isset( $usernames_by_id[ $user_id ] ) ) {
						$user_display = $usernames_by_id[ $user_id ];
					} else {
						$user_display = sprintf(
						// translators: %d: The numeric identifier for the user with the missing display name
							__( '%d (missing display)', 'yoinkinventory' )
							, $user_id );
					}

					$adjustments[] = array(
						// 10 + 9 + 1 bytes
						'user_id'  => $user_id,
						// 7 + 125 + 1 bytes
						'user'     => $user_display,
						// 11 + 2 + 1 bytes
						'state_id' => $state_id,
						// 6 + 9 bytes
						'qty'      => $quantity,
					);

					if ( $quantity < 1 ) {
						$line_item_warnings[] = sprintf(
						// translators: %d: The ID of the corrupted order item state record
							__( 'Negative quantity for order item state %d',
								'yoinkinventory' ), $order_item_id );
					}

					if ( $state_id != $dibs_state_id ) {
						$already_taken += $quantity;
					}
				}
				$line_item_result['adjustments'] = $adjustments;
			}

			if ( $order_quantity < 1 ) {
				$line_item_warnings[]
					= __( 'Quantity less than one', 'yoinkinventory' );
			}

			if ( $product_id != null && $order_quantity > $already_taken ) {
				$needed_product_ids_set[ $product_id ] = null;
			}

			if ( $already_taken > $order_quantity ) {
				$line_item_warnings[]
					= __( 'Sum of taken records exceeds required order quantity',
					'yoinkinventory' );
			}

			if ( count( $line_item_warnings ) > 0 ) {
				$line_item_result['warnings'] = $line_item_warnings;
			}

			$line_item_result['order_quantity'] = $order_quantity;
			// error_log('product ID: ' . $product_id);
		}


		$distinct_product_ids = array_keys( $needed_product_ids_set );
		$stash_query          = <<<SQL
SELECT product_id, warehouse_id, warehouse_name, stash_id, quantity FROM
(SELECT S.product_id, S.warehouse_id,
	W.name as warehouse_name,
	S.stash_id,
	S.quantity,
	ROW_NUMBER() OVER (PARTITION BY S.product_id ORDER BY S.warehouse_id = %d DESC, S.quantity DESC) AS row_num
FROM {$wpdb->prefix}yoinkinventory_stashes S
	JOIN {$wpdb->prefix}yoinkinventory_warehouses W
		ON S.warehouse_id = W.warehouse_id
	AND S.product_id IN (
SQL;
		$stash_query_params   = array( $warehouse_id );
		$stash_query          .= implode( ',',
				array_fill( 0, count( $distinct_product_ids ), '%d' ) )
		                         . ')) SubQ WHERE row_num < 21';

		$stash_query_params = array_merge( $stash_query_params,
			$distinct_product_ids, array( $warehouse_id ) );

		$stash_rows = $wpdb->get_results( $wpdb->prepare( $stash_query,
			$stash_query_params )
		);

		$stash_entries_by_product_id = array();
		if ( $stash_rows != null ) {
			foreach ( $stash_rows as $stash_row ) {
				$product_id = intval( $stash_row->product_id );
				if ( isset( $stash_entries_by_product_id[ $product_id ] ) ) {
					$stash_entries
						= &$stash_entries_by_product_id[ $product_id ];
				} else {
					$stash_entries = array();
					$stash_entries_by_product_id[ $product_id ]
					               = &$stash_entries;
				}

				// assume 20 stashes betwixt warehouses * (25 + 113 + 107 + 20 + 1 comma + 2 curly) = 5360
				$stash_entries[] = array(
					// 15 + 9 + 1 bytes
					'warehouse_id'   => intval( $stash_row->warehouse_id ),
					// 17 + 95 + 1 bytes
					'warehouse_name' => $stash_row->warehouse_name,
					// 11 + 95 + 1 bytes
					'stash_id'       => $stash_row->stash_id,
					// 11 + 9 bytes
					'quantity'       => intval( $stash_row->quantity ),

//						ToDo: Can we define custom index logic in MySql to maintain a priority order by successive stash indexes?
//				'sort_operand'   => array_map( function ( $x ) {
//					return intval( $x );
//				}, explode( ',', $stash_id ) ),
				);
				unset( $stash_entries );
			}
		}

		foreach ( $line_item_results as &$line_item_result ) {
			$product_id = $line_item_result['product_id'];
			if ( $product_id == null ) {
				continue;
			}

			if ( isset( $stash_entries_by_product_id[ $product_id ] ) ) {
				$stashes = &$stash_entries_by_product_id[ $product_id ];

				// ToDo: Maintain pre-aggregated sums in DB. E.g., total availability by product ID across all stashes in warehouse. Also, total taking adjustments across all internal users helping to fill an order.
				//       Enable quick fetch ordered by products which cannot be filled from the current and/or any warehouse to give users early warning.
				usort( $stashes,
					function ( $a, $b ) use ( $warehouse_id ) {
						$wa = $a['warehouse_id'];
						$wb = $b['warehouse_id'];
						if ( $wa != $wb ) {
							if ( $wa == $warehouse_id ) {
								return - 1;
							}

							if ( $wb == $warehouse_id ) {
								return 1;
							}
						}
// ToDo: Solve the Travelling Salesman Problem ;)
//       There are probably some better ways to choose a recommended stash for a given line-item.
//       ...even if those ways are actually NP-complete (I suspect they are), N is probably small
//       in the context of a single order. In the worst case, the computation could be moved to the
//       client device to distribute the load. There're probably also some heuristic approaches.

						return $b['quantity'] - $a['quantity'];
					} );

				$line_item_result ['stashes']
					= &$stashes;
			}
		}

//		foreach ( $line_item_results as $line_item_result ) {
//			$stashes = array();
		// if ( isset( $claim_qty_by_product_id[ $product_id ] ) ) {
		// 	$qty_in_personal_custody = $claim_qty_by_product_id[ $product_id ];
		// } else {
		// 	$qty_in_personal_custody = 0;
		// }

//			if ( $already_taken < $order_quantity ) {
//				$stash_rows = $wpdb->get_results( $wpdb->prepare(
//					"SELECT S.warehouse_id,
//								W.name as warehouse_name,
//								S.stash_id,
//								S.quantity
//							FROM {$wpdb->prefix}yoinkinventory_stashes S
//								JOIN {$wpdb->prefix}yoinkinventory_warehouses W
//									ON S.warehouse_id = W.warehouse_id
//							WHERE S.product_id = %d",
//					$product_id
//				) );

		// error_log('stash_rows:');
		// YoinkInvWooPlugin::yoink_inv_debug_log($stash_rows);

//				foreach ( $stash_rows as $stash_row ) {
//					$stash_id = $stash_row->stash_id;
//
//					// assume 20 stashes betwixt warehouses * (25 + 113 + 107 + 20 + 1 comma + 2 curly) = 5360
//					$stash_entry = array(
//						// 15 + 9 + 1 bytes
//						'warehouse_id'   => intval( $stash_row->warehouse_id ),
//						// 17 + 95 + 1 bytes
//						'warehouse_name' => $stash_row->warehouse_name,
//						// 11 + 95 + 1 bytes
//						'stash_id'       => $stash_id,
//						// 11 + 9 bytes
//						'quantity'       => intval( $stash_row->quantity ),
//
////						ToDo: Can we define custom index logic in MySql to maintain a priority order by successive stash indexes?
//						'sort_operand'   => array_map( function ( $x ) {
//							return intval( $x );
//						}, explode( ',', $stash_id ) ),
//					);

		// error_log( 'stash_id: ' . $stash_id );
		// if ( preg_match( '/^\d/u', $stash_id ) ) {
		// $stash_entry['sort_operand'] = array_map( function( $x ) {
		// 	return intval( $x );
		// 	}, explode( ',', $stash_id ) );
		// } else {
		// 	$stash_entry['sort_operand'] = array( PHP_INT_MAX );

		// 	if ( preg_match( '/^u(\d+)(o)/u', $stash_id, $matches ) ) {
		// 		if ( 'o' != $matches[2] ) {
		// 			$other_user_id = $matches[1];
		// 			error_log( 'other_user_id: ' . $other_user_id );

		// 			if ( isset( $users_by_id_cache[ $other_user_id ] ) ) {
		// 				$stash_entry['other_user'] = $users_by_id_cache[ $other_user_id ];
		// 			} else {
		// 				$other_user = $wpdb->get_var( $wpdb->prepare( "SELECT display_name
		// 						FROM {$wpdb->users_set}
		// 						WHERE ID = %d",
		// 					$other_user_id) );

		// 				$users_by_id_cache[$other_user_id] = $other_user;
		// 				$stash_entry['other_user'] = $other_user;
		// 			}
		// 		}
		// 	}
		// }

//					$stashes[] = $stash_entry;
//				}

//		// ToDo: Maintain pre-aggregated sums. E.g., total availability by product ID across all stashes in warehouse. Total taking adjustments across all internal users helping to fill an order.
//		//       Enable quick fetch ordered by products which cannot be filled from the current and/or any warehouse.
//		usort( $stashes,
//			function ( $a, $b ) use ( $warehouse_id ) {
//				$wa = $a['warehouse_id'];
//				$wb = $b['warehouse_id'];
//				if ( $wa != $wb ) {
//					if ( $wa == $warehouse_id ) {
//						return - 1;
//					}
//
//					if ( $wb == $warehouse_id ) {
//						return 1;
//					}
//				}
//// ToDo: Solve the Travelling Salesman Problem ;)
////       There are probably some better ways to choose a recommended stash for a given line-item.
////       ...even if those ways are actually NP-complete (I suspect they are), N is probably small
////       in the context of a single order. In the worst case, the computation could be moved to the
////       client device to distribute the load. There're probably also some heuristic approaches.
//
//				return $b['quantity'] - $a['quantity'];
//			} );
//	}

//$line_item_result ['stashes']
//= $stashes;

//$line_items[]
//= $line_item_result;

// array(
// 	'product_id' => $product_id,
// 	'product_name' => $line_item_row->order_item_name,
// 	'order_quantity' => $order_quantity,
// 	'qty_in_personal_custody' => $qty_in_personal_custody,
// 	'stashes' => $stashes,
// );
//		}

//		usort( $line_items, function ( $a, $b ) use ( $warehouse_id ) {
//			$stashes_a = $a['stashes'];
//			$stashes_b = $b['stashes'];
//			$ca        = count( $stashes_a );
//			$cb        = count( $stashes_b );
//			if ( $ca < 1 ) {
//				return - 1;
//			}
//
//			if ( $cb < 1 ) {
//				return 1;
//			}
//
//			$sa0 = $stashes_a[0];
//			$sb0 = $stashes_b[0];
//			$wa  = $sa0['warehouse_id'];
//			$wb  = $sb0['warehouse_id'];
//			if ( $wa != $wb ) {
//				if ( $wa != $warehouse_id ) {
//					return - 1;
//				}
//
//				return 1;
//			}
//
//			$ao = $sa0['sort_operand'];
//			$bo = $sb0['sort_operand'];
//
//			$min_index = min( count( $ao ), count( $bo ) );
//			for ( $i = 0; $i < $min_index; $i ++ ) {
//				$aoi = $ao[ $i ];
//				$boi = $bo[ $i ];
//				if ( $aoi != $boi ) {
//					return $aoi - $boi;
//				}
//			}
//
//			return 0;
//		} );
//
//		foreach ( $line_items as $line_item ) {
//			foreach ( $line_item['stashes'] as $stash ) {
//				unset( $stash['sort_operand'] );
//			}
//		}

		$ret_val = array(
			'line_items'     => $line_item_results,
			'max_item_count' => $max_item_count
		);

		if ( null != $order_claim_user_id ) {
			$order_claim_user_display
				= $usernames_by_id[ $order_claim_user_id ];
			if ( $order_claim_user_display == null ) {
				$order_claim_user_display = sprintf(
				// translators: %d: The numeric identifier for the user with the missing display name
					__( '%d (missing display)', 'yoinkinventory' )
					, $order_claim_user_id );
			}

			// 15 + 138 + 3 = 156
			$ret_val['claimant'] = array(
				// 5 + 9 + 1
				'id'      => $order_claim_user_id,
				// 10 + 128
				'display' => $order_claim_user_display
			);
		}

// error_log( 'try-claim-order result:' );
// YoinkInvWooPlugin::yoink_inv_debug_log( $ret_val );

		return $ret_val;
	}

	public
	function yoink_inv_on_receive_inventory_post(
		WP_REST_Request $request
	) {
		// YoinkInvWooPlugin::yoink_inv_debug_log("yoink_inv_on_receive_inventory_post called");
		// YoinkInvWooPlugin::yoink_inv_debug_log($request);

		global $wpdb;

		$warehouse_id   = $request['warehouse_id'];
		$stash_id       = $request['stash_id'];
		$quantity_added = $request['quantity_added'];
		$product_id     = $request['product_id'];

//		return array(
//			'warehouse_id'   => $warehouse_id,
//			'stash_id'       => $stash_id,
//			'quantity_added' => $quantity_added,
//			'product_id'     => $product_id
//		);

		$stashes_table = $wpdb->prefix . 'yoinkinventory_stashes';

		$wpdb->query( 'START TRANSACTION' );
		try {
			$previous_quantity = $wpdb->get_var( $wpdb->prepare(
				"SELECT quantity
				FROM $stashes_table
				WHERE warehouse_id = %d AND stash_id = %s AND product_id = %d
				FOR UPDATE",
				$warehouse_id, $stash_id, $product_id ) );

			// ToDo: Simplify this with ON DUPLICATE KEY UPDATE

			if ( null == $previous_quantity ) {
				$previous_quantity = 0;
				$new_quantity      = $previous_quantity + $quantity_added;
				YoinkInvWooPlugin::yoink_inv_debug_log( 'Receiving inventory: '
				                                        . $previous_quantity
				                                        . ' + '
				                                        . $quantity_added
				                                        . ' = '
				                                        . $new_quantity );
				$wpdb->insert( $stashes_table, array(
					'warehouse_id' => $warehouse_id,
					'stash_id'     => $stash_id,
					'product_id'   => $product_id,
					'quantity'     => $new_quantity,
				), array( '%d', '%s', '%d', '%d' ) );
			} else {
				$previous_quantity = intval( $previous_quantity );
				$new_quantity      = $previous_quantity + $quantity_added;
				YoinkInvWooPlugin::yoink_inv_debug_log( 'Receiving inventory: '
				                                        . $previous_quantity
				                                        . ' + '
				                                        . $quantity_added
				                                        . ' = '
				                                        . $new_quantity );
				$wpdb->update(
					$stashes_table,
					array(
						'quantity' => $new_quantity,
					),
					array(
						'warehouse_id' => $warehouse_id,
						'stash_id'     => $stash_id,
						'product_id'   => $product_id,
					),
					array( '%d' ),
					array( '%d', '%s', '%d' ) );
			}

			wc_update_product_stock( $product_id, $quantity_added,
				'increase' ); // 'increase' 'decrease':

			// YoinkInvWooPlugin::yoink_inv_debug_log('Committing transaction');
			$wpdb->query( 'COMMIT' );

			return array(
				'previous_quantity' => $previous_quantity,
				'new_quantity'      => $new_quantity,
				'quantity_added'    => $quantity_added,
			);
		} catch ( Exception $e ) {
			$wpdb->query( 'ROLLBACK' );
			throw $e; // ToDo: Log errors
		}
	}

	public
	function yoink_inv_claim_order(
		$order_id, $user_id, $existing_claim_user_id
	) {
		global $wpdb;

		$claim_state_id = 1; // claimed

		$wpdb->query( 'START TRANSACTION' );

		if ( null == $existing_claim_user_id ) {
			$wpdb->query( $wpdb->prepare(
				"INSERT INTO {$wpdb->prefix}yoinkinventory_order_state
					(order_id, state_id, user_id) VALUES
					(%d, %d, %d) ON DUPLICATE KEY UPDATE order_id=order_id",
				$order_id, $claim_state_id, $user_id ) );
		} else {
			$wpdb->query( $wpdb->prepare(
				"INSERT INTO {$wpdb->prefix}yoinkinventory_order_state
				(order_id, state_id, user_id) VALUES
				(%d, %d, %d) ON DUPLICATE KEY UPDATE user_id =
					IF(user_id = %d, %d, user_id)",
				$order_id, $claim_state_id, $user_id, $existing_claim_user_id,
				$user_id
			) );
			// "UPDATE {$wpdb->prefix}yoinkinventory_order_state
			// SET user_id = %d
			// WHERE order_id = %d",
			// $user_id, $order_id ) );
		}

		// try {
		// 	$existing_row = $wpdb->get_row( $wpdb->prepare(
		// 			"SELECT user_id, state_id
		// 			FROM {$wpdb->prefix}yoinkinventory_order_state
		// 			WHERE order_id = %d
		// 			FOR UPDATE",
		// 		$order_id
		// 	) );

		// 	$claim_state_id = 1;

		// 	if ( null == $existing_row ) {
		// 		$wpdb->query( $wpdb->prepare(
		// 				"INSERT INTO {$wpdb->prefix}yoinkinventory_order_state
		// 				(order_id, state_id, user_id) VALUES
		// 				(%d, %d, %d)",
		// 			$order_id, $claim_state_id, $user_id ) );
		// 		$claim_user_id = $user_id;
		// 	} else {
		// 		if ( $existing_row->state_id == $claim_state_id ) {
		// 			if ( $should_seize ) {
		// 				$wpdb->query( $wpdb->prepare(
		// 						"UPDATE {$wpdb->prefix}yoinkinventory_order_state
		// 						SET user_id = %d
		// 						WHERE order_id = %d",
		// 					$user_id, $order_id ) );
		// 				$claim_user_id = $user_id;
		// 			} else {
		// 				$claim_user_id = $existing_row->user_id;
		// 			}
		// 		} else {
		// 			$claim_user_id = null;
		// 		}
		// 	}

		// 	$wpdb->query( 'COMMIT' );

		// } catch ( Exception $e ) {
		// 	$wpdb->query( 'ROLLBACK' );
		// 	throw $e; // ToDo: Log errors
		// }

		// if ( null != $claim_user_id ) {
		// $display_name = $wpdb->get_var( $wpdb->prepare(
		// 	"SELECT display_name
		// 		FROM {$wpdb->users}
		// 		WHERE ID = %d",
		// 	$claim_user_id ) );

		$updated_claimant = $wpdb->get_row( $wpdb->prepare(
			"SELECT S.user_id, U.display_name
				FROM {$wpdb->prefix}yoinkinventory_order_state S
					JOIN $wpdb->users U
						ON S.user_id = U.ID
				WHERE S.order_id = %d",
			$order_id ) );

		// }
		// else {
		// 	$display_name = null;
		// }

		$wpdb->query( 'COMMIT' );

		if ( empty( $updated_claimant ) ) {
			// Create the response object
			$response = new WP_REST_Response( array(
				'error' => array(
					'code'    => 'yoink_inventory_no_claim_registered',
					'message' => __( 'The claim was not recorded due to a problem in the database.',
						'yoinkinventory' )
				),
			) );

// Add a custom status code
			$response->set_status( 500 );

// Add a custom header
			return $response;
//			return new WP_Error(
//				'yoink_inventory_no_claim_registered',
//				__( 'The claim was not recorded due to a problem in the database.' ),
//				array( 'status' => 500 )
//			);
		}

		return array(
			'user_id'      => $updated_claimant->user_id,
			'display_name' => $updated_claimant->display_name,
		);

		// $wpdb->suppress_errors( ! $should_seize );

		// $base_qry = "INSERT INTO {$wpdb->prefix}yoinkinventory_order_state (order_id, user_id) VALUES (%d, %d)";
		// if ( $should_seize ) {
		// 	$qry = $wpdb->prepare($base_qry . ' ON DUPLICATE KEY UPDATE user_id=%d',
		// 		$order_id, $user_id, $user_id);
		// } else {
		// 	$qry = $wpdb->prepare( $base_qry, $order_id, $user_id );
		// }
		// $wpdb->query( $qry );
		// // error_log('last_error: ->'. $wpdb->last_error . '<-');
		// $wpdb->suppress_errors( false );

		// if ( '' != $wpdb->last_error && ( $should_seize || substr( $wpdb->last_error, 0, 15 ) != 'Duplicate entry' ) ) {
		// 	// error_log('printing error.');
		// 	// ToDo Better client-side (JSON-based) error reporting for all endpoints. For now, swallow Duplicate entry errors.
		// 	$wpdb->print_error();
		// }

		// $order_claim = $wpdb->get_row( $wpdb->prepare(
		// 	"SELECT C.user_id, U.display_name
		// 		FROM {$wpdb->prefix}yoinkinventory_order_claims C
		// 			JOIN {$wpdb->users} U
		// 				ON C.user_id = U.ID
		// 		WHERE C.order_id = %d",
		// 	$order_id ) );

		// // if ( $should_seize ) {
		// // 	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->prefix}yoinkinventory_stashes
		// // 			SET stash_id = %s WHERE stash_id LIKE 'u%%o%d'",
		// // 		'u' . $order_claim->user_id . 'o' . $order_id, $order_id) );
		// // }

		// $wpdb->query( 'COMMIT' );

		// return $order_claim;

	}

//	function yoink_inv_is_valid_stash_id( $stash_id ) {
//		return yoink_inv_is_valid_stash_id( $stash_id );
//	}

//	public function yoink_inv_is_non_empty( $value ) {
//		return '' != $value;
//	}
//
//	public function yoink_inv_sanitrim( $value, $request, $key ) {
//		return trim( $value );
//	}
//
//	public function yoink_inv_text_mail() {
//		error_log( 'setting mail content type to text/plain' );
//		return 'text/plain';
//	}

// private function disclaim_order($order_id) {
// 	global $wpdb;

// 	$wpdb->delete("{$wpdb->prefix}yoinkinventory_order_claims", array(
// 		'order_id' => $order_id,
// 		'user_id' => get_current_user_id()
// 	), array(
// 		'%d', '%d'
// 	));
// }

// wc_update_product_stock($product, $stock_quantity, $operation); // 'increase' 'decrease':

}

new YoinkInvRestApi();
