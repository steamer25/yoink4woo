<?php

namespace YoinkInvTest\unit;

//require_once ( '../../../wooContainer/varWwwHtml/wp-includes/rest-api/class-wp-rest-request.php' );
//require_once ( '../../../wooContainer/varWwwHtml/wp-includes/plugin.php' );

use PHPUnit\Framework\TestCase;

//use WP_REST_Request;

class TestMisc extends TestCase {
	public function testReassign() {
		$decoded_json = json_decode("{\"foo\": \"bar\"}");
		fwrite(STDOUT, print_r($decoded_json->foo, TRUE));

		$decoded_json->foo = intval('7', 10);
		fwrite(STDOUT, print_r($decoded_json->foo, TRUE));
	}

	public function testIsList() {
//		self::assertFalse( array_is_list( '' ) );
		self::assertFalse( is_array( '' ) );
		self::assertTrue( is_array( array('a' => 1) ) );
		self::assertTrue( is_array( [] ) );
	}

	public function testInterpolation() {
		$foo = "bar";
		fwrite(STDOUT, print_r("Lorem
		$foo
		Ipsum", TRUE));
	}
}
