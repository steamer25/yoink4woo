<?php

namespace unit\Includes;

//fwrite( STDOUT, 'TestC' );

define( 'ABSPATH', dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) ) . '/wooContainer/varWwwHtml/' );
define( 'WP_CONTENT_DIR', dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) ) . '/wooContainer/varWwwHtml/wp-content/' );
define( 'WPINC', 'wp-includes' );

//fwrite( STDOUT, 'TestD' );

//require_once ( '../../../../wooContainer/varWwwHtml/wp-includes/plugin.php' );
// ToDo: Fix loading
//       Try: https://cmljnelson.blog/2021/01/05/phpunit-tests-in-wordpress-plugins-without-wp-cli-or-wp-develop-repo/
//require_once( dirname( dirname( dirname( dirname( __FILE__ ) ) ) )
//              . '/src/Includes/validation_utils.php' );
require_once( 'src/Includes/validation-utils.php' );
//require_once( '../wooContainer/varWwwHtml/wp-includes/load.php' );
//require_once( '../wooContainer/varWwwHtml/wp-includes/class-wp-error.php' );
//require_once( '../wooContainer/varWwwHtml/wp-includes/l10n.php' );
//require_once( '../wooContainer/varWwwHtml/wp-includes/plugin.php' );
//require_once( '../wooContainer/varWwwHtml/wp-includes/option.php' );
//require_once( '../wooContainer/varWwwHtml/wp-includes/cache.php' );
//require_once( '../wooContainer/varWwwHtml/wp-settings.php' );


//fwrite( STDOUT, 'TestE' );

use PHPUnit\Framework\TestCase;

//use YoinkInv\yoink_inv_remove_whitespace;

//use WP_REST_Request;

class TestValidationUtils extends TestCase {
	public function testNumeric() {
//		wp_set_lang_dir();
//		wp_cache_init();

		self::assertTrue( is_wp_error( yoink_inv_try_parse_non_negative_int( 'a' ) ) );
		self::assertFalse( yoink_inv_try_parse_non_negative_int( '-1' ) );
		self::assertFalse( yoink_inv_try_parse_non_negative_int( - 1 ) );

		self::assertTrue( yoink_inv_try_parse_non_negative_int( '0' ) );
		self::assertTrue( yoink_inv_try_parse_non_negative_int( '1' ) );
		self::assertTrue( yoink_inv_try_parse_non_negative_int( 0 ) );
		self::assertTrue( yoink_inv_try_parse_non_negative_int( 1 ) );

		self::assertFalse( yoink_inv_try_parse_positive_int( 'a' ) );
		self::assertFalse( yoink_inv_try_parse_positive_int( '-1' ) );
		self::assertFalse( yoink_inv_try_parse_positive_int( - 1 ) );
		self::assertFalse( yoink_inv_try_parse_positive_int( '0' ) );
		self::assertFalse( yoink_inv_try_parse_positive_int( 0 ) );

		self::assertTrue( yoink_inv_try_parse_positive_int( '1' ) );
		self::assertTrue( yoink_inv_try_parse_positive_int( 1 ) );
	}
}
