<?php

/**
 * Plugin Name: Yoink Inventory
 * Description: Adds inventory management to Woo Commerce
 * Version: 1.0.0
 * Author: Stime Tech
 * Author URI: https://stime.tech/
 * Copyright: © 2017 Brett Stime.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
	if ( ! class_exists( 'YoinkInvWooPlugin' ) ) {
		final class YoinkInvWooPlugin {
			private static $_instance = null;

			private function __construct() {
				if ( ! defined( 'YOINK_INV_ABSPATH' ) ) {
					define( 'YOINK_INV_ABSPATH', dirname( __FILE__ ) . '/' );
					define( 'YOINK_INV_PLUGIN_FILE', __FILE__ );
				}

//				self::yoink_inv_debug_log( 'YoinkInvWooPlugin::__construct called' );
				include_once( YOINK_INV_ABSPATH
				              . 'src/Includes/yoink-rest-api.php' );
				if ( is_admin() ) {
					include_once( YOINK_INV_ABSPATH
					              . 'src/Includes/yoink-admin-menus.php' );
				}

				$later_priority_than_wc
					= 15; // WordPress default is 10; WC is unspecified at time of this comment.
				add_action( 'admin_enqueue_scripts',
					array( $this, 'yoink_inv_on_admin_enqueue_scripts' ),
					$later_priority_than_wc );
				add_action( 'woocommerce_process_shop_order_meta',
					function ( $order_id ) {
						include_once( YOINK_INV_ABSPATH
						              . 'src/Includes/on-order-update.php' );
						YoinkInv\yoink_inv_on_order_update( $order_id );
					}, 45, 1 );

				$this->yoink_inv_load_plugin_textdomain();

				register_activation_hook( __FILE__,
					array( __CLASS__, 'yoink_inv_on_activation' ) );
			}

			public static function yoink_inv_instance() {
				if ( is_null( self::$_instance ) ) {
					self::$_instance = new self();
				}

				return self::$_instance;
			}

			public static function yoink_inv_on_activation() {
				include_once( YOINK_INV_ABSPATH
				              . 'src/Includes/yoink-install.php' );
				YoinkInvInstall::yoink_inv_install();
			}

			// public static function on_template_redirect($default_template) {
			// 	if ($_SERVER['REQUEST_URI'] == '/wp-admin/yoink-printable-qr-codes') {
			// 		YoinkInvWooPlugin::yoink_inv_debug_log('template for yoink-printable-qr-codes');
			// 		include (YOINK_INV_ABSPATH . 'admin-views/printable-qr-codes.php');
			// 		exit;
			// 	}
			// 	if ($_SERVER['REQUEST_URI'] == '/wp-admin/yoink-export-csv') {
			// 		include (YOINK_INV_ABSPATH . 'admin-views/csv-snapshot.php');
			// 		exit;
			// 	}

			// 	$qr_load_hook = add_submenu_page('yoink_inventory', 'Generate QR Codes',
			// 	'Generate QR Codes', 'yoink_generate_qr_codes', 'generate_yoink_qr_codes', ' ');
			// add_action( "load-$csv_load_hook", function() {
			// 	include_once( YOINK_INV_ABSPATH . 'admin-views/generate-qr-codes-view.php' );
			// 	YoinkInvWooPlugin::yoink_inv_debug_log('Load action for ');
			// 	ExportCsv::display();
			// });

			// $csv_load_hook = add_submenu_page('yoink_inventory', 'Export CSV', 'Export CSV',
			// 	'yoink_export_data', 'export_csv', ' ');
			// add_action( "load-$csv_load_hook", function() {
			// 	include_once( YOINK_INV_ABSPATH . 'admin-views/csv-snapshot.php' );
			// 	YoinkInvWooPlugin::yoink_inv_debug_log('Load action for export_csv');
			// 	ExportCsv::display();
			// });


			// 	return $default_template;
			// }

			public function yoink_inv_on_admin_enqueue_scripts() {
				$css_handle = 'yoink';

				$css_path = plugins_url( 'assets/styles.css', __FILE__ );

				wp_register_style( $css_handle, $css_path );
				wp_enqueue_style( $css_handle );

				YoinkInvAdminMenus::yoink_inv_enqueue_assets();
			}

			private function yoink_inv_load_plugin_textdomain() {
				$locale = is_admin() && function_exists( 'get_user_locale' )
					? get_user_locale() : get_locale();
				$locale = apply_filters( 'plugin_locale', $locale,
					'yoinkinventory' );

				load_textdomain( 'yoinkinventory',
					WP_LANG_DIR . '/yoinkinventory/yoinkinventory-' . $locale
					. '.mo' );
				load_plugin_textdomain( 'yoinkinventory', false,
					plugin_basename( dirname( __FILE__ ) )
					. '/i18n/languages' );
			}

			public static function yoink_inv_debug_log( $message ) {
				if ( true === WP_DEBUG ) {
					if ( is_array( $message ) || is_object( $message ) ) {
						error_log( print_r( $message, true ) );
					} else {
						error_log( $message );
					}
				}
			}
		}

		YoinkInvWooPlugin::yoink_inv_instance();
	}
}
